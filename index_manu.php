<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <title>Manutenção Agendada - FIta de Laço</title>
  <meta name="description" content="">
  <meta name="author" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,600,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" media="screen" href="_tplmanutencao/css/style.css">
  <link href="_tplmanutencao/img/favicon.ico" rel="icon" />
</head>
<body>

  <!-- particles.js container -->
  <div id="particles-js"></div>

  <div id="body">
    <div class="wrap">
      <div class="content">
        <img src="_tplmanutencao/img/logo.png" alt="Logo">
        <h1>Manutenção Agendada!</h1>

        <p>Olá, estamos realizando uma matenunção de emergência. Voltamos o mais breve possível. <br> Prazo estimado de manutenção é de 12 ~ 24 horas <br> <b>18/05/2018 às 22:00 horas até 20/05/2018 às 22:00 horas</b> (Horário de Brasília)</p>

        <h2>Estamos atendendo pelo telefone: (11) 9.5969-0579 <i class="fa fa-whatsapp"></i></h2>
      </div>
    </div>
  </div>

  <!-- scripts -->
  <script src="_tplmanutencao/js/particles.js"></script>
  <script src="_tplmanutencao/js/app.js?1"></script>

</body>
</html>