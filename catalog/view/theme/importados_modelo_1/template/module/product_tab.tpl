

  <div id="tabs-<?php echo $module; ?>" class="htabs">
  
	<?php if($featured_products){ ?>
	<ul class="nav nav-tabs">
			<li class="active">
			
	<a href="#tab-featured-<?php echo $module; ?>" data-toggle="tab" class="selected"><?php echo $tab_featured; ?></a></li>
	<?php } ?>
	
	<?php if($bestseller_products){ ?>
	<li>
	<a href="#tab-bestseller-<?php echo $module; ?>" data-toggle="tab"><?php echo $tab_bestseller; ?></a></li>
	<?php } ?>
	
  <?php if($latest_products){ ?>
  <li>
  <a href="#tab-latest-<?php echo $module; ?>" data-toggle="tab"><?php echo $tab_latest; ?></a></li>
  <?php } ?>
  
	<?php if($special_products){ ?>
	<li>
	<a href="#tab-special-<?php echo $module; ?>" data-toggle="tab"><?php echo $tab_special; ?></a></li>
	<?php } ?>
	</ul>
 </div>
 
 <div class="tab-content">
<?php if($latest_products){ ?>
 <div id="tab-latest-<?php echo $module; ?>" class="tab-content">

    
      <div class="category-item-container">



      <div class="row">



        <?php foreach ($latest_products as $product) { ?>



        <div class="col-md-3 col-sm-4 col-xs-12 autoHeight">



          <div class="item">



            <div class="item-image-container">



                <figure>



                  <a href="<?php echo $product['href']; ?>">



                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="item-image">



                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="item-image-hover">



                  </a>



                </figure>



                  <?php if ($product['price']) { ?>



                    <div class="item-price-container">



                      <?php if (!$product['special']) { ?>



                      <span class="item-price"> <?php echo $product['price']; ?> </span>



                      <?php } else { ?>



                      <span class="old-price"><?php echo $product['price']; ?></span>



                      <span class="item-price"><?php echo $product['special']; ?></span>



                      <?php } ?>



                    </div>



                  <?php } ?>



            </div>







            <div class="item-meta-container">



                <h3 class="item-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>



                <div class="item-action">



                  <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="item-add-btn">



                    <span class="icon-cart-text" style="display: inline-block;"><?php echo $button_cart; ?></span>



                  </button>



                  <div class="item-action-inner" style="visibility: hidden; overflow: hidden; width: 0px;">



                     <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-like"></button>



                      <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-compare"></button>



                  </div><!-- End .item-action-inner -->



                </div><!-- End .item-action -->

  <?php  if ($product['shipping'] == 0) {echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:110px; border-radius:5px; margin:.5em auto;' />";}?>

              </div>







          </div><!-- item -->          



        </div>



        <?php } ?>



      </div>



    </div>



 </div>
<?php } ?>
<?php if($featured_products){ ?>
  <div id="tab-featured-<?php echo $module; ?>" class="tab-content">

      <div class="category-item-container">



      <div class="row">



        <?php foreach ($featured_products as $product) { ?>



        <div class="col-md-3 col-sm-4 col-xs-12 autoHeight">



          <div class="item">



            <div class="item-image-container">



                <figure>


                  <a href="<?php echo $product['href']; ?>">



                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="item-image">



                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="item-image-hover">



                  </a>



                </figure>



                  <?php if ($product['price']) { ?>



                    <div class="item-price-container">



                      <?php if (!$product['special']) { ?>



                      <span class="item-price"> <?php echo $product['price']; ?> </span>



                      <?php } else { ?>



                      <span class="old-price"><?php echo $product['price']; ?></span>



                      <span class="item-price"><?php echo $product['special']; ?></span>



                      <?php } ?>



                    </div>



                  <?php } ?>



            </div>







            <div class="item-meta-container">



                <h3 class="item-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>



                <div class="item-action">



                  <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="item-add-btn">



                    <span class="icon-cart-text" style="display: inline-block;"><?php echo $button_cart; ?></span>



                  </button>



                  <div class="item-action-inner" style="visibility: hidden; overflow: hidden; width: 0px;">



                     <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-like"></button>



                      <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-compare"></button>



                  </div><!-- End .item-action-inner -->



                </div><!-- End .item-action -->

  <?php  if ($product['shipping'] == 0) {echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:110px; border-radius:5px; margin:.5em auto;' />";}?>

              </div>







          </div><!-- item -->          



        </div>



        <?php } ?>



      </div>



    </div>




 </div>
<?php } ?>

<?php if($bestseller_products){ ?>
 <div id="tab-bestseller-<?php echo $module; ?>" class="tab-content">


      <div class="category-item-container">



      <div class="row">



        <?php foreach ($bestseller_products as $product) { ?>



        <div class="col-md-3 col-sm-4 col-xs-12 autoHeight">



          <div class="item">



            <div class="item-image-container">



                <figure>



                  <a href="<?php echo $product['href']; ?>">



                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="item-image">



                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="item-image-hover">



                  </a>



                </figure>



                  <?php if ($product['price']) { ?>



                    <div class="item-price-container">



                      <?php if (!$product['special']) { ?>



                      <span class="item-price"> <?php echo $product['price']; ?> </span>



                      <?php } else { ?>



                      <span class="old-price"><?php echo $product['price']; ?></span>



                      <span class="item-price"><?php echo $product['special']; ?></span>



                      <?php } ?>



                    </div>



                  <?php } ?>



            </div>







            <div class="item-meta-container">



                <h3 class="item-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>



                <div class="item-action">



                  <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="item-add-btn">



                    <span class="icon-cart-text" style="display: inline-block;"><?php echo $button_cart; ?></span>



                  </button>



                  <div class="item-action-inner" style="visibility: hidden; overflow: hidden; width: 0px;">



                     <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-like"></button>



                      <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-compare"></button>



                  </div><!-- End .item-action-inner -->



                </div><!-- End .item-action -->

  <?php  if ($product['shipping'] == 0) {echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:110px; border-radius:5px; margin:.5em auto;' />";}?>

              </div>







          </div><!-- item -->          



        </div>



        <?php } ?>



      </div>



    </div>




 </div>
<?php } ?>

<?php if($special_products){ ?>
 <div id="tab-special-<?php echo $module; ?>" class="tab-content">
        <div class="category-item-container">



      <div class="row">



        <?php foreach ($special_products as $product) { ?>



        <div class="col-md-3 col-sm-4 col-xs-12 autoHeight">



          <div class="item">



            <div class="item-image-container">



                <figure>



                  <a href="<?php echo $product['href']; ?>">



                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="item-image">



                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="item-image-hover">



                  </a>



                </figure>



                  <?php if ($product['price']) { ?>



                    <div class="item-price-container">



                      <?php if (!$product['special']) { ?>



                      <span class="item-price"> <?php echo $product['price']; ?> </span>



                      <?php } else { ?>



                      <span class="old-price"><?php echo $product['price']; ?></span>



                      <span class="item-price"><?php echo $product['special']; ?></span>



                      <?php } ?>



                    </div>



                  <?php } ?>



            </div>







            <div class="item-meta-container">



                <h3 class="item-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>



                <div class="item-action">



                  <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="item-add-btn">



                    <span class="icon-cart-text" style="display: inline-block;"><?php echo $button_cart; ?></span>



                  </button>



                  <div class="item-action-inner" style="visibility: hidden; overflow: hidden; width: 0px;">



                     <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-like"></button>



                      <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-compare"></button>



                  </div><!-- End .item-action-inner -->



                </div><!-- End .item-action -->

  <?php  if ($product['shipping'] == 0) {echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:110px; border-radius:5px; margin:.5em auto;' />";}?>

              </div>







          </div><!-- item -->          



        </div>



        <?php } ?>



      </div>



    </div>




 </div>
<?php } ?>
</div>
<script type="text/javascript">
$('#tabs-<?php echo $module; ?> a').tabs();
</script> 
<div class="clear"></div>
