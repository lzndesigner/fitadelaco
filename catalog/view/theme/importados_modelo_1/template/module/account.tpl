 <aside>
  <h2 class="title">Menu de Cliente</h2>
    <div class="sideNav style1">
      <?php if (!$logged) { ?>

      <ul class="splashNav">
        <li><a href="<?php echo $login; ?>"><i class="fa fa-user"></i> <?php echo $text_login; ?></a></li> 
        <li><a href="<?php echo $register; ?>"><i class="fa fa-user-plus"></i> <?php echo $text_register; ?></a></li>
        <li><a href="<?php echo $forgotten; ?>"><i class="fa fa-lock"></i> <?php echo $text_forgotten; ?></a></li>
      </ul>


      <?php } ?>
      <?php if ($logged) { ?>

      <ul class="splashNav">
        <li><a href="<?php echo $edit; ?>"><i class="fa fa-user"></i> <?php echo $text_edit; ?></a></li>
        <li><a href="<?php echo $password; ?>"><i class="fa fa-key"></i> <?php echo $text_password; ?></a></li>
        <li><a href="<?php echo $address; ?>"><i class="fa fa-home"></i> <?php echo $text_address; ?></a></li>
        <li><a href="<?php echo $wishlist; ?>"><i class="fa fa-heart"></i> <?php echo $text_wishlist; ?></a></li>
        <li><a href="<?php echo $logout; ?>"><i class="fa fa-sign-out"></i> <?php echo $text_logout; ?></a></li>
      </ul>

      <ul class="splashNav">
         <li><a href="<?php echo $order; ?>"><i class="fa fa-bell"></i> <?php echo $text_order; ?></a></li>
         <li><a href="<?php echo $download; ?>"><i class="fa fa-download"></i> <?php echo $text_download; ?></a></li>
         <?php if ($reward) { ?>
         <li><a href="<?php echo $reward; ?>"><i class="fa fa-plus"></i> <?php echo $text_reward; ?></a></li>
         <?php } ?>
         <li><a href="<?php echo $return; ?>"><i class="fa fa-reply"></i> <?php echo $text_return; ?></a></li>
         <li><a href="<?php echo $transaction; ?>"><i class="fa fa-bank"></i> <?php echo $text_transaction; ?></a></li>
         <li><a href="<?php echo $recurring; ?>"><i class="fa fa-briefcase"></i> <?php echo $text_recurring; ?></a></li>
         <li><a href="index.php?route=information/confirmarpagamento"><i class="fa fa-check"></i> Confirmação de Pagamento</a></li>
      </ul>


      <ul class="splashNav">
         <li><a href="<?php echo $newsletter; ?>"><i class="fa fa-star"></i> <?php echo $text_newsletter; ?></a></li>
      </ul>

      <?php } ?>


      <ul class="splashNav">
        <li><a href="index.php?route=information/contact"><?php echo $text_contact; ?></a></li>
        <li><a href="index.php?route=information/confirmarpagamento"><?php echo $text_confirm; ?></a></li>
        <li><a href="index.php?route=account/return/add"><?php echo $text_return; ?></a></li>        
      </ul>
      <ul class="splashNav">
        <li><a href="index.php?route=information/sitemap"><?php echo $text_map; ?></a></li>
      </ul>
    </div>
  </aside>
