 <aside>
  <h2 class="title">Menu de Afiliados</h2>
    <div class="sideNav style1">


 <?php if (!$logged) { ?>
 <ul class="splashNav">
  <li><a href="<?php echo $login; ?>"><i class="fa fa-user"></i> <?php echo $text_login; ?></a></li>
  <li><a href="<?php echo $register; ?>"><i class="fa fa-user-plus"></i> <?php echo $text_register; ?></a></li>
  <li><a href="<?php echo $forgotten; ?>" ><i class="fa fa-key"></i> <?php echo $text_forgotten; ?></a></li>
 </ul>
  <?php } ?>
 <ul class="splashNav">
  <li><a href="<?php echo $account; ?>"><i class="fa fa-user"></i> <?php echo $text_account; ?></a></li>
</ul>
  <?php if ($logged) { ?>
 <ul class="splashNav">
  <li><a href="<?php echo $edit; ?>"><i class="fa fa-edit"></i> <?php echo $text_edit; ?></a></li>
  <li><a href="<?php echo $password; ?>"><i class="fa fa-key"></i> <?php echo $text_password; ?></a></li>
</ul>
  <?php } ?>
 <ul class="splashNav">
  <li><a href="<?php echo $payment; ?>"><i class="fa fa-info-circle"></i> <?php echo $text_payment; ?></a></li>
  <li><a href="<?php echo $tracking; ?>"><i class="fa fa-cogs"></i> <?php echo $text_tracking; ?></a></li>
  <li><a href="<?php echo $transaction; ?>"><i class="fa fa-credit-card"></i> <?php echo $text_transaction; ?></a></li>
</ul>
  <?php if ($logged) { ?>
 <ul class="splashNav">
  <li><a href="<?php echo $logout; ?>"><i class="fa fa-sign-out"></i> <?php echo $text_logout; ?></a></li>
</ul>
  <?php } ?> 

      <ul class="splashNav">
        <li><a href="index.php?route=information/contact"><?php echo $text_contact; ?></a></li>
        <li><a href="index.php?route=information/confirmarpagamento"><?php echo $text_confirm; ?></a></li>
        <li><a href="index.php?route=account/return/add"><?php echo $text_return; ?></a></li>        
      </ul>
      <ul class="splashNav">
        <li><a href="index.php?route=information/sitemap"><?php echo $text_map; ?></a></li>
      </ul>
    </div>
  </aside>