<?php echo $header; ?>
<div class="breadcrumb clearfix bgGrey">
  <div class="container">
    <h5><?php echo $heading_title; ?></h5>
    <ul class="">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div><!-- breadcrumb -->

<section class="archive xvArchieve sidebar-enable" style="margin-top:50px;">
  <div class="container">
    <div class="row">
      <?php echo $column_left; ?>

      <?php if ($column_left && $column_right) { ?>

      <?php $class = 'col-sm-6'; ?>

      <?php } elseif ($column_left || $column_right) { ?>

      <?php $class = 'col-sm-9'; ?>

      <?php } else { ?>

      <?php $class = 'col-sm-12'; ?>

      <?php } ?>

      <div id="content" class="products style2 style-demo1 text-center <?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="manageResults category" style="margin-bottom:20px; text-align:left;">        

         <h6 class="title"><?php echo $heading_title; ?></h6>


         <fieldset>
          <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>

          <div class="row">

            <div class="col-sm-4">

              <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />

            </div>

            <div class="col-sm-3">

              <select name="category_id" class="form-control">

                <option value="0"><?php echo $text_category; ?></option>

                <?php foreach ($categories as $category_1) { ?>

                <?php if ($category_1['category_id'] == $category_id) { ?>

                <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>

                <?php } else { ?>

                <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>

                <?php } ?>

                <?php foreach ($category_1['children'] as $category_2) { ?>

                <?php if ($category_2['category_id'] == $category_id) { ?>

                <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>

                <?php } else { ?>

                <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>

                <?php } ?>

                <?php foreach ($category_2['children'] as $category_3) { ?>

                <?php if ($category_3['category_id'] == $category_id) { ?>

                <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>

                <?php } else { ?>

                <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>

                <?php } ?>

                <?php } ?>

                <?php } ?>

                <?php } ?>

              </select>

            </div>

            <div class="col-sm-3">

              <label class="checkbox-inline">

                <?php if ($sub_category) { ?>

                <input type="checkbox" name="sub_category" value="1" checked="checked" />

                <?php } else { ?>

                <input type="checkbox" name="sub_category" value="1" />

                <?php } ?>

                <?php echo $text_sub_category; ?></label>

              </div>

            </div>

            <p>

              <label class="checkbox-inline">

                <?php if ($description) { ?>

                <input type="checkbox" name="description" value="1" id="description" checked="checked" />

                <?php } else { ?>

                <input type="checkbox" name="description" value="1" id="description" />

                <?php } ?>

                <?php echo $entry_description; ?></label>

              </p>

              <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-purple" />
            </fieldset>



            <h6 class="title"><?php echo $text_search; ?></h6>


            <?php if ($products) { ?>

            <a href="<?php echo $compare; ?>" class="btn btn-default pull-left btn-sm" style="margin:10px 0;" id="compare-total"><?php echo $text_compare; ?></a>
            <a href="#" class="manageTriger"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/nav.png" alt=""></a>
            <ul class="change-prod-style" style="display:none;">
              <li><a class="prod-style-2" href="#"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/grid-s.png" alt=""></a></li>
              <li class="active"><a class="prod-style-3" href="#"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/grid-small.png" alt=""></a></li>
              <li><a class="prod-style-4" href="#"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/grid-4.png" alt=""></a></li>
              <li><a class="prod-style-5" href="#"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/grid.png" alt=""></a></li>
            </ul>
            <div class="xvmanage" style="margin-top:10px;">
              <div class="row">
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-sm-4">
                      <h6><?php echo $text_sort; ?></h6>
                      <select id="input-sort" class="light" onchange="location = this.value;">
                        <?php foreach ($sorts as $sorts) { ?>
                        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select>                                                
                    </div>
                    <div class="col-sm-4">
                      <h6><?php echo $text_limit; ?></h6>
                      <select id="input-limit" class="light" onchange="location = this.value;">
                        <?php foreach ($limits as $limits) { ?>
                        <?php if ($limits['value'] == $limit) { ?>
                        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-sm-4">

                    </div>
                  </div>
                </div>
              </div>
            </div><!-- xvmanage -->
          </div>







  <ul class="products item-back-display style3 col-grid-4 space-30 clearfix">


            <?php foreach ($products as $product) { ?>


  <li class="product text-center">
      <div class="productImages">
        <?php if ($product['quantity'] <= '0') { ?>
        <span class="dontStock"><?php echo $text_instock; ?></span>
        <?php } ?>
        <div class="image-default"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
      </div>
      <div class="productInfo">
        <h3><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>

        <?php if ($product['price']) { ?>
        <p><small><b>A partir de:</b></small></p>
        <ul class="pricestyle">
          <?php if (!$product['special']) { ?>
          <li><span class="priceReal"><?php echo $product['price']; ?></span></li>
          <?php } else { ?>
          <li>De: <strike><?php echo $product['price']; ?></strike></li>
          <li>Por: <span class="priceReal"><?php echo $product['special']; ?></span></li>
          <?php } ?> 
        </ul>
        <?php } ?>
        
        <?php if (!$product['quantity'] <= '0') { ?>
        <div class="btn-group btn-group-justified" role="group">

          <div class="btn-group" role="group">
            <button type="submit" class="add_to_cart_button btn btn-purple" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <?php echo $button_cart; ?></button>
          </div>

          <div class="btn-group" role="group">
            <button type="submit" class="add_to_cart_button btn btn-default" id="btnNotification" data-toggle="tooltip" title="<?php echo $button_details; ?>" onclick="location.href='<?php echo $product['href']; ?>#content';"><i class="fa fa-shopping-cart"></i> <?php echo $button_details; ?></button>
          </div>

        </div>
        <?php }else{ ?>
        <ul class="linksBotoes">            
          <li><button type="submit" class="add_to_cart_button" id="btnNotification" data-toggle="tooltip" title="<?php echo $button_visit; ?>" onclick="location.href='<?php echo $product['href']; ?>#content';"><i class="fa fa-shopping-cart"></i> <?php echo $button_visit; ?></button></li>           
        </ul>


        <?php } ?>

        <?php  if ($product['shipping'] == 0) {echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:110px; border-radius:5px; margin:.5em auto;' />";}?>
      </div>
    </li>

            <?php } ?>


          </ul>

          <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
          </div>

          <?php } ?>



          <?php if (!$categories && !$products) { ?>
          <p><?php echo $text_empty; ?></p>
          <div class="buttons">
            <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_continue; ?></a></div>
          </div>
          <?php } ?>
          <?php echo $content_bottom; ?>
        </div><!-- #content -->
        <?php echo $column_right; ?>
      </div><!-- row -->
    </div><!-- container -->
  </section><!--//wrap-->


  <script type="text/javascript"><!--

  $('#button-search').bind('click', function() {

   url = 'index.php?route=product/search';



   var search = $('#content input[name=\'search\']').prop('value');



   if (search) {

    url += '&search=' + encodeURIComponent(search);

  }



  var category_id = $('#content select[name=\'category_id\']').prop('value');



  if (category_id > 0) {

    url += '&category_id=' + encodeURIComponent(category_id);

  }



  var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');



  if (sub_category) {

    url += '&sub_category=true';

  }



  var filter_description = $('#content input[name=\'description\']:checked').prop('value');



  if (filter_description) {

    url += '&description=true';

  }



  location = url;

});



  $('#content input[name=\'search\']').bind('keydown', function(e) {

   if (e.keyCode == 13) {

    $('#button-search').trigger('click');

  }

});



  $('select[name=\'category_id\']').on('change', function() {

   if (this.value == '0') {

    $('input[name=\'sub_category\']').prop('disabled', true);

  } else {

    $('input[name=\'sub_category\']').prop('disabled', false);

  }

});



  $('select[name=\'category_id\']').trigger('change');

  --></script> 


  <?php echo $footer; ?>