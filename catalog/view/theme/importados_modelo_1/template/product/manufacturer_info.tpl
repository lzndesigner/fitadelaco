<?php echo $header; ?>
<div class="breadcrumb clearfix bgGrey">

  <div class="container">

    <h5><?php echo $heading_title; ?></h5>

    <ul class="">

      <?php foreach ($breadcrumbs as $breadcrumb) { ?>

      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

      <?php } ?>

    </ul>

  </div>

</div><!-- breadcrumb -->


<div class="container">

  <div class="row"><?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="products style2 style-demo1 text-center <?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="manageResults category" style="margin-bottom:20px;">
        <h1 class="title"><?php echo $heading_title; ?></h1>


        

        <?php if ($products) { ?>
        <a href="<?php echo $compare; ?>" class="btn btn-default pull-left btn-sm" style="margin:0;" id="compare-total"><?php echo $text_compare; ?></a>
        <a href="#" class="manageTriger"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/nav.png" alt=""></a>
        <ul class="change-prod-style" style="display:none;">
          <li><a class="prod-style-2" href="#"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/grid-s.png" alt=""></a></li>
          <li class="active"><a class="prod-style-3" href="#"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/grid-small.png" alt=""></a></li>
          <li><a class="prod-style-4" href="#"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/grid-4.png" alt=""></a></li>
          <li><a class="prod-style-5" href="#"><img src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/img/icons/grid.png" alt=""></a></li>
        </ul>
        <div class="xvmanage">
          <div class="row">
            <div class="col-md-8">
              <div class="row">
                <div class="col-sm-4">
                  <h6><?php echo $text_sort; ?></h6>
                  <select id="input-sort" class="light" onchange="location = this.value;">
                    <?php foreach ($sorts as $sorts) { ?>
                    <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                    <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>                                                
                </div>
                <div class="col-sm-4">
                  <h6><?php echo $text_limit; ?></h6>
                  <select id="input-limit" class="light" onchange="location = this.value;">
                    <?php foreach ($limits as $limits) { ?>
                    <?php if ($limits['value'] == $limit) { ?>
                    <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-sm-4">
                  
                </div>
              </div>
            </div>
          </div>
        </div><!-- xvmanage -->
      </div>

      <ul class="products categorys item-back-display style3 col-grid-4 space-30 clearfix">

        <?php foreach ($products as $product) { ?>

     <li class="product text-center">
    <div class="productImages">
      <?php if ($product['quantity'] <= '0') { ?>
      <span class="dontStock"><?php echo $text_instock; ?></span>
      <?php } ?>
      <div class="image-default"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
    </div>
    <div class="productInfo">
      <h3><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>

        <?php if ($product['price']) { ?>
        <p><small><b>A partir de:</b></small></p>
        <ul class="pricestyle">
          <?php if (!$product['special']) { ?>
          <li><span class="priceReal"><?php echo $product['price']; ?></span></li>
          <?php } else { ?>
          <li>De: <strike><?php echo $product['price']; ?></strike></li>
          <li>Por: <span class="priceReal"><?php echo $product['special']; ?></span></li>
          <?php } ?> 
        </ul>
        <?php } ?>
        
          <?php if (!$product['quantity'] <= '0') { ?>
        <div class="btn-group btn-group-justified" role="group">

          <div class="btn-group" role="group">
            <button type="submit" class="add_to_cart_button btn btn-purple" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <?php echo $button_cart; ?></button>
          </div>

          <div class="btn-group" role="group">
            <button type="submit" class="add_to_cart_button btn btn-default" id="btnNotification" data-toggle="tooltip" title="<?php echo $button_details; ?>" onclick="location.href='<?php echo $product['href']; ?>#content';"><i class="fa fa-book"></i> <?php echo $button_details; ?></button>
          </div>

        </div>
        <?php }else{ ?>
        <ul class="linksBotoes">            
          <li><button type="submit" class="add_to_cart_button" id="btnNotification" data-toggle="tooltip" title="<?php echo $button_visit; ?>" onclick="location.href='<?php echo $product['href']; ?>#content';"><i class="fa fa-shopping-cart"></i> <?php echo $button_visit; ?></button></li>           
        </ul>
        <?php } ?>
      <?php  if ($product['shipping'] == 0) {echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:110px; border-radius:5px; margin:.5em auto;' />";}?>
    </div>
  </li>

      <?php } ?>



      

    </ul>

    <div class="row">
      <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
      <div class="col-sm-6 text-right"><?php echo $results; ?></div>
    </div>
    

    <?php } else { ?>

    <p><?php echo $text_empty; ?></p>
    <div class="buttons">
      <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_continue; ?></a></div>
    </div>

    <?php } ?>
  </div>

  <?php echo $content_bottom; ?></div>

  <?php echo $column_right; ?></div>

</div>

<?php echo $footer; ?> 