<?php echo $header; ?>

<div class="breadcrumb clearfix bgGrey">
  <div class="container">
    <h5><?php echo $heading_title; ?></h5>
    <ul class="">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div><!-- breadcrumb -->


<div class="container">

  <?php if ($success) { ?>



  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>



  <?php } ?>



  <?php if ($error_warning) { ?>



  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>



  <?php } ?>



  <div class="row"><?php echo $column_left; ?>



    <?php if ($column_left && $column_right) { ?>



    <?php $class = 'col-sm-6'; ?>



    <?php } elseif ($column_left || $column_right) { ?>



    <?php $class = 'col-sm-9'; ?>



    <?php } else { ?>



    <?php $class = 'col-sm-12'; ?>



    <?php } ?>



    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>


      <div class="accountDropdownTriger">
        <div class="accountDropdown style clearfix">
          <ul class="tabs">
            <li class="active">
              <a href="#tab1"><?php echo $text_no_cliente; ?></a>
            </li>
            <li>
              <a href="#tab2"><?php echo $text_yes_cliente; ?></a>
            </li>
          </ul>
          <div class="tab-panel">
            <div id="tab1" class="tab-pane active">
              <p><?php echo $text_register; ?></p>
              <p><?php echo $text_register_account; ?></p>
              <a href="<?php echo $register; ?>" class="btn btn-default"><?php echo $button_continue; ?></a>
            </div>

            <div id="tab2" class="tab-pane">              
              <p><?php echo $text_i_am_returning_customer; ?></p>
              <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                 <div class="itemDesc clearfix">
                    <div class="name">
                      <?php echo $entry_email; ?></label>
                    </div>
                    <div class="cont">
                      <div class="custome-select grey clearfix">
                        <input type="email" required name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                      </div>
                    </div>
                  </div><!-- itemDesc -->

                  <div class="itemDesc clearfix">
                    <div class="name">
                      <?php echo $entry_password; ?></label>
                    </div>
                    <div class="cont">
                      <div class="custome-select grey clearfix">
                        <input type="password" required name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                      </div>
                    </div>
                  </div><!-- itemDesc -->

                <span class="pull-right text-right">
                 <a href="<?php echo $forgotten; ?>" class="btn btn-danger"><?php echo $text_forgotten; ?></a>
               </span>
               <div class="pull-left text-left">
                <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-default" />
                <?php if ($redirect) { ?>
                <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                <?php } ?>         
              </div><!-- End .input-group -->
            </form>
          </div>
        </div>
      </div>
    </div>






    </div>



      <?php echo $content_bottom; ?>



    <?php echo $column_right; ?></div>



</div>



<?php echo $footer; ?>