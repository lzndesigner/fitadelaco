<?php echo $header; ?>
<div class="breadcrumb clearfix bgGrey">
  <div class="container">
    <h5><?php echo $heading_title; ?></h5>
    <ul class="">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div><!-- breadcrumb -->

<div class="container">
            <?php foreach ($totals as $total) { ?>

            <?php 
            if($total['title'] == "Sub-total"){

                $minimo = "5000"; // VALOR MÍNIMO QUE VAI UTILIZAR NA LOJA
                $vertotal = str_replace("R$","", $total['text']);
                $vertotal = str_replace(",",".", $vertotal);
                $vertotal = str_replace(".","", $vertotal);
                if ($vertotal < $minimo) {
                  echo "<div class='alert alert-danger'>O valor mínimo de <b>R$ 50,00</b> não foi atingido. <br/> Clique no botão abaixo e continue comprando.<br/>";
                  echo "</div>";
                  echo "<a href='".$continue."' class='btn btn-purple'><i class='fa fa-reply'></i> ".$button_shopping."</a>";
                }

              }
            ?>
            <?php } ?>
  <?php if ($attention) { ?>
  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>


     <h1 class="title"><?php echo $heading_title; ?></h1>
     <?php if ($weight) { ?>
     <p>(<?php echo $weight; ?>)</p>
     <?php } ?>    

     <div class="row">
      <div class="col-md-8">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

          <ul class="listCartProducts">
           <?php $i=0; foreach ($products as $product) {  $i++;?>
           <li>
            <?php if ($product['thumb']) { ?>
            <div class="img">
             <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
           </div><!-- img -->
           <?php } ?>
           <div class="content">
            <!-- content -->
            <h1><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h1>
            <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-sm btn-danger" style="margin:0px 0 0 0;padding:4.5px 8px;" onclick="cart.remove('<?php echo $product['key']; ?>');"><i class="fa fa-times-circle"></i></button>

            <span class="article">
              - Ref: <?php echo $product['model']; ?>
            </span>

            <?php if (!$product['stock']) { ?>
                  <span class="text-danger" style="font-size:14px;"><i class="fa fa-exclamation-circle"></i></span>
            <?php } ?>

            <?php if ($product['option']) { ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <br />
                  <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                  <?php } ?>
            <?php } ?>

            <?php if ($product['reward']) { ?>
                  <br />
                  <small><?php echo $product['reward']; ?></small>
            <?php } ?>

            <?php if ($product['recurring']) { ?>
                    <br />
                    <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
            <?php } ?>

              <div class="quantity">
                <p>Quantidade:</p>
                <div class="input-group btn-block" style="max-width: 200px;">


                   <script>
                $(document).ready(function() {

                  var idProd = "<?php echo $i; ?>"

                   if($(".inputQuantity<?php echo $i; ?>").val() <= 1){
                    $(".menosQuantity<?php echo $i; ?>").addClass("disabled");                        
                    }else{
                    $(".menosQuantity<?php echo $i; ?>").removeClass("disabled");  
                    }

                  $(".maisQuantity<?php echo $i; ?>").click(function(){
                    if($(".inputQuantity<?php echo $i; ?>").val() >= 1){
                    $(".menosQuantity<?php echo $i; ?>").removeClass("disabled");  
                    $(".inputQuantity<?php echo $i; ?>").val(parseInt($(".inputQuantity<?php echo $i; ?>").val())+1);
                    }

                  });// mais

                  $(".menosQuantity<?php echo $i; ?>").click(function(){
                      if($(".inputQuantity<?php echo $i; ?>").val()!=1){                                        
                        $(".inputQuantity<?php echo $i; ?>").val(parseInt($(".inputQuantity<?php echo $i; ?>").val())-1);         
                        if($(".inputQuantity<?php echo $i; ?>").val() <= 1){
                              $(".menosQuantity<?php echo $i; ?>").addClass("disabled");  
                          }             
                      }
                  });// menos
                });
                </script>


                <div class="btn-group btn-quantity">
                  <button type="button" class="btn btn-purple btn-sm menosQuantity<?php echo $i; ?>"><i class="fa fa-minus"></i></button>
                  <input type="text" class="inputQuantity<?php echo $i; ?>" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" min="1">
                  <button type="button" class="btn btn-purple btn-sm maisQuantity<?php echo $i; ?>"><i class="fa fa-plus"></i></button>
                </div>



                    <span class="input-group-btn">

                    <button type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="btn btn-sm btn-custom" style="margin:0px 0 0 0;padding:6.5px 12px;"><i class="fa fa-refresh"></i></button>

                    <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-sm btn-danger" style="margin:0px 0 0 0;padding:6.5px 12px;" onclick="cart.remove('<?php echo $product['key']; ?>');"><i class="fa fa-times-circle"></i></button></span></div>

               </div>

               <ul class="price">
                 <li class="total"><b><?php echo $product['total']; ?></b></li>
               </ul>

            <!-- content -->
          </div>
        </li>
        <?php }//foreach ?>
              <?php foreach ($vouchers as $vouchers) { ?>
               <tr>
                 <td></td>
                 <td class="text-left"><?php echo $vouchers['description']; ?></td>
                 <td class="text-left"></td>
                 <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">
                   <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />
                   <span class="input-group-btn"><button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $vouchers['key']; ?>');"><i class="fa fa-times-circle"></i></button></span></div></td>
                   <td class="text-right"><?php echo $vouchers['amount']; ?></td>
                   <td class="text-right"><?php echo $vouchers['amount']; ?></td>
                 </tr>
                 <?php } ?>
      </ul>
         </form>

<div class="clearfix"></div>
<a href="<?php echo $continue; ?>" class="btn btn-sm btn-default"><i class="fa fa-reply"></i> <?php echo $button_shopping; ?></a>

       </div><!-- col-md-8 -->

       <div class="col-md-4">
        <?php if($shipping){ ?>
        <?php echo $shipping; ?> 
        <?php } ?>

        <?php if($coupon){?>
        <?php echo $coupon; ?>
        <?php } ?>

        <?php if($voucher){ ?>
        <?php echo $voucher; ?>
        <?php } ?>

        <h4>Resumo do Pedido</h4>
        <ul class="resumeOrder">
         <?php foreach ($totals as $total) { ?>
         <li><b><?php echo $total['title']; ?>:</b>  <span class="text-left"><?php echo $total['text']; ?></span></li>
         <?php } ?>                  
       </ul>
       <hr class="clearfix">
            <?php foreach ($totals as $total) { ?>
            <?php 
            if($total['title'] == "Sub-total"){

                $minimo = "5000"; // VALOR MÍNIMO QUE VAI UTILIZAR NA LOJA
                $vertotal = str_replace("R$","", $total['text']);
                $vertotal = str_replace(",",".", $vertotal);
                $vertotal = str_replace(".","", $vertotal);
                if ($vertotal >= $minimo) { ?>
              <a <?php if (!$product['stock']) { ?> disabled<?php }else{?> href="<?php echo $checkout; ?>" <?php } ?> class="btn btn-purple btn-block" ><?php echo $button_checkout; ?></a>
              <?php 
                }else{
                  echo "<div class='alert alert-danger'>O valor mínimo de <b>R$ 50,00</b> não foi atingido.</div>";
                }
              }
            }
            ?>
         
     </div><!-- col-md-4 -->

   </div><!-- row -->

   <?php if ($reward) { ?>
   <header class="content-title" style="margin-top:1em;">
     <h1 class="title"><?php echo $text_next; ?></h1>
     <p><?php echo $text_next_choice; ?></p>    
   </header>     
   <div class="row">
     <div class="col-md-8 col-sm-12 col-xs-12">
       <?php echo $reward; ?>
     </div><!-- col-md-8 col-sm-12 col-xs-12 -->

     <?php } ?>
   </div><!-- row -->

   <?php echo $content_bottom; ?></div>
   <?php echo $column_right; ?></div>
 </div>
 <?php echo $footer; ?> 