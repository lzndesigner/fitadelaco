<li id="cart"  class="cartDropdownTriger">
 <a class="add_to_cart_button cartCount" href="#">
  <i class="fa fa-shopping-cart"></i> <?php echo $text_my_cart; ?>
    <span id="cart-total" class="cartCount"><?php echo $text_items; ?></span>
  
</a>
 <div class="cartDropdown">
  <div>
    <h5><?php echo $text_my_cart; ?></h5>
  </div>
  <?php if ($products || $vouchers) { ?>

    <div>
    <?php foreach ($totals as $total) { ?>
    <h6 style=""><?php echo $total['title']; ?> <span style="float:right;"><?php echo $total['text']; ?></span></h6>
    <?php } ?>
    <?php foreach ($totals as $total) { ?>
        <?php 
            if($total['title'] == "Sub-total"){

                $minimo = "5000"; // VALOR MÍNIMO QUE VAI UTILIZAR NA LOJA
                $vertotal = str_replace("R$","", $total['text']);
                $vertotal = str_replace(",",".", $vertotal);
                $vertotal = str_replace(".","", $vertotal);
                if ($vertotal < $minimo) {
                  echo "<div class='alert alert-danger' style='font-size:11px;'>O valor mínimo de <b>R$ 50,00</b> não foi atingido. <br/> Continue comprando.<br/>";
                  echo "</div>";
                }

              }
            ?>
  <?php } ?>

    <div class="margin5px"></div>
    <a href="<?php echo $cart; ?>" class="btn btn-default"><?php echo $text_cart; ?></a>
  </div>

  <?php foreach ($products as $product) { ?>
  <div style="width:100%; float:left;">
    <div class="addedItem">
      <div class="itemDetails">        
      <?php if ($product['thumb']) { ?>
      <figure>
        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
      </figure>
      <?php } ?>
        <span class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></span>
        <span class="option">
          <?php if ($product['option']) { ?>
          <?php foreach ($product['option'] as $option) { ?>
          <?php echo $option['name']; ?>: <?php echo $option['value']; ?>
          <?php } ?>
          <?php } ?>

          <?php if ($product['recurring']) { ?>
          <?php echo $text_recurring; ?> <?php echo $product['recurring']; ?>
          <?php } ?>
        </span>
        <span class="price"><?php echo $product['quantity']; ?>x <?php echo $product['price']; ?> = <?php echo $product['total']; ?></span>

      </div><!-- itemDetails -->
      <button type="button" onclick="cart.remove('<?php echo $product['key']; ?>');" title="<?php echo $button_remove; ?>" class="removeitem"><i class="xv-arrows_circle_remove"></i></button>
    </div><!-- addedItem -->
  </div>

  <?php } ?>

  <?php foreach ($vouchers as $voucher) { ?>


  <td class="text-center"></td>

  <td class="text-left"><?php echo $voucher['description']; ?></td>

  <td class="text-right">x&nbsp;1</td>

  <td class="text-right"><?php echo $voucher['amount']; ?></td>

  <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>



  <?php } ?>


  <?php } else { ?>  
  <div>
    <p class="text-center"><?php echo $text_empty; ?></p>
  </div> 
  <?php } ?>





</div>
</li>
