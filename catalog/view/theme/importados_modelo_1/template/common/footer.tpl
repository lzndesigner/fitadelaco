<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/595ba65750fd5105d0c83e3c/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

<footer class="doc-footer">
    <div class="bgFooter bgWhite">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 text-center">
                    <h6><?php echo $text_account; ?></h6>
                    <ul class="links">
                        <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                        <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                        <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                        <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>

                    </ul>
                </div>
                <div class="col-sm-3 text-center">
                    <h6><?php echo $text_information; ?></h6>
                    <ul class="links">
                        <?php if ($informations) { ?>
                        <?php foreach ($informations as $information) { ?>
                        <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php } ?>
                        <?php } ?>            


                    </ul>
                </div>
                <div class="col-sm-3 text-center">
                    <h6><?php echo $title_helpdesk; ?></h6>
                    <ul class="links">
                        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                        <li><a href="index.php?route=information/confirmarpagamento"><?php echo $text_confirm_payment; ?></a></li>
                        <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
                        <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <h6>Curta nossa Página</h6>
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.11&appId=1094654680570525';
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-page" data-href="https://www.facebook.com/fitadelaco1/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/fitadelaco1/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/fitadelaco1/">Fita de Laço</a></blockquote></div>
                </div>
            </div>
            <hr>
</div><!-- container -->

<div class="bgsocial-midia">
<div class="container">
<div class="row">
    <div class="col-md-12">
        <div class="col-md-3"><h5>Encontre-nos aqui</h5></div>
        <div class="col-md-9">
                <ul class="social-icons text-left">
                      <li class="social-icons-facebook" data-toggle="tooltip" title="Facebook"><a href="https://www.facebook.com/fitadelaco1/" target="_Blank"><i class="fa fa-facebook"></i></a></li>

                     <li class="social-icons-instagram" data-toggle="tooltip" title="Instagram"><a href="https://www.instagram.com/fitadelaco1/" target="_Blank"><i class="fa fa-instagram"></i></a></li>
                </ul>
        </div>
    </div>
</div>
</div>
</div>
    
<div class="container">
    <div class="row">
                <div class="col-md-8">
                    <h6>Formas de Pagamentos</h6>
                            <div class="banner_pagamentos">
                                <ul>
                                    <li class="" style="width:100%;"><img src="image/catalog/Banners/layout/banner_parcelamento_6x.png" alt="Pagamento por Depósito Bancário ou Cartões de Crédito pelo PagSeguro ou Pagar.me"></li>
                                </ul>
                            </div>
                </div><!-- col-md-8 -->
                <div class="col-md-4">
                    <h6>Formas de Envio</h6>
                         <div class="banner_pagamentos">
                                <ul>
                                    <li class=""><img src="catalog/view/images/exatos/correios.png?1" alt="Correios"></li>
                                    <li class=""><img src="catalog/view/images/exatos/pac.png?1" alt="PAC"></li>
                                    <li class=""><img src="catalog/view/images/exatos/sedex.png?1" alt="SEDEX"></li>
                                </ul>
                            </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-ms-12">
                    <div class="col-xs-12 col-sm-2">
                        <img src="_galerias/protegido_site_seguro.png" alt="Certificado de Segurança Digital" style="width:150px; margin:-14px 0 0 0;">
                    </div>
                    <div class="col-xs-12 col-sm-8 text-center">
                        <span class="copyrights">
                            <?php echo $enderecoLoja; ?>
                            <br/>
                            <?php echo $powered; ?><br/>
                        </span>
                    </div>

                    <div class="col-xs-12 col-sm-2 pull-right">
                        <b><?php echo $text_tecnologia; ?></b><br/>
                        <a href="http://www.innsystem.com.br" target="_Blank">
                            <img src="https://authentictenis.com.br/_galerias/logoinnsystem.webp" alt="InnSystem - Inovação em Sistemas" style="width:110px; margin:0px 0 0 0;">
                        </a>
                    </div>
                </div><!-- col-sm-12 -->        
            </div>

            
        </div>
    </div>

</footer>



<script src="catalog/view/theme/<?php echo $usedTpl; ?>/_template/js/bootstrap.min.js"></script>


<div id="box-privacity-ctn" class="box-privacity-ctn" style="display:none">
  <div class="box-privacity d-flex-column d-sm-flex-column d-md-flex align-items-center" id="box-privacity">
    <div class="flex-grow-1 mb-3 mb-sm-3 mb-md-0 mr-md-3">
      <p><b>Privacidade e os cookies</b>: a gente usa cookies para personalizar anúncios e melhorar a sua experiência no
        site. Ao continuar navegando, você concorda com a nossa <a href="/politica-de-privacidade">Política de
          Privacidade</a>.</p>
    </div>
    <div class="box-privacity-newsletter">
      <a href="javascript:buttonClose();" id="button-close" class="btn btn-primary btn-sm">Continuar e Fechar</a>
    </div>
  </div>
</div>

<script type='text/javascript'>
  $(function () {

    if ($.cookie('box-pop-up') == null) {

      $("#box-privacity").click(function (e) {
        e.stopPropagation();
      });

      $("#button-close").click(function (e) {
        $('#box-privacity-ctn').fadeOut();
      });

      $('#box-privacity-ctn').show();
    }

    var date = new Date();
    
    date.setTime(date.getTime() + (365 * 24 * 60 * 60)); //year
    // date.setTime(date.getTime() + (30 * 60 * 1000)); //30 min
    // date.setTime(date.getTime() + (10)); //10 segundos

    $.cookie('box-pop-up', 'box-pop', { expires: date });
  });
</script>
</div><!-- End .bodyWrap -->
</body></html>