<div class="row">
  <div class="col-sm-6">
    <h2><?php echo $text_new_customer; ?></h2>
    <p><?php echo $text_checkout; ?></p>
    <div class="radio">
      <label>
        <?php if ($account == 'register') { ?>
        <input type="radio" name="account" value="register" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="account" value="register" />
        <?php } ?>
        <?php echo $text_register; ?></label>
    </div>
    <?php if ($checkout_guest) { ?>
    <div class="radio">
      <label>
        <?php if ($account == 'guest') { ?>
        <input type="radio" name="account" value="guest" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="account" value="guest" />
        <?php } ?>
        <?php echo $text_guest; ?></label>
    </div>
    <?php } ?>
    <p><?php echo $text_register_account; ?></p>
    <input type="button" value="<?php echo $button_continue; ?>" id="button-account" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-custom" />
  </div>
  <div class="col-sm-6">
    <h2><?php echo $text_returning_customer; ?></h2>
    <p><?php echo $text_i_am_returning_customer; ?></p>
          <div class="input-group">
            <span class="input-group-addon"><span class="input-icon input-icon-email"></span><span class="input-text"><?php echo $entry_email; ?></span></span>
            <input type="email" required="" name="email" id="input-email" class="form-control input-lg" value="" placeholder="<?php echo $entry_email; ?>">
          </div><!-- End .input-group -->

          <div class="input-group xs-margin">
            <span class="input-group-addon"><span class="input-icon input-icon-password"></span><span class="input-text">Password*</span></span>
            <input type="password" required="" name="password" id="input-password" value="" placeholder="<?php echo $entry_password; ?>" class="form-control input-lg">
          </div><!-- End .input-group -->

     <a href="<?php echo $forgotten; ?>" class="btn btn-danger"><?php echo $text_forgotten; ?></a>
    <input type="button" value="<?php echo $button_login; ?>" id="button-login" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-custom" />
  </div>
</div>
