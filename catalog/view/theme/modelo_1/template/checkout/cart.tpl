<?php echo $header; ?>

<div class="container">

  <div id="category-breadcrumb">

    <div class="container">

      <ul class="breadcrumb">

        <?php foreach ($breadcrumbs as $breadcrumb) { ?>

        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

        <?php } ?>

      </ul>

    </div>

  </div><!-- category-breadcrumb -->



  <?php if ($attention) { ?>

  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>

    <button type="button" class="close" data-dismiss="alert">&times;</button>

  </div>

  <?php } ?>

  <?php if ($success) { ?>

  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>

    <button type="button" class="close" data-dismiss="alert">&times;</button>

  </div>

  <?php } ?>

  <?php if ($error_warning) { ?>

  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>

    <button type="button" class="close" data-dismiss="alert">&times;</button>

  </div>

  <?php } ?>

  <div class="row"><?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      <header class="content-title">

            <h1 class="title"><?php echo $heading_title; ?></h1>

            <?php if ($weight) { ?>

             <p>(<?php echo $weight; ?>)</p>

            <?php } ?>    

      </header>

      

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">

        <div class="table-responsive">

          <table class="table table-bordered table-cart">

            <thead>

              <tr>

                <td class="text-center" width="5%"><?php echo $column_image; ?></td>

                <td class="text-left"><?php echo $column_name; ?></td>

                <td class="text-left"width="15%"><?php echo $column_model; ?></td>

                <td class="text-left" width="20%"><?php echo $column_quantity; ?></td>

                <td class="text-right"width="10%"><?php echo $column_price; ?></td>

                <td class="text-right" width="12%"><?php echo $column_total; ?></td>

              </tr>

            </thead>

            <tbody>

              <?php foreach ($products as $product) { ?>

              <tr>

                <td class="text-center" width="5%"><?php if ($product['thumb']) { ?>

                  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>

                  <?php } ?></td>

                <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>

                  <?php if (!$product['stock']) { ?>

                  <span class="text-danger">***</span>

                  <?php } ?>

                  <?php if ($product['option']) { ?>

                  <?php foreach ($product['option'] as $option) { ?>

                  <br />

                  <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>

                  <?php } ?>

                  <?php } ?>

                  <?php if ($product['reward']) { ?>

                  <br />

                  <small><?php echo $product['reward']; ?></small>

                  <?php } ?>

                  <?php if ($product['recurring']) { ?>

                  <br />

                  <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>

                  <?php } ?></td>

                <td class="text-left"><?php echo $product['model']; ?></td>

                <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">

                    <input type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control input-sm" />

                    <span class="input-group-btn">

                    <button type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="btn btn-sm btn-custom" style="margin:0px 0 0 0;padding:6.5px 12px;"><i class="fa fa-refresh"></i></button>

                    <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-sm btn-danger" style="margin:0px 0 0 0;padding:6.5px 12px;" onclick="cart.remove('<?php echo $product['key']; ?>');"><i class="fa fa-times-circle"></i></button></span></div></td>

                <td class="text-right"><?php echo $product['price']; ?></td>

                <td class="text-right" style="font-weight:bold;"><?php echo $product['total']; ?></td>

              </tr>

              <?php } ?>

              <?php foreach ($vouchers as $vouchers) { ?>

              <tr>

                <td></td>

                <td class="text-left"><?php echo $vouchers['description']; ?></td>

                <td class="text-left"></td>

                <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">

                    <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />

                    <span class="input-group-btn"><button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $vouchers['key']; ?>');"><i class="fa fa-times-circle"></i></button></span></div></td>

                <td class="text-right"><?php echo $vouchers['amount']; ?></td>

                <td class="text-right"><?php echo $vouchers['amount']; ?></td>

              </tr>

              <?php } ?>

            </tbody>

          </table>

        </div>

      </form>

      <?php if ($coupon || $voucher || $reward || $shipping) { ?>

      <header class="content-title" style="margin-top:1em;">

            <h1 class="title"><?php echo $text_next; ?></h1>

             <p><?php echo $text_next_choice; ?></p>    

      </header>     

      <div class="row">

        <div class="col-md-8 col-sm-12 col-xs-12">





      <div class="tab-container left">

        <ul class="nav-tabs">

          <li class="active"><a href="#shipping" data-toggle="tab" style="font-size:14px; min-width:220px;">Calcular o Frete</a></li>

          <li><a href="#discount" data-toggle="tab" style="font-size:14px; min-width:220px;">Cupom de Desconto</a></li>

          <li><a href="#gift" data-toggle="tab" style="font-size:14px; min-width:220px;">Vale Presente</a></li>

          

        </ul>

            <div class="tab-content" style="padding:0 50px 0 250px">

              <div class="tab-pane active" id="shipping">

                <?php echo $shipping; ?>                  

              </div><!-- End .tab-pane -->

              

              <div class="tab-pane" id="discount">

                <?php echo $coupon; ?>                  

              </div><!-- End .tab-pane -->

              

              <div class="tab-pane" id="gift">

               <?php echo $voucher; ?>

              </div><!-- End .tab-pane -->



             <?php echo $reward; ?>

              

            </div><!-- End .tab-content -->

        </div><!-- End .tab-container -->

      </div><!-- col-md-8 col-sm-12 col-xs-12 -->



    



      <?php } ?>



        <div class="col-md-4 col-sm-12 col-xs-12">

          <table class="table total-table" style="font:13px Arial, Tahoma, Verdana;">

            <?php foreach ($totals as $total) { ?>

            <tr>

              <td class="text-right"><strong><?php echo $total['title']; ?>:</strong></td>

              <td class="text-right"><?php echo $total['text']; ?></td>

            </tr>

            <?php } ?>

          </table>

          <div class="buttons">

            <div class="pull-left"><a href="<?php echo $continue; ?>" class="btn btn-custom-2"><?php echo $button_shopping; ?></a></div>

            <div class="pull-right"><a href="<?php echo $checkout; ?>" class="btn btn-custom"><?php echo $button_checkout; ?></a></div>

          </div>

        </div>

    </div><!-- row -->

      

      <?php echo $content_bottom; ?></div>

    <?php echo $column_right; ?></div>

</div>

<?php echo $footer; ?> 