<div class="col-sm-12" style="margin:1em 0;">
	<header class="content-title">
            <h1 class="title"><?php echo $text_instruction; ?></h1>
            <p><b><?php echo $text_description; ?></b></p>
      </header>


<blockquote>

  <p><?php echo $bank; ?></p>

  <p><small><?php echo $text_payment; ?></small></p>

</blockquote>

<div class="buttons">

  <div class="pull-right">

    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-custom" />

  </div>

</div>

<script type="text/javascript"><!--

$('#button-confirm').on('click', function() {

	$.ajax({ 

		type: 'get',

		url: 'index.php?route=payment/bank_transfer/confirm',

		cache: false,

		beforeSend: function() {

			$('#button-confirm').button('loading');

		},

		complete: function() {

			$('#button-confirm').button('reset');

		},		

		success: function() {

			location = '<?php echo $continue; ?>';

		}		

	});

});

//--></script> 

</div><!-- col-sm-12 -->