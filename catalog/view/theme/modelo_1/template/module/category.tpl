
      <aside class="col-md-12 col-sm-12 col-xs-12 sidebar">      

  <div class="panel-group custom-accordion sm-accordion" id="category-filter">

           <div class="panel">


                <div class="accordion-header">


                  <div class="accordion-title"><span>Categorias</span></div><!-- End .accordion-title -->


                    <a class="accordion-btn opened" data-toggle="collapse" data-target="#category-list-1"></a>


                  </div><!-- End .accordion-header -->


                


                  <div id="category-list-1" class="collapse in">


                      <div class="panel-body">




  <?php foreach ($categories as $category) { ?>
<ul class="category-filter-list jscrollpane">

  <?php if ($category['category_id'] == $category_id) { ?>

  <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>

  <?php if ($category['children']) { ?>

  <?php foreach ($category['children'] as $child) { ?>

  <?php if ($child['category_id'] == $child_id) { ?>

  <li><a href="<?php echo $child['href']; ?>">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a></li>
  <?php } else { ?>

  <li><a href="<?php echo $child['href']; ?>">&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a></li>

  <?php } ?>

  <?php } ?>

  <?php } ?>

  <?php } else { ?>

  <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>

  <?php } ?>
</ul>
  <?php } ?>

</div>
</div>
</div>
</div>

</aside>
