 
      <aside class="col-md-12 col-sm-12 col-xs-12 sidebar">      



        <div class="panel-group custom-accordion sm-accordion" id="category-filter">




            <div class="panel">


                <div class="accordion-header">


                  <div class="accordion-title"><span>Conta Afiliado</span></div><!-- End .accordion-title -->


                    <a class="accordion-btn opened" data-toggle="collapse" data-target="#category-list-1"></a>


                  </div><!-- End .accordion-header -->


                


                  <div id="category-list-1" class="collapse in">


                      <div class="panel-body">


                      <ul class="category-filter-list jscrollpane">

                         <?php if (!$logged) { ?>

  <li><a href="<?php echo $login; ?>"><i class="fa fa-user"></i> <?php echo $text_login; ?></a></li>
  <li><a href="<?php echo $register; ?>"><i class="fa fa-user-plus"></i> <?php echo $text_register; ?></a></li>
  <li><a href="<?php echo $forgotten; ?>" ><i class="fa fa-key"></i> <?php echo $text_forgotten; ?></a></li>

  <?php } ?>

  <li><a href="<?php echo $account; ?>"><i class="fa fa-user"></i> <?php echo $text_account; ?></a></li>

  <?php if ($logged) { ?>

  <li><a href="<?php echo $edit; ?>"><i class="fa fa-edit"></i> <?php echo $text_edit; ?></a></li>
  <li><a href="<?php echo $password; ?>"><i class="fa fa-key"></i> <?php echo $text_password; ?></a></li>

  <?php } ?>

  <li><a href="<?php echo $payment; ?>"><i class="fa fa-info-circle"></i> <?php echo $text_payment; ?></a></li>
  <li><a href="<?php echo $tracking; ?>"><i class="fa fa-cogs"></i> <?php echo $text_tracking; ?></a></li>
  <li><a href="<?php echo $transaction; ?>"><i class="fa fa-credit-card"></i> <?php echo $text_transaction; ?></a></li>

  <?php if ($logged) { ?>

  <li><a href="<?php echo $logout; ?>"><i class="fa fa-sign-out"></i> <?php echo $text_logout; ?></a></li>

  <?php } ?>                       

                      </ul>


                  </div><!-- End .panel-body -->


                </div><!-- #collapse -->


              </div><!-- End .panel -->


                       
        </div><!-- panel-group -->


      </aside>






