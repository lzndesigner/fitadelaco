                              <div class="category-item-container latest-items carousel-wrapper">


                                    <header class="content-title">


                                        <div class="title-bg">


                                            <h2 class="title"><?php echo $heading_title; ?></h2>


                                        </div><!-- End .title-bg -->


                                        <!-- <p class="title-desc">Only with us you can get a new model with a discount.</p> -->


                                    </header>


                                    

                                     <div class="latest-items-slider owl-carousel">


                                            <?php foreach ($products as $product) { ?>


                                        <div class="item">                                            


                                              <div class="item-image-container">


                                                <figure>


                                                    <a href="<?php echo $product['href']; ?>">


                                                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="item-image">


                                                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="item-image-hover">


                                                    </a>


                                                </figure>


                                                <?php if ($product['price']) { ?>


                                                  <div class="item-price-container">


                                                    <?php if (!$product['special']) { ?>


                                                     <span class="item-price"><?php echo $product['price']; ?></span>


                                                    <?php } else { ?>


                                                    <span class="old-price"><?php echo $product['price']; ?></span>


                                                    <span class="item-price"><?php echo $product['special']; ?></span>


                                                    <?php } ?>


                                                  </div>


                                                  <?php } ?>


                                                 


                                                </div><!-- End .item-image -->


                                              <div class="item-meta-container">                                               


                                                    <h3 class="item-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>


                                                    <div class="item-action">


                                                       <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="item-add-btn">


                                                            <span class="icon-cart-text"><?php echo $button_cart; ?></span>


                                                        </button>


                                                        <div class="item-action-inner">


                                                            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-like"><?php echo $button_wishlist; ?></button>


                                                            <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-compare"><?php echo $button_compare; ?></button>


                                                        </div><!-- End .item-action-inner -->


                                                    </div><!-- End .item-action -->
                                                    <?php  if ($product['shipping'] == 0) {echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:110px; border-radius:5px; margin:.5em auto;' />";}?>


                                            </div><!-- End .item-meta-container --> 


                                       </div><!-- End .item -->


                                        <?php } ?>





                                   </div><!--latest-items-slider -->


                                   


                              </div><!-- End .latest-items -->