<?php echo $header; ?>

<div class="container">

  <div id="category-breadcrumb">

    <div class="container">

      <ul class="breadcrumb">

        <?php foreach ($breadcrumbs as $breadcrumb) { ?>

        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

        <?php } ?>

      </ul>

    </div>

  </div><!-- category-breadcrumb -->



  <div class="row"><?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

       <div class="col-md-12 col-sm-12 col-xs-12 main-content">

        <header class="content-title">

            <h1 class="title"><?php echo $heading_title; ?></h1>     

            <p><?php echo $text_password; ?></p>     

        </header>



      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">

        <fieldset>

       <ul style="padding:0 1.5em 1.5em; list-style:inside;">

        <?php if ($error_password) { ?>

          <li class="text-danger"><?php echo $error_password; ?></li>

        <?php } ?>

      </ul>

      



                          <div class="input-group required">

                            <span class="input-group-addon"><span class="input-icon input-icon-password"></span><span class="input-text"><?php echo $entry_password; ?></span></span>

                            <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control input-lg">

                          </div><!-- End .input-group -->

                          <div class="input-group">

                            <span class="input-group-addon"><span class="input-icon input-icon-password"></span><span class="input-text"><?php echo $entry_confirm; ?></span></span>

                            <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control input-lg">

                          </div><!-- End .input-group -->

        </fieldset>

        <div class="buttons clearfix">

          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>

          <div class="pull-right">

            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-custom" />

          </div>

        </div>

      </form>

    </div>

      <?php echo $content_bottom; ?></div>

    <?php echo $column_right; ?></div>

</div>

<?php echo $footer; ?> 