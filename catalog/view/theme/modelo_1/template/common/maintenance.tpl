<?php echo $header; ?>
<section id="content" class="no-content">
        	
        	<div class="lg-margin"></div><!-- Space -->
        	<div class="container">
        		<div class="row">
        			<div class="col-md-12">
        				<div class="no-content-comment">
                           <h2>Manutenção</h2>
                           <?php echo $message; ?>
                        </div><!-- End .no-content-comment -->
        			</div><!-- End .col-md-12 -->
        		</div><!-- End .row -->
			</div><!-- End .container -->
        
        </section>

<?php echo $footer; ?>