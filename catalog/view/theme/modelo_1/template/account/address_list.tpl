<?php echo $header; ?>

<div class="container">

  <div id="category-breadcrumb">

    <div class="container">

      <ul class="breadcrumb">

        <?php foreach ($breadcrumbs as $breadcrumb) { ?>

        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

        <?php } ?>

      </ul>

    </div>

  </div><!-- category-breadcrumb -->



  <?php if ($success) { ?>

  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>

  <?php } ?>

  <?php if ($error_warning) { ?>

  <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>

  <?php } ?>

  <div class="row"><?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      <div class="col-md-12 col-sm-12 col-xs-12 main-content">

        <header class="content-title">

            <h1 class="title"><?php echo $text_address_book; ?></h1>     



        </header>

      <?php if ($addresses) { ?>

      <div class="row">

        <?php foreach ($addresses as $result) { ?>

        <div class="col-md-4 col-sm-4 col-xs-12">

          <ul style="border-left:2px solid #DDD; padding:.5em 1em; margin-bottom:1em; border-radius:5px;">

            <?php echo $result['address']; ?>



            <li style="margin-top:.5em;">

            <a href="<?php echo $result['update']; ?>" class="btn btn-custom"><?php echo $button_edit; ?></a>

            <a href="<?php echo $result['delete']; ?>" class="btn btn-danger"><?php echo $button_delete; ?></a>

          </li>

          </ul>



          

        </div>

        <?php } ?>

      </div>

      <?php } else { ?>

      <p><?php echo $text_empty; ?></p>

      <?php } ?>

      <div class="buttons clearfix">

        <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>

        <div class="pull-right"><a href="<?php echo $add; ?>" class="btn btn-custom"><?php echo $button_new_address; ?></a></div>

      </div>

    </div>

      <?php echo $content_bottom; ?></div>

    <?php echo $column_right; ?></div>

</div>

<?php echo $footer; ?>