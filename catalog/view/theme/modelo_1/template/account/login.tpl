<?php echo $header; ?>



<div class="container">







<div id="category-breadcrumb">



  <div class="container">



    <ul class="breadcrumb">



      <?php foreach ($breadcrumbs as $breadcrumb) { ?>



      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>



      <?php } ?>



    </ul>



  </div>



</div>







  <?php if ($success) { ?>



  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>



  <?php } ?>



  <?php if ($error_warning) { ?>



  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>



  <?php } ?>



  <div class="row"><?php echo $column_left; ?>



    <?php if ($column_left && $column_right) { ?>



    <?php $class = 'col-sm-6'; ?>



    <?php } elseif ($column_left || $column_right) { ?>



    <?php $class = 'col-sm-9'; ?>



    <?php } else { ?>



    <?php $class = 'col-sm-12'; ?>



    <?php } ?>



    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>




        <div class="col-sm-6 col-sm-6 col-xs-12">



          <h2 class="checkout-title"><?php echo $text_new_customer; ?></h2>



            <p><?php echo $text_register; ?></p>



            <p><?php echo $text_register_account; ?></p>



            <a href="<?php echo $register; ?>" class="btn btn-custom"><?php echo $button_continue; ?></a>



            <div class="xs-margin"></div>            



        </div>



        <div class="col-md-6 col-sm-6 col-xs-12">               



            <h2 class="checkout-title"><?php echo $text_returning_customer; ?></h2>



            <p><?php echo $text_i_am_returning_customer; ?></p>



            <div class="xs-margin"></div>







            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">



                  <div class="input-group">



                    <span class="input-group-addon"><span class="input-icon input-icon-email"></span><span class="input-text"><?php echo $entry_email; ?></span></span>



                    <input type="email" required="" name="email" id="input-email" class="form-control input-lg" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>">



                  </div><!-- End .input-group -->







                  <div class="input-group xs-margin">



                    <span class="input-group-addon"><span class="input-icon input-icon-password"></span><span class="input-text">Password*</span></span>



                    <input type="password" required="" name="password" id="input-password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" class="form-control input-lg">



                  </div><!-- End .input-group -->







                  <span class="pull-right text-right">



                    <a href="<?php echo $forgotten; ?>" class="btn btn-danger"><?php echo $text_forgotten; ?></a>



                  </span>



                <div class="input-group pull-left">



                  <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-custom" />



                  <?php if ($redirect) { ?>



                  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />



                  <?php } ?>         



              </div><!-- End .input-group -->



            </form>



        </div><!-- col-md-6 -->





    </div>



      <?php echo $content_bottom; ?>



    <?php echo $column_right; ?></div>



</div>



<?php echo $footer; ?>