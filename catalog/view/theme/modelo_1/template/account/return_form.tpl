<?php echo $header; ?>

<div class="container">

  <div id="category-breadcrumb">

    <div class="container">

      <ul class="breadcrumb">

        <?php foreach ($breadcrumbs as $breadcrumb) { ?>

        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

        <?php } ?>

      </ul>

    </div>

  </div><!-- category-breadcrumb -->

  

  <?php if ($error_warning) { ?>

  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>

  <?php } ?>

  <div class="row"><?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

       <header class="content-title">

        <h1 class="title"><?php echo $heading_title; ?></h1>

        <p class="title-desc"><?php echo $text_description; ?></p>

      </header>

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">

        <fieldset>



   <ul style="padding:1.5em; list-style:inside;">

        <?php if ($error_firstname) { ?>

         <li class="text-danger"><?php echo $error_firstname; ?></li>

        <?php } ?>  

        <?php if ($error_lastname) { ?>

         <li class="text-danger"><?php echo $error_lastname; ?></li>

        <?php } ?>

        <?php if ($error_email) { ?>

         <li class="text-danger"><?php echo $error_email; ?></li>

        <?php } ?>

        <?php if ($error_telephone) { ?>

         <li class="text-danger"><?php echo $error_telephone; ?></li>

        <?php } ?>

        <?php if ($error_order_id) { ?>

        <li class="text-danger"><?php echo $error_order_id; ?></li>

        <?php } ?>

        <?php if ($error_product) { ?>

          <li class="text-danger"><?php echo $error_product; ?></li>

        <?php } ?>

        <?php if ($error_model) { ?>

          <li class="text-danger"><?php echo $error_model; ?></li>

        <?php } ?>        

        <?php if ($error_reason) { ?>

          <li class="text-danger"><?php echo $error_reason; ?></li>

        <?php } ?>

        <?php if ($error_captcha) { ?>

          <li class="text-danger"><?php echo $error_captcha; ?></li>

        <?php } ?>

      </ul>

          

                  <div class="input-group">

                    <span class="input-group-addon"><span class="input-icon input-icon-user"></span><span class="input-text"><?php echo $entry_firstname; ?></span></span>

                    <input type="text"  name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control input-lg">    

                  </div><!-- End .input-group -->





                  <div class="input-group">

                    <span class="input-group-addon"><span class="input-icon input-icon-user"></span><span class="input-text"><?php echo $entry_lastname; ?></span></span>

                    <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control input-lg" >

                  </div><!-- End .input-group -->



                  <div class="input-group">

                            <span class="input-group-addon"><span class="input-icon input-icon-email"></span><span class="input-text"><?php echo $entry_email; ?></span></span>

                            <input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control input-lg">

                          </div><!-- End .input-group -->



                  <div class="input-group">

                    <span class="input-group-addon"><span class="input-icon input-icon-phone"></span><span class="input-text"><?php echo $entry_telephone; ?></span></span>

                    <input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control input-lg">

                  </div><!-- End .input-group -->



                  <div class="input-group">

                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_order_id; ?></span></span>

                    <input type="text" name="order_id" value="<?php echo $order_id; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" class="form-control input-lg">

                  </div><!-- End .input-group -->



                  <div class="input-group">

                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_date_ordered; ?></span></span>

                     <div class="date"><input type="text" name="date_ordered" value="<?php echo $date_ordered; ?>" placeholder="<?php echo $entry_date_ordered; ?>" data-date-format="YYYY-MM-DD" id="input-date-ordered" class="form-control input-lg" style="width:40%;" /><span class="input-group-btn">

                <button type="button" class="btn btn-default btn-lg" style="margin:.08em 0;"><i class="fa fa-calendar"></i></button>

                </span></div>

                  </div><!-- End .input-group -->

     

          

        </fieldset>

        <fieldset>

         <header class="content-title">

            <h1 class="title"><?php echo $text_product; ?></h1>

          </header>



           <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_product; ?></span></span>

            <input type="text" name="product" value="<?php echo $product; ?>" placeholder="<?php echo $entry_product; ?>" id="input-product" class="form-control input-lg">    

          </div><!-- End .input-group -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_model; ?></span></span>

            <input type="text" name="model" value="<?php echo $model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control input-lg">    

          </div><!-- End .input-group -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_quantity; ?></span></span>

            <input type="text" name="quantity" value="<?php echo $quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control input-lg">    

          </div><!-- End .input-group -->



          <div class="row">

            <div class="col-md-4">

              <div class="input-group col-md-12">

                <span class="input-text"><b><?php echo $entry_reason; ?></b></span>

                <?php foreach ($return_reasons as $return_reason) { ?>

                  <?php if ($return_reason['return_reason_id'] == $return_reason_id) { ?>

                  <div class="radio">

                    <label>

                      <input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" checked="checked" />

                      <?php echo $return_reason['name']; ?></label>

                  </div>

                  <?php } else { ?>

                  <div class="radio">

                    <label>

                      <input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" />

                      <?php echo $return_reason['name']; ?></label>

                  </div>

                  <?php  } ?>

                  <?php  } ?>

              </div><!-- End .input-group -->



                  <div class="input-group col-md-12">

                    <span class="input-text"><b><?php echo $entry_opened; ?></b></span>

                    <br/>

                      <label class="radio-inline">

                        <?php if ($opened) { ?>

                        <input type="radio" name="opened" value="1" checked="checked" />

                        <?php } else { ?>

                        <input type="radio" name="opened" value="1" />

                        <?php } ?>

                        <?php echo $text_yes; ?></label>

                      <label class="radio-inline">

                        <?php if (!$opened) { ?>

                        <input type="radio" name="opened" value="0" checked="checked" />

                        <?php } else { ?>

                        <input type="radio" name="opened" value="0" />

                        <?php } ?>

                        <?php echo $text_no; ?></label>

                  </div><!-- End .input-group -->

            </div><!-- col-md-4 -->

            <div class="col-md-8">



                <div class="input-group col-md-12">

                  <span class="input-text"><b><?php echo $entry_fault_detail; ?></b></span>

                  <br/>

                  <textarea name="comment" rows="10" placeholder="<?php echo $entry_fault_detail; ?>" id="input-comment" class="form-control input-lg"><?php echo $comment; ?></textarea>    

                </div><!-- End .input-group -->



            </div><!-- col-md-8 -->

          </div><!-- row -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_captcha; ?></span></span>

            <input type="text" name="captcha" value="" placeholder="<?php echo $entry_captcha; ?>" id="input-captcha" class="form-control input-lg" style="width:40%;">   

            <img src="index.php?route=tool/captcha" alt="" style="margin:0.4em 0;" /> 

          </div><!-- End .input-group -->



          



          



         



        </fieldset>

        <?php if ($text_agree) { ?>

        <div class="buttons clearfix">

          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-danger"><?php echo $button_back; ?></a></div>

          <div class="pull-right"><?php echo $text_agree; ?>

            <?php if ($agree) { ?>

            <input type="checkbox" name="agree" value="1" checked="checked" />

            <?php } else { ?>

            <input type="checkbox" name="agree" value="1" />

            <?php } ?>

            <input type="submit" value="<?php echo $button_submit; ?>" class="btn btn-custom" />

          </div>

        </div>

        <?php } else { ?>

        <div class="buttons clearfix">

          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>

          <div class="pull-right">

            <input type="submit" value="<?php echo $button_submit; ?>" class="btn btn-custom" />

          </div>

        </div>

        <?php } ?>

      </form>

      <?php echo $content_bottom; ?></div>

    <?php echo $column_right; ?></div>

</div>

<script type="text/javascript"><!--

$('.date').datetimepicker({

	pickTime: false

});

//--></script>

<?php echo $footer; ?>

