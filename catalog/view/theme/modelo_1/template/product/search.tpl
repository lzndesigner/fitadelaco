<?php echo $header; ?>

<div class="container">


  <div id="category-breadcrumb">



  <ul class="breadcrumb">



    <?php foreach ($breadcrumbs as $breadcrumb) { ?>



    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>



    <?php } ?>



  </ul>



</div>

  <div class="row"><?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      <header class="content-title">
         <h6 class="title"><?php echo $heading_title; ?></h6>
      </header>

      <fieldset>
      <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>

      <div class="row">

        <div class="col-sm-4">

          <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />

        </div>

        <div class="col-sm-3">

          <select name="category_id" class="form-control">

            <option value="0"><?php echo $text_category; ?></option>

            <?php foreach ($categories as $category_1) { ?>

            <?php if ($category_1['category_id'] == $category_id) { ?>

            <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>

            <?php } else { ?>

            <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>

            <?php } ?>

            <?php foreach ($category_1['children'] as $category_2) { ?>

            <?php if ($category_2['category_id'] == $category_id) { ?>

            <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>

            <?php } else { ?>

            <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>

            <?php } ?>

            <?php foreach ($category_2['children'] as $category_3) { ?>

            <?php if ($category_3['category_id'] == $category_id) { ?>

            <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>

            <?php } else { ?>

            <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>

            <?php } ?>

            <?php } ?>

            <?php } ?>

            <?php } ?>

          </select>

        </div>

        <div class="col-sm-3">

          <label class="checkbox-inline">

            <?php if ($sub_category) { ?>

            <input type="checkbox" name="sub_category" value="1" checked="checked" />

            <?php } else { ?>

            <input type="checkbox" name="sub_category" value="1" />

            <?php } ?>

            <?php echo $text_sub_category; ?></label>

        </div>

      </div>

      <p>

        <label class="checkbox-inline">

          <?php if ($description) { ?>

          <input type="checkbox" name="description" value="1" id="description" checked="checked" />

          <?php } else { ?>

          <input type="checkbox" name="description" value="1" id="description" />

          <?php } ?>

          <?php echo $entry_description; ?></label>

      </p>

      <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-custom" />
    </fieldset>
      <header class="content-title">
         <h6 class="title"><?php echo $text_search; ?></h6>
       </header>

      <?php if ($products) { ?>

      <p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p>





      <div class="row">

          <div class="category-toolbar clearfix">



                  <div class="toolbox-filter clearfix">



                    







                    <div class="view-box" style="display:none;">



                      <button type="button" id="list-view" class="icon-button icon-list" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th"></i></button>



                      <button type="button" id="grid-view" class="icon-button icon-grid" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th-list"></i></button>



                    </div><!-- End .view-box -->











                    <div class="sort-box">



                      <span class="separator"><?php echo $text_sort; ?></span>



                      <div class="btn-group select-dropdown">



                        <select id="input-sort" class="select-btn" onchange="location = this.value;">



                          <?php foreach ($sorts as $sorts) { ?>



                          <?php if ($sorts['value'] == $sort . '-' . $order) { ?>



                          <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>



                          <?php } else { ?>



                          <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>



                          <?php } ?>



                          <?php } ?>



                        </select>



                      </div>



                    </div>



                    



                  </div><!-- End .toolbox-filter -->



                  <div class="toolbox-pagination">



                    <div class="view-count-box">



                      <span class="separator"><?php echo $text_limit; ?></span>



                      <div class="btn-group select-dropdown">



                        <select id="input-limit" class="select-btn" onchange="location = this.value;">



                          <?php foreach ($limits as $limits) { ?>



                          <?php if ($limits['value'] == $limit) { ?>



                          <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>



                          <?php } else { ?>



                          <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>



                          <?php } ?>



                          <?php } ?>



                        </select>



                      </div>



                    </div><!-- End .view-count-box -->



                    



                  </div><!-- End .toolbox-pagination -->

        </div><!-- End .category-toolbar -->


      </div>

      <br />

      <div class="row">

        <?php foreach ($products as $product) { ?>

      
        <div class="product-layout product-list col-xs-12">



          <div class="item">



            <div class="item-image-container">



                <figure>



                  <a href="<?php echo $product['href']; ?>">



                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="item-image">



                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="item-image-hover">



                  </a>



                </figure>



                 


            </div>







            <div class="item-meta-container">



                <h3 class="item-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>



                <div class="item-action">



                  <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="item-add-btn">



                    <span class="icon-cart-text" style="display: inline-block;"><?php echo $button_cart; ?></span>



                  </button>



                  <div class="item-action-inner" style="visibility: hidden; overflow: hidden; width: 0px;">



                     <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-like"></button>



                      <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-compare"></button>



                  </div><!-- End .item-action-inner -->



                </div><!-- End .item-action -->
                 <?php  if ($product['shipping'] == 0) {echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:110px; border-radius:5px; margin:.5em auto;' />";}?>



              </div>







          </div><!-- item -->          



        </div>


        <?php } ?>

      </div>

      <div class="row">

        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>

        <div class="col-sm-6 text-right"><?php echo $results; ?></div>

      </div>

      <?php } else { ?>

      <p><?php echo $text_empty; ?></p>

      <?php } ?>

      <?php echo $content_bottom; ?></div>

    <?php echo $column_right; ?></div>

</div>

<script type="text/javascript"><!--

$('#button-search').bind('click', function() {

	url = 'index.php?route=product/search';

	

	var search = $('#content input[name=\'search\']').prop('value');

	

	if (search) {

		url += '&search=' + encodeURIComponent(search);

	}



	var category_id = $('#content select[name=\'category_id\']').prop('value');

	

	if (category_id > 0) {

		url += '&category_id=' + encodeURIComponent(category_id);

	}

	

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	

	if (sub_category) {

		url += '&sub_category=true';

	}

		

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	

	if (filter_description) {

		url += '&description=true';

	}



	location = url;

});



$('#content input[name=\'search\']').bind('keydown', function(e) {

	if (e.keyCode == 13) {

		$('#button-search').trigger('click');

	}

});



$('select[name=\'category_id\']').on('change', function() {

	if (this.value == '0') {

		$('input[name=\'sub_category\']').prop('disabled', true);

	} else {

		$('input[name=\'sub_category\']').prop('disabled', false);

	}

});



$('select[name=\'category_id\']').trigger('change');

--></script> 

<?php echo $footer; ?> 