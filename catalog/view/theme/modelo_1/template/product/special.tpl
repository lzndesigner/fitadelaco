<?php echo $header; ?>

<div class="container">


  <div id="category-breadcrumb">



  <ul class="breadcrumb">



    <?php foreach ($breadcrumbs as $breadcrumb) { ?>



    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>



    <?php } ?>



  </ul>



</div>

  <div class="row"><?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
  <header class="content-title">



         <h6 class="title"><?php echo $heading_title; ?></h6>



      </header>

      <?php if ($products) { ?>

      <p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p>

      <div class="row">


          <div class="category-toolbar clearfix">



                  <div class="toolbox-filter clearfix">



                    







                    <div class="view-box" style="display:none;">


                      
                      <button type="button" id="list-view" class="icon-button icon-list" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th"></i></button>



                      <button type="button" id="grid-view" class="icon-button icon-grid" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th-list"></i></button>



                    </div><!-- End .view-box -->











                    <div class="sort-box">



                      <span class="separator"><?php echo $text_sort; ?></span>



                      <div class="btn-group select-dropdown">



                        <select id="input-sort" class="select-btn" onchange="location = this.value;">



                          <?php foreach ($sorts as $sorts) { ?>



                          <?php if ($sorts['value'] == $sort . '-' . $order) { ?>



                          <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>



                          <?php } else { ?>



                          <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>



                          <?php } ?>



                          <?php } ?>



                        </select>



                      </div>



                    </div>



                    



                  </div><!-- End .toolbox-filter -->



                  <div class="toolbox-pagination">



                    <div class="view-count-box">



                      <span class="separator"><?php echo $text_limit; ?></span>



                      <div class="btn-group select-dropdown">



                        <select id="input-limit" class="select-btn" onchange="location = this.value;">



                          <?php foreach ($limits as $limits) { ?>



                          <?php if ($limits['value'] == $limit) { ?>



                          <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>



                          <?php } else { ?>



                          <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>



                          <?php } ?>



                          <?php } ?>



                        </select>



                      </div>



                    </div><!-- End .view-count-box -->



                    



                  </div><!-- End .toolbox-pagination -->



                      



                      



     </div><!-- End .category-toolbar -->


      </div>

      <br />

      <div class="row">  

        <?php foreach ($products as $product) { ?>     


 <div class="product-layout product-list col-xs-12">



          <div class="item">



            <div class="item-image-container">



                <figure>



                  <a href="<?php echo $product['href']; ?>">



                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="item-image">



                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="item-image-hover">



                  </a>



                </figure>



                  <?php if ($product['price']) { ?>



                    <div class="item-price-container">



                      <?php if (!$product['special']) { ?>



                      <span class="item-price"> <?php echo $product['price']; ?> </span>



                      <?php } else { ?>



                      <span class="old-price"><?php echo $product['price']; ?></span>



                      <span class="item-price"><?php echo $product['special']; ?></span>



                      <?php } ?>



                    </div>



                  <?php } ?>



            </div>







            <div class="item-meta-container">



                <h3 class="item-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>



                <div class="item-action">



                  <button type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="item-add-btn">



                    <span class="icon-cart-text" style="display: inline-block;"><?php echo $button_cart; ?></span>



                  </button>



                  <div class="item-action-inner" style="visibility: hidden; overflow: hidden; width: 0px;">



                     <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-like"></button>



                      <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-compare"></button>



                  </div><!-- End .item-action-inner -->



                </div><!-- End .item-action -->
                <?php  if ($product['shipping'] == 0) {echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:110px; border-radius:5px; margin:.5em auto;' />";}?>



              </div>







          </div><!-- item -->          



        </div>





             

           

        <?php } ?>

      </div>

      <div class="row">

        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>

        <div class="col-sm-6 text-right"><?php echo $results; ?></div>

      </div>

      <?php } else { ?>

      <p><?php echo $text_empty; ?></p>

      <div class="buttons">

        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-custom"><?php echo $button_continue; ?></a></div>

      </div>

      <?php } ?>

      <?php echo $content_bottom; ?></div>

    <?php echo $column_right; ?></div>

</div>

<?php echo $footer; ?>