<?php
class ControllerInformationConfirmarPagamento extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('information/confirmarpagamento');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/confirmarpagamento')
		);

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_name'] = $this->language->get('text_name');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_pedido'] = $this->language->get('text_pedido');
		$data['text_anexo'] = $this->language->get('text_anexo');
		$data['text_anexo_acc'] = $this->language->get('text_anexo_acc');
		$data['text_notas'] = $this->language->get('text_notas');

		$data['email_admin'] = $this->config->get('config_email');

		$data['text_location'] = $this->language->get('text_location');
		$data['text_store'] = $this->language->get('text_store');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_open'] = $this->language->get('text_open');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_enquiry'] = $this->language->get('entry_enquiry');
		$data['entry_captcha'] = $this->language->get('entry_captcha');

		$data['button_map'] = $this->language->get('button_map');

		
		$data['button_submit'] = $this->language->get('button_submit');
	
	
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/confirmarpagamento.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/confirmarpagamento.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/information/confirmarpagamento.tpl', $data));
		}
	}

	

	
}
