<?php
class ControllerModuleRandom extends Controller {
	public function index($setting) {
		$this->load->language('module/random');

		$data['heading_title'] = $setting['name'];

		$data['text_tax'] = $this->language->get('text_tax');
    $data['text_instock'] = $this->language->get('text_instock');
    $data['button_visit'] = $this->language->get('button_visit');
		$data['button_details'] = $this->language->get('button_details');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/random');

		$this->load->model('tool/image');

		$data['products'] = array();

		$filter_data = array(
			'sort'  => 'p.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$results = $this->model_catalog_random->getRandomProducts($filter_data);
// Funcao de Parcelamento na Pagina do Produto
    function eleminaPontosRandom($valor){
      $confV = str_replace(",","",$valor);
      $confV = str_replace(".","",$confV);
      return $confV;
    }

    function parcelamentoRandom($valor,$maximo){
      $valorcompontos = $valor;
      $valor = eleminaPontosRandom($valor);
      $maximo = eleminaPontosRandom($maximo);
      
                  		$parc[1]='1.0000';
                  		$parc[2]='1.0451';
                  		$parc[3]='1.0604';
                  		$parc[4]='1.0759';
                  		$parc[5]='1.0915';
                  		$parc[6]='1.1072';
                  		$parc[7]='1.1231';
                  		$parc[8]='1.1392';
                  		$parc[9]='1.1555';
                  		$parc[10]='1.1717';
                  		$parc[11]='1.1883';
                  		$parc[12]='1.2048';

		      $var = '';
		      for($i = 1; $i <= 12; $i++){
		        $conf = ($valor * $parc[$i]) / $i;
		        $conf = number_format($conf * '0.01',2);
		        $conf = str_replace(",","",$conf);
		        $conf = number_format($conf, 2, ',', '.');
		        $confV = str_replace(",","",$conf);
		        $confV = str_replace(".","",$confV);

		        if($i == '12'){
		          $te = '<small>ou  </small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small> no <b>PagSeguro</b></small>';
		          return $te;
		        }
		        if($confV > $maximo){
		          $var.= '<small>ou </small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small>no <b>PagSeguro</b></small>';
		        } else {
			if($i == '1'){
		        		$te = '<small>ou</small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small> no <b>PagSeguro</b></small>';
		        	}else if($i == '2'){
		        		$te = '<small>ou</small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small> no <b>PagSeguro</b></small>';
		        	}else{
		        		$te = '<small>ou </small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small> no <b>PagSeguro</b></small>';
		        	}			          
		          return $te;
		        }
		      }
		    }

		      /*
		      // Parcelas de X sem juros acima de R$ 70,00
		      if ($valorcompontos >= '70.00') {
                  		$parc[1]='1.0000';
                  		$parc[2]='1.0000';
                  		$parc[3]='1.0604';
                  		$parc[4]='1.0759';
                  		$parc[5]='1.0915';
                  		$parc[6]='1.1072';
                  		$parc[7]='1.1231';
                  		$parc[8]='1.1392';
                  		$parc[9]='1.1555';
                  		$parc[10]='1.1717';
                  		$parc[11]='1.1883';
                  		$parc[12]='1.2048';
                  	}else{
                  		$parc[1]='1.0000';
                  		$parc[2]='1.0451';
                  		$parc[3]='1.0604';
                  		$parc[4]='1.0759';
                  		$parc[5]='1.0915';
                  		$parc[6]='1.1072';
                  		$parc[7]='1.1231';
                  		$parc[8]='1.1392';
                  		$parc[9]='1.1555';
                  		$parc[10]='1.1717';
                  		$parc[11]='1.1883';
                  		$parc[12]='1.2048';
                  	}

		      $var = '';
		      for($i = 1; $i <= 12; $i++){
		        $conf = ($valor * $parc[$i]) / $i;
		        $conf = number_format($conf * '0.01',2);
		        $conf = str_replace(",","",$conf);
		        $conf = number_format($conf, 2, ',', '.');
		        $confV = str_replace(",","",$conf);
		        $confV = str_replace(".","",$confV);

		        if($i == '12'){
		          $te = '<small>ou  </small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small> no <b>PagSeguro</b></small>';
		          return $te;
		        }
		        if($confV > $maximo){
		          $var.= '<small>ou </small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small>no <b>PagSeguro</b></small>';
		        } else {
		        	if($valorcompontos >= '70.00'){
			        	if($i == '1'){
			        		$te = '<small>ou</small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small> no <b>PagSeguro</b></small>';
			        	}else if($i == '2'){
			        		$te = '<small>ou</small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small> no <b>PagSeguro</b></small>';
			        	}else{
			        		$te = '<small>ou </small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small> no <b>PagSeguro</b></small>';
			        	}
			}else{
				if($i == '1'){
			        		$te = '<small>ou</small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small> no <b>PagSeguro</b></small>';
			        	}else if($i == '2'){
			        		$te = '<small>ou</small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small> no <b>PagSeguro</b></small>';
			        	}else{
			        		$te = '<small>ou </small> <b>'.$i."<small>x de R$ </small>".$conf. '</b> <br/> <small> no <b>PagSeguro</b></small>';
			        	}
			}
		          
		          return $te;
		        }
		      }
		    }
		    */
        // Funcao de Parcelamento na Pagina do Produto

		if ($results) {
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}


				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'quantity'    => $result['quantity'],
					'shipping'     => $result['shipping'],
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
				);
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/random.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/random.tpl', $data);
			} else {
				return $this->load->view('default/template/module/random.tpl', $data);
			}
		}
	}
}