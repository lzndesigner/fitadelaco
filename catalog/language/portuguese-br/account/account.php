<?php
// Heading
$_['heading_title']      = 'Minha conta';

// Text
$_['text_account']       = 'Minha conta';
$_['button_reorderall']      = 'Comprar Novamente';
$_['text_my_account']    = 'Minha conta';
$_['text_my_orders']     = 'Meus pedidos';
$_['text_my_newsletter'] = 'Meu informativo';
$_['text_edit']          = 'Informações da conta';
$_['text_password']      = 'Modificar senha';
$_['text_address']       = 'Endereços';
$_['text_wishlist']      = 'Lista de desejos';
$_['text_order']         = 'Histórico de pedidos';
$_['text_download']      = 'Downloads';
$_['text_reward']        = 'Pontos';
$_['text_return']        = 'Devoluções';
$_['text_transaction']   = 'Transações';
$_['text_newsletter']    = 'Novidades, ofertas e promoções por e-mail';
$_['text_recurring']     = 'Assinaturas';
$_['text_transactions']  = 'Transações';

$_['box_1_title']   = 'Histórico de Pedidos';
$_['box_1_desc']    = 'Veja todos seus pedidos realizados em nossa loja. Você pode imprimir e visualizar faturas nesta página.';

$_['box_2_title']   = 'Sua lista de endereços';
$_['box_2_desc']    = 'Adicione, edite e remova endereços de entrega e faturas de sua lista de endereços.';

$_['box_3_title']   = 'Detalhes de sua conta';
$_['box_3_desc']    = 'Atualize seus dados de contato, endereço de entrega, email e/ou sua senha de acesso.';

$_['box_4_title']   = 'Produtos Favoritos';
$_['box_4_desc']    = 'Veja uma lista completa dos ítens que você salvou como favorito recentemente em nossa loja.';