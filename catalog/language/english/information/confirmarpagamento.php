<?php

// Heading

$_['heading_title']  = 'Confirm Payment';

$_['text_name']  = 'Your Name';
$_['text_email']  = 'Your E-mail';
$_['text_pedido']  = 'Order N° ';
$_['text_anexo']  = 'Annex Voucher';
$_['text_anexo_acc']  = '* Accepted file types: .JPEG .PNG .GIF .PDF';
$_['text_notas']  = 'Additional notes';