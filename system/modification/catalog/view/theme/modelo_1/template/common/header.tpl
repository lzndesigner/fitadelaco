<!DOCTYPE html>

<!--[if IE]><![endif]-->

<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->

<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->

<!--[if (gt IE 9)|!(IE)]><!-->

<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">

<!--<![endif]-->

<head>

<meta charset="UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<meta name="robots" content="follow" />
<meta name="googlebot" content="index, follow, all" />
<meta name="language" content="pt-br" />
<meta name="revisit-after" content="3 days">
<meta name="rating" content="general" />
<meta property="og:locale" content="pt_BR"/>
<meta property="og:type" content="website"/>
<?php if($fbMetas){ ?>
  <?php foreach ($fbMetas as $fbMeta) { ?>
  <meta property="og:image:url" content="<?php echo $base; ?>image/<?php echo $fbMeta['content']; ?>" />
  <meta property="og:image:type" content="image/jpeg" />
  <?php } ?>
<?php }else{ ?>
<meta property="og:image:url" content="<?php echo $base; ?>facebook.jpg" />
<meta property="og:image:type" content="image/jpeg" />
<?php } ?>

<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<meta property="og:description" content="<?php echo $description; ?>"/>
<?php } ?>

<?php if ($keywords) { ?>

<meta name="keywords" content= "<?php echo $keywords; ?>" />

<?php } ?>

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<?php if ($icon) { ?>

<link href="<?php echo $base; ?>image/catalog/favicon.ico" rel="icon" />

<?php } ?>

<?php foreach ($links as $link) { ?>

<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />

<?php } ?>

      <meta name="robots" content="follow" />

      <meta name="googlebot" content="index, follow, all" />

      <meta name="language" content="pt-br" />

      <meta name="revisit-after" content="3 days">

      <meta name="rating" content="general" />

      <meta name="author" content="Embalagens para Presente" /> 

<!-- Requerido Template Novo --> 

<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic%7CPT+Gudea:400,700,400italic%7CPT+Oswald:400,700,300' rel='stylesheet' id="googlefont">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        

        <link rel="stylesheet" href="catalog/view/theme/modelo_1/_template/css/bootstrap.min.css">

        <link rel="stylesheet" href="catalog/view/theme/modelo_1/_template/css/prettyPhoto.css">

        <link rel="stylesheet" href="catalog/view/theme/modelo_1/_template/css/colpick.css">

        <link rel="stylesheet" href="catalog/view/theme/modelo_1/_template/css/revslider.css">

        <link rel="stylesheet" href="catalog/view/theme/modelo_1/_template/css/owl.carousel.css">

        <link rel="stylesheet" href="catalog/view/theme/modelo_1/_template/css/style.css?1">

        <link rel="stylesheet" href="catalog/view/theme/modelo_1/_template/css/responsive.css">

        
       
        <!--- jQuery -->

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <script>window.jQuery || document.write('<script src="catalog/view/theme/modelo_1/_template/js/jquery-1.11.0.min.js"><\/script>')</script>

    <!--[if lt IE 9]>

      <script src="catalog/view/theme/modelo_1/_template/js/html5shiv.js"></script>

      <script src="catalog/view/theme/modelo_1/_template/js/respond.min.js"></script>

    <![endif]-->

<!-- Requerido Template Novo -->







<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<link href="catalog/view/theme/modelo_1/stylesheet/stylesheet.css" rel="stylesheet" type="text/css" />

<?php foreach ($styles as $style) { ?>

<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />

<?php } ?>

<script src="catalog/view/javascript/common.js" type="text/javascript"></script>

<?php foreach ($scripts as $script) { ?>

<script src="<?php echo $script; ?>" type="text/javascript"></script>

<?php } ?>

<?php echo $google_analytics; ?>

<script src="catalog/view/javascript/jquery.maskedinput.js"></script>
<script>
jQuery(function($){
       $("#input-telephone").mask("(99) 9.9999-9999");
       $("#input-fax").mask("(99) 9.9999-9999");

       $("#input-payment-telephone").mask("(99) 9.9999-9999");
       $("#input-payment-fax").mask("(99) 9.9999-9999");

       $("#input-tax").mask("999.999.999-99");
       $("#input-custom-field1").mask("99.999.999/9999-99");
});
</script>

<script src="https://assets.pagar.me/checkout/checkout.js"></script>
</head>

<body class="<?php echo $class; ?>">

<div id="wrapper">

      <header id="header">

        <div id="header-top">

          <div class="container">

              <div class="row">

                <div class="col-md-12">

                  <div class="header-top-left">

                    <ul class="top-links">
                      <li style="padding:.2em 0 0 0;"><div id="quick-access"><?php echo $search; ?></div></li>
               

                    </ul>

                  </div><!-- End .header-top-left -->

                  <div class="header-top-right">     
                  

                  

                    <?php if ($logged) { ?>
                    <div class="clearfix"></div>

                     <ul class="top-links">

                      <li><a href="<?php echo $account; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $text_account; ?>">

                        <i class="fa fa-user"></i> <span class="hide-for-xs"><?php echo $text_account; ?></span></a>

                      </li>

                      <li><a href="<?php echo $logout; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $text_logout; ?>">

                        <i class="fa fa-sign-out"></i> <span class="hide-for-xs"><?php echo $text_logout; ?></span></a>

                      </li>

                      </ul>

                    <?php } else { ?>

                      <p class="header-link" style="margin-left:1em">

                    <a href="<?php echo $register; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $text_register; ?>"><i class="fa fa-user"></i> Cadastre-se</a> ou 

                    <a href="<?php echo $login; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $text_login; ?>"><i class="fa fa-sign-in"></i> <?php echo $text_login; ?></a>

                      </p>

                    <?php } ?>

                  </div><!-- End .pull-right -->

                </div><!-- End .header-top-right -->

              </div><!-- End .col-md-12 -->

          </div><!-- End .row -->

        </div><!-- End .container -->

    

        

        <div id="inner-header">

          <div class="container">

            <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12 logo-container">

              <h1 class="logo text-center clearfix">

               <?php if ($logo) { ?>

                <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>

                <?php } else { ?>

                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>

                <?php } ?>

              </h1>

              <ul class="MenuImgs">            
              <li><a href="<?php echo $base; ?>index.php?route=product/category&path=4" title="Laço" class="menu_1"></a></li>
              <li><a href="<?php echo $base; ?>index.php?route=product/category&path=8" title="Sacola para Presente" class="menu_2"></a></li>
              <li><a href="<?php echo $base; ?>index.php?route=product/category&path=11" title="Saco para Presente" class="menu_3"></a></li>
              <li><a href="<?php echo $base; ?>index.php?route=product/category&path=16" title="Caixa para Presente" class="menu_4"></a></li>    
                  
              </ul>

            </div><!-- End .col-md-5 -->

              <div class="col-md-12 col-sm-12 col-xs-12 header-inner-right text-center">

                 <div class="header-inner-right-wrapper">

                  <div class="header-top-dropdowns pull-right">

                     <?php echo $currency; ?>

                     <?php echo $language; ?>

                  </div><!-- End .header-top-dropdowns -->
                            

              </div><!-- End .header-inner-right-wrapper -->

                  

              </div><!-- End .col-md-7 -->

            </div><!-- End .row -->

          </div><!-- End .container -->

          

          <div id="main-nav-container">           

          </div><!-- End #nav -->

        </div><!-- End #inner-header -->

      </header><!-- End #header -->



