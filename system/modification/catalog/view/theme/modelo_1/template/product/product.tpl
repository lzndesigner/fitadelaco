<?php echo $header; ?>

<div class="container">

  <div id="category-breadcrumb">

    <div class="container">

      <ul class="breadcrumb" style="margin-bottom:.5em;">

        <?php foreach ($breadcrumbs as $breadcrumb) { ?>

        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

        <?php } ?>

      </ul>

    </div>

  </div><!-- category-breadcrumb -->



  <div class="row"><?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>





<div class="row">

              <div class="col-md-12">

                

                <div class="row">

                

                <div class="col-md-6 col-sm-12 col-xs-12 product-viewer clearfix">

               

                          <?php if ($thumb || $images) { ?>

                          <div id="product-image-carousel-container">

                            <ul id="product-carousel" class="celastislide-list">                              

                               <?php if ($thumb) { ?>
                               <li><a data-image="<?php echo $thumb; ?>" data-zoom-image="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>">
                                <img src="<?php echo $thumb; ?>"title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                              </a></li>
                                <?php } ?>


                              <?php if ($images) { ?>

                              <?php foreach ($images as $image) { ?>

                              <li><a data-image="<?php echo $image['popup']; ?>" data-zoom-image="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>">
                                <img src="<?php echo $image['thumb']; ?>"title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                              </a></li>

                              <?php } ?>

                              <?php } ?>

                          </ul><!-- End product-carousel -->

                          </div>


                           



                          <div id="product-image-container">

                            <?php if ($thumb) { ?>

                            <figure>

                              <img id="zoom_03" src="<?php echo $thumb; ?>"  data-zoom-image="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="product-image" />

                          
                            </figure>
                            <small class="hideMobileInfoImgProduct">Passe o mouse sobre a imagem para focar e rode a bolinha para aproxima.</small>

                            <?php } ?>

                          </div><!-- product-image-container -->     

                          <?php } ?>   

<script src="catalog/view/theme/modelo_1/_template/js/elevatezoom/jquery.elevatezoom.js" type="text/javascript"></script>
<script>
//initiate the plugin and pass the id of the div containing gallery images
$("#zoom_03").elevateZoom({scrollZoom : true, zoomType: "lens", containLensZoom: true, gallery:'product-image-carousel-container', cursor: 'pointer', galleryActiveClass: "active"}); 
//pass the images to Fancybox 
$("#zoom_03").bind("click", function(e) { var ez = $('#zoom_03').data('elevateZoom');  $.fancybox(ez.getGalleryList()); return false; });
</script>

<style>
/*set a border on the images to prevent shifting*/ 
#gal1 img{border:2px solid white;} 
/*Change the colour*/ 
.active img{border:2px solid #333 !important;}
</style>



                

                </div><!-- End .col-md-6 -->



                <div class="col-md-6 col-sm-12 col-xs-12 product">



                  <div class="lg-margin visible-sm visible-xs"></div><!-- Space -->

                  <h1 class="product-name"><?php echo $heading_title; ?></h1>


                  <?php if ($review_status) { ?>

                  <div class="ratings-container">

                  <?php for ($i = 1; $i <= 5; $i++) { ?>

                  <?php if ($rating < $i) { ?>

                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>

                  <?php } else { ?>

                  <span class="fa fa-stack" style="width:1.5em; height:1.5em; color:#14bfcc;"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>

                  <?php } ?>

                  <?php } ?>

                    <span class="ratings-amount separator">

                      <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a>

                    </span>

                    <span class="separator">|</span>

                    <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;" class="rate-this"><?php echo $text_write; ?></a>

                  </div><!-- End .rating-container -->

                  <?php } ?>




          <div id="product">

            <?php if ($options) { ?>

            <hr>

            <h3><?php echo $text_option; ?></h3>

            <?php foreach ($options as $option) { ?>

            <?php if ($option['type'] == 'select') { ?>

            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> col-md-6">

              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>

              <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">

                <option value=""><?php echo $text_select; ?></option>

                <?php foreach ($option['product_option_value'] as $option_value) { ?>

                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>

                <?php if ($option_value['price']) { ?>

                (<?php echo $option_value['price']; ?>) <?php echo $option_value['o_description'] ?>

                <?php } ?>

                </option>

                <?php } ?>

              </select>

            </div>

            <?php } ?>

            <?php if ($option['type'] == 'radio') { ?>

            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> col-md-12">

              <label class="control-label"><?php echo $option['name']; ?></label>

              <div id="input-option<?php echo $option['product_option_id']; ?>" class="estampas">

                <?php foreach ($option['product_option_value'] as $option_value) { ?>

                <div class="radio">

                  <label>

                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />

                    <?php echo $option_value['name']; ?> 

                    <?php if ($option_value['image']) { ?>
                    <img src="<?php echo $option_value['image']; ?>" alt="" />
                    <?php } ?>



                                    <?php if ($option_value['price']) { ?>

                    (<?php echo $option_value['price']; ?>)
 <div><?php echo $option_value['o_description'] ?></div>

                    <?php } ?>

                  </label>

                </div>

                <?php } ?>

              </div>

            </div>

            <?php } ?>

            <?php if ($option['type'] == 'checkbox') { ?>

            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">

              <label class="control-label"><?php echo $option['name']; ?></label>

              <div id="input-option<?php echo $option['product_option_id']; ?>">

                <?php foreach ($option['product_option_value'] as $option_value) { ?>

                <div class="checkbox">

                  <label>

                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />

                    <?php echo $option_value['name']; ?>

                    <?php if ($option_value['price']) { ?>

                    - (<?php echo $option_value['price']; ?>)
 <div style="font-size:11.5px;">- <?php echo $option_value['o_description'] ?></div>

                    <?php } ?>

                  </label>

                </div>

                <?php } ?>

              </div>

            </div>

            <?php } ?>

            <?php if ($option['type'] == 'image') { ?>

            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">

              <label class="control-label"><?php echo $option['name']; ?></label>

              <div id="input-option<?php echo $option['product_option_id']; ?>">

                <?php foreach ($option['product_option_value'] as $option_value) { ?>

                <div class="radio">

                  <label>

                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />

                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>

                    <?php if ($option_value['price']) { ?>

                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
 <div><?php echo $option_value['o_description'] ?></div>

                    <?php } ?>

                  </label>

                </div>

                <?php } ?>

              </div>

            </div>

            <?php } ?>

            <?php if ($option['type'] == 'text') { ?>

            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> col-md-12">

              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>

              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />

            </div>

            <?php } ?>

            <?php if ($option['type'] == 'textarea') { ?>

            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">

              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>

              <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>

            </div>

            <?php } ?>

            <?php if ($option['type'] == 'file') { ?>

            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">

              <label class="control-label"><?php echo $option['name']; ?></label>

              <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>

              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />

            </div>

            <?php } ?>

            <?php if ($option['type'] == 'date') { ?>

            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">

              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>

              <div class="input-group date">

                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />

                <span class="input-group-btn">

                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>

                </span></div>

            </div>

            <?php } ?>

            <?php if ($option['type'] == 'datetime') { ?>

            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">

              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>

              <div class="input-group datetime">

                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />

                <span class="input-group-btn">

                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>

                </span></div>

            </div>

            <?php } ?>

            <?php if ($option['type'] == 'time') { ?>

            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">

              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>

              <div class="input-group time">

                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />

                <span class="input-group-btn">

                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>

                </span></div>

            </div>

            <?php } ?>

            <?php } ?>

            <?php } ?>

            <?php if ($recurrings) { ?>


            <hr>

            <h3><?php echo $text_payment_recurring ?></h3>

            <div class="form-group required">

              <select name="recurring_id" class="form-control">

                <option value=""><?php echo $text_select; ?></option>

                <?php foreach ($recurrings as $recurring) { ?>

                <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>

                <?php } ?>

              </select>

              <div class="help-block" id="recurring-description"></div>

            </div>

            <?php } ?>


            <?php if ($minimum > 1) { ?>

            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>

            <?php } ?>
          </div>

             <hr> 

             <script type="text/javascript">
$('#button-quote').on('click', function() {
    $.ajax({
          url: 'index.php?route=product/product/quoteProduct',
          type: 'post',
          data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
          dataType: 'json',
          beforeSend: function() {
              $('#button-quote').button('loading');
          },
          complete: function() {
              $('#button-quote').button('reset');
          },
          success: function(json) {
              $('.alert, .text-danger').remove();
              $.ajax({
            url: 'index.php?route=product/product/quote',
            type: 'post',
            data: 'country_id=' + $('select[name=\'country_id\']').val() + '&zone_id=' + $('select[name=\'zone_id\']').val() + '&postcode=' + encodeURIComponent($('input[name=\'postcode\']').val()),
            dataType: 'json',
            success: function(json) {
                        if (json['error']) {
                              if (json['error']['warning']) {
                                  $('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                                  $('html, body').animate({ scrollTop: 0 }, 'slow');
                              }

                              if (json['error']['country']) {
                                  $('select[name=\'country_id\']').after('<div class="text-danger">' + json['error']['country'] + '</div>');
                              }

                              if (json['error']['zone']) {
                                  $('select[name=\'zone_id\']').after('<div class="text-danger">' + json['error']['zone'] + '</div>');
                              }

                              if (json['error']['postcode']) {
                                  $('input[name=\'postcode\']').after('<div class="text-danger">' + json['error']['postcode'] + '</div>');
                              }
                        }

                          if (json['shipping_method']) {
                              $('#modal-shipping').remove();

                              html  = '<div id="modal-shipping" class="modal">';
                              html += '  <div class="modal-dialog">';
                              html += '    <div class="modal-content">';
                              html += '      <div class="modal-header">';
                              html += '        <h4 class="modal-title"><?php echo $estimate_shipping["estimate_shipping_popup_title"]; ?></h4>';
                              html += '      </div>';
                              html += '      <div class="modal-body">';

                              for (i in json['shipping_method']) {
                                  html += '<p><strong>' + json['shipping_method'][i]['title'] + '</strong></p>';

                                  if (!json['shipping_method'][i]['error']) {
                                      for (j in json['shipping_method'][i]['quote']) {
                                          html += '<div class="radio">';
                                          html += '  <label>';
                                          html += json['shipping_method'][i]['quote'][j]['title'] + ' - ' + json['shipping_method'][i]['quote'][j]['text'] + '<br/><img src="catalog/view/images/' + json['shipping_method'][i]['quote'][j]['code'] + '.png" alt="' + json['shipping_method'][i]['quote'][j]['title'] + '" style="width:120px; margin:.5em;" /> </label></div>';
                                      }
                                  } else {
                                      html += '<div class="alert alert-danger">' + json['shipping_method'][i]['error'] + '</div>';
                                  }
                              }

                              html += '      </div>';
                              html += '      <div class="modal-footer">';
                              html += '        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_cancel; ?></button>';

                              html += '      </div>';
                              html += '    </div>';
                              html += '  </div>';
                              html += '</div> ';

                              $('body').append(html);

                              $('#modal-shipping').modal('show');

                              $('input[name=\'shipping_method\']').on('change', function() {
                                  $('#button-shipping').prop('disabled', false);
                              });
                          }
            }//success
          });
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});
</script>
<script type="text/javascript">
$('select[name=\'country_id\']').on('change', function() {
    $.ajax({
    url: 'index.php?route=product/product/country&country_id=' + this.value,
        dataType: 'json',
        beforeSend: function() {
            $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
        },
        complete: function() {
            $('.fa-spin').remove();
        },
        success: function(json) {
            if (json['postcode_required'] == '1') {
                  $('input[name=\'postcode\']').parent().parent().addClass('required');
            } else {
                  $('input[name=\'postcode\']').parent().parent().removeClass('required');
            }

            html = '<option value=""><?php echo $text_select; ?></option>';

            if (json['zone'] && json['zone'] != '') {
                  for (i = 0; i < json['zone'].length; i++) {
                      html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                      if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                          html += ' selected="selected"';
                      }

                      html += '>' + json['zone'][i]['name'] + '</option>';
                  }
            } else {
                html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
            }

              $('select[name=\'zone_id\']').html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

$('select[name=\'country_id\']').trigger('change');
</script>



                 <div class="md-margin"></div><!-- Space -->

                  <div class="product-extra clearfix">

                <div class="product-extra-box-container clearfix">

                  <div class="item-action-inner">

                    <button type="button" data-toggle="tooltip" class="btn btn-custom-2" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="fa fa-heart"></i></button>

<button type="button" data-toggle="tooltip" class="btn btn-custom-2" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-exchange"></i></button>





                  </div><!-- End .item-action-inner -->

                </div>

                <div class="md-margin visible-xs"></div>

                <div class="share-button-group">

                  <!-- AddThis Button BEGIN -->

                  <div class="addthis_toolbox addthis_default_style addthis_32x32_style">

                  <a class="addthis_button_facebook"></a>

                  <a class="addthis_button_twitter"></a>

                  <a class="addthis_button_email"></a>

                  <a class="addthis_button_print"></a>

                  <a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>

                  </div>

                  <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>

                  <script type="text/javascript" src="../../../../s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52b2197865ea0183"></script>

                  <!-- AddThis Button END -->

            

            <!-- AddThis Button BEGIN 

            <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>

            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> 

             AddThis Button END --> 



                </div><!-- End .share-button-group -->

                  </div>

                </div><!-- End .col-md-6 -->

                  

                  

                </div><!-- End .row -->

                

                <div class="lg-margin2x"></div><!-- End .space -->

                

                <div class="row">

                  <div class="col-md-12 col-sm-12 col-xs-12">

                    

                    <div class="tab-container left product-detail-tab clearfix">



                        <ul class="nav-tabs">

                    



                      <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>

                      <?php if ($attribute_groups) { ?>

                      <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>

                      <?php } ?>

                      <?php if ($review_status) { ?>

                      <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>

                      <?php } ?>

                    </ul>

                        <div class="tab-content">

                          

                          <div class="tab-pane active" id="tab-description">

                           <?php echo $description; ?>

                          </div><!-- End .tab-pane -->

                          

   



                           <?php if ($attribute_groups) { ?>

                            <div class="tab-pane" id="tab-specification">

                              <table class="table table-bordered">

                                <?php foreach ($attribute_groups as $attribute_group) { ?>

                                <thead>

                                  <tr>

                                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>

                                  </tr>

                                </thead>

                                <tbody>

                                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>

                                  <tr>

                                    <td><?php echo $attribute['name']; ?></td>

                                    <td><?php echo $attribute['text']; ?></td>

                                  </tr>

                                  <?php } ?>

                                </tbody>

                                <?php } ?>

                              </table>

                            </div><!-- End .tab-pane -->

                            <?php } ?>

                            <?php if ($review_status) { ?>

                            <div class="tab-pane" id="tab-review">

                              <form class="form-horizontal">

                                <div id="review"></div>
                                  <header class="title-bg">
                                    <h3><?php echo $text_write; ?></h3>
                                  </header>
                               

                                <?php if ($review_guest) { ?>

                                <div class="input-group">

                                  <span class="input-group-addon"><span class="input-text"><?php echo $entry_name; ?></span></span>

                                  <input type="text" name="name" value="" id="input-name" class="form-control input-lg">

                                </div><!-- End .input-group -->


                                <div class="input-group textarea-container">
                                  <span class="input-group-addon" style="text-align:left;"><span class="input-text"><?php echo $entry_review; ?></span></span>
                                  <textarea name="text" id="input-review" class="form-control" cols="30" rows="6"></textarea>
                                  <div class="help-block"><?php echo $text_note; ?></div>
                                </div>



                                <div class="input-group">

                                  <label><?php echo $entry_rating; ?></label>

                                   &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;

                                    <input type="radio" name="rating" value="1" />

                                    &nbsp;

                                    <input type="radio" name="rating" value="2" />

                                    &nbsp;

                                    <input type="radio" name="rating" value="3" />

                                    &nbsp;

                                    <input type="radio" name="rating" value="4" />

                                    &nbsp;

                                    <input type="radio" name="rating" value="5" />

                                    &nbsp;<?php echo $entry_good; ?>

                                </div><!-- End .input-group -->

                                 <div class="input-group">

                                   <img src="index.php?route=tool/captcha" alt="" id="captcha" />

                                </div><!-- End .input-group -->


                                 <div class="input-group">

                                  <label><?php echo $entry_captcha; ?></label>

                                   <input type="text" name="captcha" value="" id="input-captcha" class="form-control" />

                                </div><!-- End .input-group -->


                             

                                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-custom"><?php echo $button_continue; ?></button>

                                <?php } else { ?>

                                <?php echo $text_login; ?>

                                <?php } ?>

                              </form>

                            </div><!-- End .tab-pane -->

                            <?php } ?>

                          



                          

                        </div><!-- End .tab-content -->

                    </div><!-- End .tab-container -->

                    <div class="lg-margin visible-xs"></div>

                  </div><!-- End .col-md-9 -->    

           

                </div><!-- End .row -->    

                <div class="lg-margin2x"></div><!-- End .space -->

                <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">

                  <?php if ($products) { ?>

                <div class="purchased-items-container carousel-wrapper">

                            <header class="content-title">

                                <div class="title-bg">

                                    <h2 class="title"><?php echo $text_related; ?></h2>

                                </div><!-- End .title-bg -->

                            </header>

                               

                                <div class="carousel-controls">

                                    <div id="purchased-items-slider-prev" class="carousel-btn carousel-btn-prev"></div><!-- End .carousel-prev -->

                                    <div id="purchased-items-slider-next" class="carousel-btn carousel-btn-next carousel-space"></div><!-- End .carousel-next -->

                                </div><!-- End .carousel-controllers -->

                                <div class="purchased-items-slider owl-carousel">

                                           

                                              <?php $i = 0; ?>

                                              <?php foreach ($products as $product) { ?>                                             

                                              

                                            <div class="item">                                            

                                            <div class="item-image-container">

                                            <figure>

                                              <a href="<?php echo $product['href']; ?>">

                                                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="item-image">

                                                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="item-image-hover">

                                                    </a>

                                            </figure>

                                                <?php if ($product['price']) { ?>

                                                  <div class="item-price-container">

                                                    <?php if (!$product['special']) { ?>

                                                     <span class="item-price"><?php echo $product['price']; ?></span>

                                                    <?php } else { ?>

                                                    <span class="old-price"><?php echo $product['price']; ?></span>

                                                    <span class="item-price"><?php echo $product['special']; ?></span>

                                                    <?php } ?>

                                                  </div>

                                                  <?php } ?>

                                                 

                                                </div><!-- End .item-image -->

                                            <div class="item-meta-container">

                                               <?php if ($product['rating']) { ?>

                                                <div class="ratings-container">                                                    

                                                    <div class="rating">

                                                      <?php for ($i = 1; $i <= 5; $i++) { ?>

                                                      <?php if ($product['rating'] < $i) { ?>

                                                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>

                                                      <?php } else { ?>

                                                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>

                                                      <?php } ?>

                                                      <?php } ?>

                                                    </div>

                                                </div><!-- End .rating-container -->

                                                <?php } ?>

                                                <h3 class="item-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>

                                                <div class="item-action">

                                                       <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="item-add-btn">

                                                            <span class="icon-cart-text"><?php echo $button_cart; ?></span>

                                                        </button>

                                                        <div class="item-action-inner">

                                                            <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-like"><?php echo $button_wishlist; ?></button>

                                                            <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');" class="icon-button icon-compare"><?php echo $button_compare; ?></button>

                                                        </div><!-- End .item-action-inner -->

                                                    </div><!-- End .item-action -->

                                            </div><!-- End .item-meta-container --> 

                                        </div><!-- End .item -->

                                         <?php $i++; ?>

                                              <?php } ?>

                                        





                                </div><!--purchased-items-slider -->

                            </div><!-- End .purchased-items-container -->

                             <?php } ?>

                </div><!-- col-md-12 col-sm-12 col-xs-12 -->

                </div><!-- row -->   





              </div><!-- End .col-md-12 -->

            </div><!-- End .row -->













































      

      <?php if ($tags) { ?>

      <p><i class="fa fa-tags"></i>

        <?php for ($i = 0; $i < count($tags); $i++) { ?>

        <?php if ($i < (count($tags) - 1)) { ?>

        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,

        <?php } else { ?>

        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>

        <?php } ?>

        <?php } ?>

      </p>

      <?php } ?>

      <?php echo $content_bottom; ?></div>

    <?php echo $column_right; ?></div>

</div>

<script type="text/javascript"><!--

$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){

  $.ajax({

    url: 'index.php?route=product/product/getRecurringDescription',

    type: 'post',

    data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),

    dataType: 'json',

    beforeSend: function() {

      $('#recurring-description').html('');

    },

    success: function(json) {

      $('.alert, .text-danger').remove();

      

      if (json['success']) {

        $('#recurring-description').html(json['success']);

      }

    }

  });

});

//--></script> 

<script type="text/javascript"><!--

$('#button-cart').on('click', function() {

  $.ajax({

    url: 'index.php?route=checkout/cart/add',

    type: 'post',

    data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),

    dataType: 'json',

    beforeSend: function() {

      $('#button-cart').button('loading');

    },

    complete: function() {

      $('#button-cart').button('reset');

    },

    success: function(json) {

      $('.alert, .text-danger').remove();

      $('.form-group').removeClass('has-error');



      if (json['error']) {

        if (json['error']['option']) {

          for (i in json['error']['option']) {

            var element = $('#input-option' + i.replace('_', '-'));

            

            if (element.parent().hasClass('input-group')) {

              element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');

            } else {

              element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');

            }

          }

        }

        

        if (json['error']['recurring']) {

          $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');

        }

        

        // Highlight any found errors

        $('.text-danger').parent().addClass('has-error');

      }

      

      if (json['success']) {

        $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

        

        $('#cart-total').html(json['total']);

        

        $('html, body').animate({ scrollTop: 0 }, 'slow');

        

        $('#cart > ul').load('index.php?route=common/cart/info ul li');

      }

    }

  });

});

//--></script> 

<script type="text/javascript"><!--

$('.date').datetimepicker({

  pickTime: false

});



$('.datetime').datetimepicker({

  pickDate: true,

  pickTime: true

});



$('.time').datetimepicker({

  pickDate: false

});



$('button[id^=\'button-upload\']').on('click', function() {

  var node = this;

  

  $('#form-upload').remove();

  

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  

  $('#form-upload input[name=\'file\']').trigger('click');

  

  timer = setInterval(function() {

    if ($('#form-upload input[name=\'file\']').val() != '') {

      clearInterval(timer);

      

      $.ajax({

        url: 'index.php?route=tool/upload',

        type: 'post',

        dataType: 'json',

        data: new FormData($('#form-upload')[0]),

        cache: false,

        contentType: false,

        processData: false,

        beforeSend: function() {

          $(node).button('loading');

        },

        complete: function() {

          $(node).button('reset');

        },

        success: function(json) {

          $('.text-danger').remove();

          

          if (json['error']) {

            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');

          }

          

          if (json['success']) {

            alert(json['success']);

            

            $(node).parent().find('input').attr('value', json['code']);

          }

        },

        error: function(xhr, ajaxOptions, thrownError) {

          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

        }

      });

    }

  }, 500);

});

//--></script> 

<script type="text/javascript"><!--

$('#review').delegate('.pagination a', 'click', function(e) {

  e.preventDefault();



    $('#review').fadeOut('slow');



    $('#review').load(this.href);



    $('#review').fadeIn('slow');

});



$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');



$('#button-review').on('click', function() {

  $.ajax({

    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',

    type: 'post',

    dataType: 'json',

    data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),

    beforeSend: function() {

      $('#button-review').button('loading');

    },

    complete: function() {

      $('#button-review').button('reset');

      $('#captcha').attr('src', 'index.php?route=tool/captcha#'+new Date().getTime());

      $('input[name=\'captcha\']').val('');

    },

    success: function(json) {

      $('.alert-success, .alert-danger').remove();

      

      if (json['error']) {

        $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');

      }

      

      if (json['success']) {

        $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

        

        $('input[name=\'name\']').val('');

        $('textarea[name=\'text\']').val('');

        $('input[name=\'rating\']:checked').prop('checked', false);

        $('input[name=\'captcha\']').val('');

      }

    }

  });

});



$(document).ready(function() {

  $('.thumbnails').magnificPopup({

    type:'image',

    delegate: 'a',

    gallery: {

      enabled:true

    }

  });

});

//--></script> 


<script type="text/javascript">
$('#button-quote').on('click', function() {
    $.ajax({
          url: 'index.php?route=product/product/quoteProduct',
          type: 'post',
          data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
          dataType: 'json',
          beforeSend: function() {
              $('#button-quote').button('loading');
          },
          complete: function() {
              $('#button-quote').button('reset');
          },
          success: function(json) {
              $('.alert, .text-danger').remove();
              $.ajax({
    				url: 'index.php?route=product/product/quote',
    				type: 'post',
    				data: 'country_id=' + $('select[name=\'country_id\']').val() + '&zone_id=' + $('select[name=\'zone_id\']').val() + '&postcode=' + encodeURIComponent($('input[name=\'postcode\']').val()),
    				dataType: 'json',
    				success: function(json) {
                        if (json['error']) {
                              if (json['error']['warning']) {
                                  $('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                                  $('html, body').animate({ scrollTop: 0 }, 'slow');
                              }

                              if (json['error']['country']) {
                                  $('select[name=\'country_id\']').after('<div class="text-danger">' + json['error']['country'] + '</div>');
                              }

                              if (json['error']['zone']) {
                                  $('select[name=\'zone_id\']').after('<div class="text-danger">' + json['error']['zone'] + '</div>');
                              }

                              if (json['error']['postcode']) {
                                  $('input[name=\'postcode\']').after('<div class="text-danger">' + json['error']['postcode'] + '</div>');
                              }
                        }

                          if (json['shipping_method']) {
                              $('#modal-shipping').remove();

                              html  = '<div id="modal-shipping" class="modal">';
                              html += '  <div class="modal-dialog">';
                              html += '    <div class="modal-content">';
                              html += '      <div class="modal-header">';
                              html += '        <h4 class="modal-title"><?php echo $estimate_shipping["estimate_shipping_popup_title"]; ?></h4>';
                              html += '      </div>';
                              html += '      <div class="modal-body">';

                              for (i in json['shipping_method']) {
                                  html += '<p><strong>' + json['shipping_method'][i]['title'] + '</strong></p>';

                                  if (!json['shipping_method'][i]['error']) {
                                      for (j in json['shipping_method'][i]['quote']) {
                                          html += '<div class="radio">';
                                          html += '  <label>';
                                          html += json['shipping_method'][i]['quote'][j]['title'] + ' - ' + json['shipping_method'][i]['quote'][j]['text'] + '<br/><img src="catalog/view/images/' + json['shipping_method'][i]['quote'][j]['code'] + '.png" alt="' + json['shipping_method'][i]['quote'][j]['title'] + '" style="width:120px; margin:.5em;" /> </label></div>';
                                      }
                                  } else {
                                      html += '<div class="alert alert-danger">' + json['shipping_method'][i]['error'] + '</div>';
                                  }
                              }

                              html += '      </div>';
                              html += '      <div class="modal-footer">';
                              html += '        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_cancel; ?></button>';

                              html += '      </div>';
                              html += '    </div>';
                              html += '  </div>';
                              html += '</div> ';

                              $('body').append(html);

                              $('#modal-shipping').modal('show');

                              $('input[name=\'shipping_method\']').on('change', function() {
                                  $('#button-shipping').prop('disabled', false);
                              });
                          }
    				}//success
    			});
    		},
    		error: function(xhr, ajaxOptions, thrownError) {
    			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    		}
    });
});
</script>
<script type="text/javascript">
$('select[name=\'country_id\']').on('change', function() {
    $.ajax({
		url: 'index.php?route=product/product/country&country_id=' + this.value,
        dataType: 'json',
        beforeSend: function() {
            $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
        },
        complete: function() {
            $('.fa-spin').remove();
        },
        success: function(json) {
            if (json['postcode_required'] == '1') {
                  $('input[name=\'postcode\']').parent().parent().addClass('required');
            } else {
                  $('input[name=\'postcode\']').parent().parent().removeClass('required');
            }

            html = '<option value=""><?php echo $text_select; ?></option>';

            if (json['zone'] && json['zone'] != '') {
                  for (i = 0; i < json['zone'].length; i++) {
                      html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                      if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                          html += ' selected="selected"';
                      }

                      html += '>' + json['zone'][i]['name'] + '</option>';
                  }
            } else {
                html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
            }

              $('select[name=\'zone_id\']').html(html);
        },
        error: function(xhr, ajaxOptions, thrownError) {
              alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});

$('select[name=\'country_id\']').trigger('change');
</script>
            

            <script type="text/javascript" src="index.php?route=product/live_options/js&product_id=<?php echo $product_id; ?>"></script>
            
<?php echo $footer; ?>

