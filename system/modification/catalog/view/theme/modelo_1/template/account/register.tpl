<?php echo $header; ?>
<div class="container">
  <div id="category-breadcrumb">
    <div class="container">
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>



        <?php } ?>



      </ul>



    </div>



  </div><!-- category-breadcrumb -->




  <div class="row"><?php echo $column_left; ?>



    <?php if ($column_left && $column_right) { ?>



    <?php $class = 'col-sm-6'; ?>



    <?php } elseif ($column_left || $column_right) { ?>



    <?php $class = 'col-sm-9'; ?>



    <?php } else { ?>



    <?php $class = 'col-sm-12'; ?>



    <?php } ?>



    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>



      <header class="content-title">



        <h1 class="title"><?php echo $heading_title; ?></h1>



        <p class="title-desc"><?php echo $text_account_already; ?></p>



      </header>



      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">



          <div class="row">           





                <div class="col-md-12 col-sm-12 col-xs-12">



  <?php if ($error_warning) { ?>



  <div class="alert alert-danger clearfix">



    <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>

     <?php if ($error_confirm) { ?>



          <li class="text-danger"><?php echo $error_confirm; ?></li>



         <?php } ?>



  </div>



  <?php } ?>
               
                  



                  <fieldset id="account">

 <div class="input-group" style="display: <?php echo (count($customer_groups) > 1 ? 'block' : 'none'); ?>;">

                    <h2 class="sub-title">Quem está comprando?</h2>

                    <?php foreach ($customer_groups as $customer_group) { ?>



                    <?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>

                   

                      <span class="input-group-addon" style="text-align:left;"><span class="input-text" style="padding:.3em .7em"><?php echo $customer_group['name']; ?></span>                  



                        <input type="radio" name="customer_group_id" style="float:left;" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" /></span>   
                     


                    <?php } else { ?>

                 

                      <span class="input-group-addon" style="text-align:left;"><span class="input-text" style="padding:.3em .7em"><?php echo $customer_group['name']; ?></span>                     



                        <input type="radio" name="customer_group_id" style="float:left;" value="<?php echo $customer_group['customer_group_id']; ?>" /></span> 
                    


                    <?php } ?>



                    <?php } ?>



                  </div><!-- End .input-group -->


                        <h2 class="sub-title"><?php echo $text_your_password; ?></h2>



                          <div class="input-group <?php if ($error_email) { ?>has-error<?php }?>">



                            <span class="input-group-addon"><span class="input-text"><?php echo $entry_email; ?></span></span>



                            <input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control input-lg">                             


                          </div><!-- End .input-group -->

                           <?php if ($error_email) { ?>

                             <span class="help-block"><p class="text-danger"><?php echo $error_email; ?></p></span>

                            <?php } ?>




                          <div class="input-group <?php if ($error_password) { ?>has-error<?php }?>">



                            <span class="input-group-addon"><span class="input-text"><?php echo $entry_password; ?></span></span>



                            <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control input-lg">



                          </div><!-- End .input-group -->
                                  <?php if ($error_password) { ?>



                                  <span class="help-block"><p class="text-danger"><?php echo $error_password; ?></p></span>



                                <?php } ?>



                          <div class="input-group <?php if ($error_password) { ?>has-error<?php }?>">



                            <span class="input-group-addon"><span class="input-text"><?php echo $entry_confirm; ?></span></span>



                            <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control input-lg">



                          </div><!-- End .input-group -->
                          <?php if ($error_password) { ?>



                                  <span class="help-block"><p class="text-danger"><?php echo $error_password; ?></p></span>



                                <?php } ?>



                                <hr>



                  <h2 class="sub-title"><?php echo $text_your_details; ?></h2>


 

                 





                  <div class="input-group <?php if ($error_firstname) { ?>has-error<?php }?>">



                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_firstname; ?></span></span>



                    <input type="text"  name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control input-lg">    



                  </div><!-- End .input-group -->

                    <?php if ($error_firstname) { ?>
                   <span class="help-block"><p class="text-danger"><?php echo $error_firstname; ?></p></span>
                     <?php } ?> 









                  <div class="input-group <?php if ($error_lastname) { ?>has-error<?php }?>">



                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_lastname; ?></span></span>



                    <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control input-lg" >



                  </div><!-- End .input-group -->

                      <?php if ($error_lastname) { ?>
                       <span class="help-block"><p class="text-danger"><?php echo $error_lastname; ?></p></span>
                      <?php } ?>






                  <div class="input-group <?php if ($error_telephone) { ?>has-error<?php }?>">



                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_telephone; ?></span></span>



                    <input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control input-lg">



                  </div><!-- End .input-group -->



                <?php if ($error_telephone) { ?>



                 <span class="help-block"><p class="text-danger"><?php echo $error_telephone; ?></p></span>



                <?php } ?>







                  <div class="input-group">



                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_fax; ?></span></span>



                    <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="form-control input-lg">



                  </div><!-- End .input-group -->






          <?php foreach ($custom_fields as $custom_field) { ?>



          <?php if ($custom_field['location'] == 'account') { ?>



  
          <?php if ($custom_field['type'] == 'select') { ?>

              <div class="input-group custom-field <?php if ($error_zone) { ?>has-error<?php }?>" id="custom-field<?php echo $custom_field['custom_field_id']; ?>"  data-sort="<?php echo $custom_field['sort_order']; ?>">



                    <span class="input-group-addon"><span class="input-text"><?php echo $custom_field['name']; ?></span></span>


              <select name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control input-lg">



                <option value=""><?php echo $text_select; ?></option>



                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>



                <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>



                <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>" selected="selected"><?php echo $custom_field_value['name']; ?></option>



                <?php } else { ?>



                <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>



                <?php } ?>



                <?php } ?>



              </select>



                  </div><!-- End .input-group -->


              <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>



              <span class="help-block"><p class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></p></span>



              <?php } ?>


          <?php } ?>



          <?php if ($custom_field['type'] == 'radio') { ?>



                  <div class="input-group custom-field <?php if ($error_zone) { ?>has-error<?php }?>" id="custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">



                    <span class="input-group-addon" style="text-align:left; padding-right:.5em"><span class="input-text" style="font-weight:bold;"><?php echo $custom_field['name']; ?>:</span>


        <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>



                <div class="radio-inline">



                  <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>



                  <label>



                    <input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />



                    <?php echo $custom_field_value['name']; ?></label>



                  <?php } else { ?>



                  <label>



                    <input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />



                    <?php echo $custom_field_value['name']; ?></label>



                  <?php } ?>



                </div>



                <?php } ?>



                  </div><!-- End .input-group -->
 

                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                  <span class="help-block"><p class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></p></span>
                <?php } ?>
          <?php } ?>



          <?php if ($custom_field['type'] == 'checkbox') { ?>


  <div class="input-group custom-field <?php if ($error_zone) { ?>has-error<?php }?>" id="custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">



                    <span class="input-group-addon" style="text-align:left; padding-right:.5em"><span class="input-text" style="font-weight:bold;"><?php echo $custom_field['name']; ?>:</span>


        <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>



                <div class="checkbox-inline">



                  <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>



                  <label>



                    <input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />



                    <?php echo $custom_field_value['name']; ?></label>



                  <?php } else { ?>



                  <label>



                    <input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />



                    <?php echo $custom_field_value['name']; ?></label>



                  <?php } ?>



                </div>



                <?php } ?>



                  </div><!-- End .input-group -->
 

                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                  <span class="help-block"><p class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></p></span>
                <?php } ?>


          <?php } ?>



       
          <?php if ($custom_field['type'] == 'text') { ?>



            <div class="input-group custom-field <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>has-error<?php } ?>" id="custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">


              <span class="input-group-addon"><span class="input-text"><?php echo $custom_field['name']; ?></span></span>

              <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control input-lg" />

            </div><!-- End .input-group -->

          <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
              <span class="help-block"><p class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></p></span>
          <?php } ?>

          <?php } ?>



          <?php if ($custom_field['type'] == 'textarea') { ?>



            <div class="input-group custom-field <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>has-error<?php } ?>" id="custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">


              <span class="input-group-addon"><span class="input-text"><?php echo $custom_field['name']; ?></span></span>

              <textarea name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control input-lg" row="3" cols="5"></textarea>

            </div><!-- End .input-group -->

          <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
              <span class="help-block"><p class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></p></span>
          <?php } ?>



          <?php } ?>



          <?php if ($custom_field['type'] == 'file') { ?>



          <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">



            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>



            <div class="col-sm-10">



              <button type="button" id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>



              <input type="hidden" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : ''); ?>" />



              <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>



              <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>



              <?php } ?>



            </div>



          </div>



          <?php } ?>



          <?php if ($custom_field['type'] == 'date') { ?>



          <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">



            <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>



            <div class="col-sm-10">



              <div class="input-group date">



                <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />



                <span class="input-group-btn">



                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>



                </span></div>



              <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>



              <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>



              <?php } ?>



            </div>



          </div>



          <?php } ?>



          <?php if ($custom_field['type'] == 'time') { ?>



          <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">



            <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>



            <div class="col-sm-10">



              <div class="input-group time">



                <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />



                <span class="input-group-btn">



                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>



                </span></div>



              <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>



              <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>



              <?php } ?>



            </div>



          </div>



          <?php } ?>



          <?php if ($custom_field['type'] == 'datetime') { ?>



          <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">



            <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>



            <div class="col-sm-10">



              <div class="input-group datetime">



                <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />



                <span class="input-group-btn">



                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>



                </span></div>



              <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>



              <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>



              <?php } ?>



            </div>



          </div>



          <?php } ?>



          <?php } ?>



          <?php } ?>





                  



                  </fieldset>                 



              <hr>







                  <fieldset id="address">



                  <h2 class="sub-title"><?php echo $text_your_address; ?></h2>



                  <div class="input-group">



                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_company; ?></span></span>



                    <input type="text" name="company" value="<?php echo $company; ?>" placeholder="<?php echo $entry_company; ?>" id="input-company" class="form-control input-lg">



                  </div><!-- End .input-group -->



                  

                   <div class="input-group <?php if ($error_postcode) { ?>has-error<?php }?>">



                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_postcode; ?></span></span>



                    <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" class="form-control input-lg">                    



                  </div><!-- End .input-group -->                 

                        <?php if ($error_postcode) { ?>
                            <span class="help-block"><p class="text-danger"><?php echo $error_postcode; ?></p></span>
                        <?php } else { ?>
                         <span class="help-block"><?php echo $entry_postinfo; ?> <img src="catalog/view/images/loader.gif" alt="" style="display:inline; width:15px;" /></span>
                         <?php } ?>





                  <div class="input-group <?php if ($error_address_1) { ?>has-error<?php }?>">



                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_address_1; ?></span></span>



                    <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1" class="form-control input-lg">



                  </div><!-- End .input-group -->

                  <?php if ($error_address_1) { ?>
                            <span class="help-block"><p class="text-danger"><?php echo $error_address_1; ?></p></span>
                          <?php } ?>



                  <div class="input-group <?php if ($error_address_1) { ?>has-error<?php }?>">



                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_address_2; ?></span></span>



                    <input type="text" name="address_2" value="<?php echo $address_2; ?>" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2" class="form-control input-lg">



                  </div><!-- End .input-group -->

                          <?php if ($error_address_1) { ?>
                            <span class="help-block"><p class="text-danger"><?php echo $error_address_1; ?></p></span>
                          <?php } ?>



                  <div class="input-group <?php if ($error_city) { ?>has-error<?php } ?>">



                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_city; ?></span></span>



                    <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" id="input-city" class="form-control input-lg">



                  </div><!-- End .input-group -->


                    <?php if ($error_city) { ?>
                          <span class="help-block"><p class="text-danger"><?php echo $error_city; ?></p></span>
                    <?php } ?>




                 

                  <div class="input-group <?php if ($error_country) { ?>has-error<?php }?>">



                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_country; ?></span></span>



                    <select name="country_id" id="input-country" class="form-control input-lg">



                      <option value=""><?php echo $text_select; ?></option>



                      <?php foreach ($countries as $country) { ?>



                      <?php if ($country['country_id'] == $country_id) { ?>



                      <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>



                      <?php } else { ?>



                      <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>



                      <?php } ?>



                      <?php } ?>



                    </select>



                  </div><!-- End .input-group -->
                       <?php if ($error_country) { ?>
                        <span class="help-block"><p class="text-danger"><?php echo $error_country; ?></p></span>
                      <?php } ?>



                  <div class="input-group <?php if ($error_zone) { ?>has-error<?php }?>">



                    <span class="input-group-addon"><span class="input-text"><?php echo $entry_zone; ?></span></span>



                    <select  name="zone_id" id="input-zone" class="form-control input-lg">



                    </select>



                  </div><!-- End .input-group -->
 

                <?php if ($error_zone) { ?>
                  <span class="help-block"><p class="text-danger"><?php echo $error_zone; ?></p></span>
                <?php } ?>





          <?php foreach ($custom_fields as $custom_field) { ?>



          <?php if ($custom_field['location'] == 'address') { ?>
<hr>
 <h2 class="sub-title">Outros Dados de Endereço</h2>

          <?php if ($custom_field['type'] == 'select') { ?>

              <div class="input-group custom-field <?php if ($error_zone) { ?>has-error<?php }?>" id="custom-field<?php echo $custom_field['custom_field_id']; ?>"  data-sort="<?php echo $custom_field['sort_order']; ?>">



                    <span class="input-group-addon"><span class="input-text"><?php echo $custom_field['name']; ?></span></span>


              <select name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control input-lg">



                <option value=""><?php echo $text_select; ?></option>



                <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>



                <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>



                <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>" selected="selected"><?php echo $custom_field_value['name']; ?></option>



                <?php } else { ?>



                <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>



                <?php } ?>



                <?php } ?>



              </select>



                  </div><!-- End .input-group -->


              <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>



              <span class="help-block"><p class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></p></span>



              <?php } ?>


          <?php } ?>



          <?php if ($custom_field['type'] == 'radio') { ?>



                  <div class="input-group custom-field <?php if ($error_zone) { ?>has-error<?php }?>" id="custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">



                    <span class="input-group-addon" style="text-align:left; padding-right:.5em"><span class="input-text" style="font-weight:bold;"><?php echo $custom_field['name']; ?>:</span>


        <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>



                <div class="radio-inline">



                  <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>



                  <label>



                    <input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />



                    <?php echo $custom_field_value['name']; ?></label>



                  <?php } else { ?>



                  <label>



                    <input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />



                    <?php echo $custom_field_value['name']; ?></label>



                  <?php } ?>



                </div>



                <?php } ?>



                  </div><!-- End .input-group -->
 

                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                  <span class="help-block"><p class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></p></span>
                <?php } ?>
          <?php } ?>



          <?php if ($custom_field['type'] == 'checkbox') { ?>


  <div class="input-group custom-field <?php if ($error_zone) { ?>has-error<?php }?>" id="custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">



                    <span class="input-group-addon" style="text-align:left; padding-right:.5em"><span class="input-text" style="font-weight:bold;"><?php echo $custom_field['name']; ?>:</span>


        <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>



                <div class="checkbox-inline">



                  <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>



                  <label>



                    <input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />



                    <?php echo $custom_field_value['name']; ?></label>



                  <?php } else { ?>



                  <label>



                    <input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />



                    <?php echo $custom_field_value['name']; ?></label>



                  <?php } ?>



                </div>



                <?php } ?>



                  </div><!-- End .input-group -->
 

                <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
                  <span class="help-block"><p class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></p></span>
                <?php } ?>


          <?php } ?>



       
          <?php if ($custom_field['type'] == 'text') { ?>



            <div class="input-group custom-field <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>has-error<?php } ?>" id="custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">


              <span class="input-group-addon"><span class="input-text"><?php echo $custom_field['name']; ?></span></span>

              <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control input-lg" />

            </div><!-- End .input-group -->

          <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
              <span class="help-block"><p class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></p></span>
          <?php } ?>

          <?php } ?>



          <?php if ($custom_field['type'] == 'textarea') { ?>



            <div class="input-group custom-field <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>has-error<?php } ?>" id="custom-field<?php echo $custom_field['custom_field_id']; ?>" data-sort="<?php echo $custom_field['sort_order']; ?>">


              <span class="input-group-addon"><span class="input-text"><?php echo $custom_field['name']; ?></span></span>

              <textarea name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control input-lg" row="3" cols="5"></textarea>

            </div><!-- End .input-group -->

          <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
              <span class="help-block"><p class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></p></span>
          <?php } ?>



          <?php } ?>



          <?php if ($custom_field['type'] == 'file') { ?>



          <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">



            <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>



            <div class="col-sm-10">



              <button type="button" id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>



              <input type="hidden" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : ''); ?>" />



              <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>



              <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>



              <?php } ?>



            </div>



          </div>



          <?php } ?>



          <?php if ($custom_field['type'] == 'date') { ?>



          <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">



            <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>



            <div class="col-sm-10">



              <div class="input-group date">



                <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />



                <span class="input-group-btn">



                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>



                </span></div>



              <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>



              <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>



              <?php } ?>



            </div>



          </div>



          <?php } ?>



          <?php if ($custom_field['type'] == 'time') { ?>



          <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">



            <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>



            <div class="col-sm-10">



              <div class="input-group time">



                <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />



                <span class="input-group-btn">



                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>



                </span></div>



              <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>



              <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>



              <?php } ?>



            </div>



          </div>



          <?php } ?>



          <?php if ($custom_field['type'] == 'datetime') { ?>



          <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">



            <label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>



            <div class="col-sm-10">



              <div class="input-group datetime">



                <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />



                <span class="input-group-btn">



                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>



                </span></div>



              <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>



              <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>



              <?php } ?>



            </div>



          </div>



          <?php } ?>



          <?php } ?>



          <?php } ?>          


                  



                  </fieldset>



                  </div><!-- End .col-md-12 col-sm-12 col-xs-12 -->







                  
























































   







                      <div class="col-md-12 col-sm-12 col-xs-12">







                          <fieldset>



                          <h2 class="sub-title"><?php echo $text_newsletter; ?></h2>



                           <?php if ($newsletter) { ?>



                            <label class="radio-inline">



                              <input type="radio" name="newsletter" value="1" checked="checked" />



                              <?php echo $text_yes; ?></label>



                            <label class="radio-inline">



                              <input type="radio" name="newsletter" value="0" />



                              <?php echo $text_no; ?></label>



                            <?php } else { ?>



                            <label class="radio-inline">



                              <input type="radio" name="newsletter" value="1" />



                              <?php echo $text_yes; ?></label>



                            <label class="radio-inline">



                              <input type="radio" name="newsletter" value="0" checked="checked" />



                              <?php echo $text_no; ?></label>



                            <?php } ?>



                          </fieldset>







                    </div><!-- col-12 -->



                  



                







        <?php if ($text_agree) { ?>



        <div class="col-md-12 col-sm-12 col-xs-12">



            <fieldset>



              <h2 class="sub-title">
                <?php if ($agree) { ?>



            <label><input type="checkbox" name="agree" value="1" checked="checked" /> <?php echo $text_yes; ?>, </label>



            <?php } else { ?>



             <label><input type="checkbox" name="agree" value="1" /> <?php echo $text_yes; ?>, </label>



            <?php } ?> <?php echo $text_agree; ?></h2>






            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-block btn-custom" />



            </fieldset>



            </div><!-- col-12 -->



        <?php } else { ?>



        <div class="col-md-12 col-sm-12 col-xs-12">



           <fieldset>



            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-block btn-custom" />



          </fieldset>  



          </div><!-- col-12 -->    



        <?php } ?>






        </div><!-- row -->



      </form>







      <?php echo $content_bottom; ?></div>



    <?php echo $column_right; ?></div>



</div> 



<script type="text/javascript"><!--



// Sort the custom fields



$('#account .form-group[data-sort]').detach().each(function() {



	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {



		$('#account .form-group').eq($(this).attr('data-sort')).before(this);



	} 



	



	if ($(this).attr('data-sort') > $('#account .form-group').length) {



		$('#account .form-group:last').after(this);



	}



		



	if ($(this).attr('data-sort') < -$('#account .form-group').length) {



		$('#account .form-group:first').before(this);



	}



});







$('#address .form-group[data-sort]').detach().each(function() {



	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {



		$('#address .form-group').eq($(this).attr('data-sort')).before(this);



	} 



	



	if ($(this).attr('data-sort') > $('#address .form-group').length) {



		$('#address .form-group:last').after(this);



	}



		



	if ($(this).attr('data-sort') < -$('#address .form-group').length) {



		$('#address .form-group:first').before(this);



	}



});







$('input[name=\'customer_group_id\']').on('change', function() {



	$.ajax({



		url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,



		dataType: 'json',



		success: function(json) {



			$('.custom-field').hide();



			$('.custom-field').removeClass('required');







			for (i = 0; i < json.length; i++) {



				custom_field = json[i];







				$('#custom-field' + custom_field['custom_field_id']).show();







				if (custom_field['required']) {



					$('#custom-field' + custom_field['custom_field_id']).addClass('required');



				}



			}



			







		},



		error: function(xhr, ajaxOptions, thrownError) {



			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);



		}



	});



});







$('input[name=\'customer_group_id\']:checked').trigger('change');



//--></script> 



<script type="text/javascript"><!--



$('button[id^=\'button-custom-field\']').on('click', function() {



	var node = this;







	$('#form-upload').remove();







	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');







	$('#form-upload input[name=\'file\']').trigger('click');







	timer = setInterval(function() {



		if ($('#form-upload input[name=\'file\']').val() != '') {



			clearInterval(timer);



			



			$.ajax({



				url: 'index.php?route=tool/upload',



				type: 'post',



				dataType: 'json',



				data: new FormData($('#form-upload')[0]),



				cache: false,



				contentType: false,



				processData: false,



				beforeSend: function() {



					$(node).button('loading');



				},



				complete: function() {



					$(node).button('reset');



				},



				success: function(json) {



					$(node).parent().find('.text-danger').remove();



					



					if (json['error']) {



						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');



					}



	



					if (json['success']) {



						alert(json['success']);







						$(node).parent().find('input').attr('value', json['code']);



					}



				},



				error: function(xhr, ajaxOptions, thrownError) {



					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);



				}



			});



		}



	}, 500);



});



//--></script> 



<script type="text/javascript"><!--



$('.date').datetimepicker({



	pickTime: false



});







$('.time').datetimepicker({



	pickDate: false



});







$('.datetime').datetimepicker({



	pickDate: true,



	pickTime: true



});



//--></script> 



<script type="text/javascript"><!--



$('select[name=\'country_id\']').on('change', function() {



	$.ajax({



		url: 'index.php?route=account/account/country&country_id=' + this.value,



		dataType: 'json',



		beforeSend: function() {



			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');



		},



		complete: function() {



			$('.fa-spin').remove();



		},



		success: function(json) {



			if (json['postcode_required'] == '1') {



				$('input[name=\'postcode\']').parent().parent().addClass('required');



			} else {



				$('input[name=\'postcode\']').parent().parent().removeClass('required');



			}



			



			html = '<option value=""><?php echo $text_select; ?></option>';



			



			if (json['zone'] != '') {



				for (i = 0; i < json['zone'].length; i++) {



					html += '<option value="' + json['zone'][i]['zone_id'] + '"';



					



					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {



						html += ' selected="selected"';



					}



				



					html += '>' + json['zone'][i]['name'] + '</option>';



				}



			} else {



				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';



			}



			



			$('select[name=\'zone_id\']').html(html);



		},



		error: function(xhr, ajaxOptions, thrownError) {



			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);



		}



	});



});







$('select[name=\'country_id\']').trigger('change');



//--></script>




					<script type="text/javascript">
						$(function(){
							$('#input-postcode').blur(function(){
								var cep = $.trim($('#input-postcode').val().replace('-', ''));
				
								$.getJSON("https://viacep.com.br/ws/"+cep+"/json/", function(data) {
									var resultadoCEP = data;
									if(resultadoCEP["logradouro"] != "" && resultadoCEP["logradouro"] != undefined){
										$('#input-address-1').val(unescape(resultadoCEP["logradouro"]));
										$('#input-address-2').val(unescape(resultadoCEP["bairro"]));
										$('#input-city').val(unescape(resultadoCEP["localidade"]));

										$('#input-country').find('option[value="30"]').attr('selected', true);
										$.post('index.php?route=account/register/estado_autocompletar&estado=' + unescape(resultadoCEP['uf']), function(zone_id){
											$.ajax({
												url: 'index.php?route=account/account/country&country_id=30',
												dataType: 'json',
												beforeSend: function() {
													$('#input-country').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
												},
												complete: function() {
													$('.wait').remove();
												},			
												success: function(json) {
													if (json['postcode_required'] == '1') {
														$('input[name=\'postcode\']').parent().parent().addClass('required');
													} else {
														$('input[name=\'postcode\']').parent().parent().removeClass('required');
													}
			
													var html = '<option value=""><?php echo $text_select; ?></option>';
			
													if (json['zone'] != '') {
														for (i = 0; i < json['zone'].length; i++) {
															html += '<option value="' + json['zone'][i]['zone_id'] + '"';
															
															if (json['zone'][i]['zone_id'] == zone_id) {
																html += ' selected="selected"';
															}
											
															html += '>' + json['zone'][i]['name'] + '</option>';
														}
													} else {
														html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
													}
			
													$('#input-zone').html(html);
												}
											});
										});
									} else if (resultadoCEP.erro) {
										$('#input-postcode').val('');
										alert('CEP não encontrado!');
										$('#input-postcode').focus();
									}
								});
							});
						});
						$(document).ready(function() {
							if ($('#input-postcode').val().length==8 || $('#input-postcode').val().length==9) {
								$('#input-postcode').trigger('blur');
							}
						});
					</script>
				
<?php echo $footer; ?>