<?php echo $header; ?>

<div class="container">

  <div id="category-breadcrumb">

    <div class="container">

      <ul class="breadcrumb">

        <?php foreach ($breadcrumbs as $breadcrumb) { ?>

        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

        <?php } ?>

      </ul>

    </div>

  </div><!-- category-breadcrumb -->



  <?php if ($error_warning) { ?>

  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>

  <?php } ?>

  <div class="row"><?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      <header class="content-title">

            <h1 class="title"><?php echo $heading_title; ?></h1>  

      </header>

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">

        <fieldset>

          <ul style="padding:1.5em; list-style:inside;">

            <?php if ($error_firstname) { ?>

              <li class="text-danger"><?php echo $error_firstname; ?></li>

              <?php } ?>

            <?php if ($error_lastname) { ?>

              <li class="text-danger"><?php echo $error_lastname; ?></li>

              <?php } ?>

            <?php if ($error_email) { ?>

              <li class="text-danger"><?php echo $error_email; ?></li>

              <?php } ?>

            <?php if ($error_telephone) { ?>

              <li class="text-danger"><?php echo $error_telephone; ?></li>

              <?php } ?>

            <?php if ($error_address_1) { ?>

              <li class="text-danger"><?php echo $error_address_1; ?></li>

              <?php } ?>

            <?php if ($error_city) { ?>

              <li class="text-danger"><?php echo $error_city; ?></li>

              <?php } ?>

            <?php if ($error_postcode) { ?>

              <li class="text-danger"><?php echo $error_postcode; ?></li>

              <?php } ?>

            <?php if ($error_country) { ?>

              <li class="text-danger"><?php echo $error_country; ?></li>

              <?php } ?>

            <?php if ($error_zone) { ?>

              <li class="text-danger"><?php echo $error_zone; ?></li>

              <?php } ?>

           </ul>

          <h2 class="checkout-title"><?php echo $text_your_details; ?></h2>

          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_firstname; ?></span></span>

            <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control input-lg" />

          </div><!-- End .input-group -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_lastname; ?></span></span>

            <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control input-lg" />

          </div><!-- End .input-group -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_email; ?></span></span>

            <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control input-lg" />

          </div><!-- End .input-group -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_telephone; ?></span></span>

            <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control input-lg" />

          </div><!-- End .input-group -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_fax; ?></span></span>

            <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="form-control input-lg" />

          </div><!-- End .input-group -->

        </fieldset>

        <fieldset>

          <h2 class="checkout-title"><?php echo $text_your_address; ?></h2>

          

                    <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_company; ?></span></span>

            <input type="text" name="company" value="<?php echo $company; ?>" placeholder="<?php echo $entry_company; ?>" id="input-company" class="form-control input-lg" />

          </div><!-- End .input-group -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_website; ?></span></span>

            <input type="text" name="website" value="<?php echo $website; ?>" placeholder="<?php echo $entry_website; ?>" id="input-website" class="form-control input-lg" />

          </div><!-- End .input-group -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_address_1; ?></span></span>

            <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1" class="form-control input-lg" />

          </div><!-- End .input-group -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_address_2; ?></span></span>

            <input type="text" name="address_2" value="<?php echo $address_2; ?>" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2" class="form-control input-lg" />

          </div><!-- End .input-group -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_city; ?></span></span>

            <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" id="input-city" class="form-control input-lg" />

          </div><!-- End .input-group -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_postcode; ?></span></span>

            <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode" class="form-control input-lg" />

          </div><!-- End .input-group -->



          <div class="input-group">

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_country; ?></span></span>

            <select name="country_id" id="input-country" class="form-control input-lg">

                <option value="false"><?php echo $text_select; ?></option>

                <?php foreach ($countries as $country) { ?>

                <?php if ($country['country_id'] == $country_id) { ?>

                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>

                <?php } else { ?>

                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>

                <?php } ?>

                <?php } ?>

              </select>              

          </div><!-- End .input-group -->





          <div class="input-group">   

            <span class="input-group-addon"><span class="input-text"><?php echo $entry_zone; ?></span></span>

           <select name="zone_id" id="input-zone" class="form-control input-lg">

              </select>

              <?php if ($error_zone) { ?>

              <div class="text-danger"><?php echo $error_zone; ?></div>

              <?php } ?>                          

          </div><!-- End .input-group -->





        </fieldset>

        <div class="buttons clearfix">

          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>

          <div class="pull-right">

            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-custom" />

          </div>

        </div>

      </form>

      <?php echo $content_bottom; ?></div>

    <?php echo $column_right; ?></div>

</div>

<script type="text/javascript"><!--

$('select[name=\'country_id\']').on('change', function() {

	$.ajax({

		url: 'index.php?route=affiliate/edit/country&country_id=' + this.value,

		dataType: 'json',

		beforeSend: function() {

			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');

		},

		complete: function() {

			$('.fa-spin').remove();

		},

		success: function(json) {

      $('.fa-spin').remove();



			if (json['postcode_required'] == '1') {

				$('input[name=\'postcode\']').parent().parent().addClass('required');

			} else {

				$('input[name=\'postcode\']').parent().parent().removeClass('required');

			}



			html = '<option value=""><?php echo $text_select; ?></option>';



			if (json['zone'] != '') {

				for (i = 0; i < json['zone'].length; i++) {

					html += '<option value="' + json['zone'][i]['zone_id'] + '"';



					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {

						html += ' selected="selected"';

					}



					html += '>' + json['zone'][i]['name'] + '</option>';

				}

      } else {

				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';

			}



			$('select[name=\'zone_id\']').html(html);

		},

		error: function(xhr, ajaxOptions, thrownError) {

			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

		}

	});

});



$('select[name=\'country_id\']').trigger('change');

//--></script>


					<script type="text/javascript">
						$(function(){
							$('#input-postcode').blur(function(){
								var cep = $.trim($('#input-postcode').val().replace('-', ''));
				
								$.getJSON("https://viacep.com.br/ws/"+cep+"/json/", function(data) {
									var resultadoCEP = data;
									if(resultadoCEP["logradouro"] != "" && resultadoCEP["logradouro"] != undefined){
										$('#input-address-1').val(unescape(resultadoCEP["logradouro"]));
										$('#input-address-2').val(unescape(resultadoCEP["bairro"]));
										$('#input-city').val(unescape(resultadoCEP["localidade"]));

										$('#input-country').find('option[value="30"]').attr('selected', true);
										$.post('index.php?route=account/register/estado_autocompletar&estado=' + unescape(resultadoCEP['uf']), function(zone_id){
											$.ajax({
												url: 'index.php?route=account/account/country&country_id=30',
												dataType: 'json',
												beforeSend: function() {
													$('#input-country').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
												},
												complete: function() {
													$('.wait').remove();
												},			
												success: function(json) {
													if (json['postcode_required'] == '1') {
														$('input[name=\'postcode\']').parent().parent().addClass('required');
													} else {
														$('input[name=\'postcode\']').parent().parent().removeClass('required');
													}
			
													var html = '<option value=""><?php echo $text_select; ?></option>';
			
													if (json['zone'] != '') {
														for (i = 0; i < json['zone'].length; i++) {
															html += '<option value="' + json['zone'][i]['zone_id'] + '"';
															
															if (json['zone'][i]['zone_id'] == zone_id) {
																html += ' selected="selected"';
															}
											
															html += '>' + json['zone'][i]['name'] + '</option>';
														}
													} else {
														html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
													}
			
													$('#input-zone').html(html);
												}
											});
										});
									} else if (resultadoCEP.erro) {
										$('#input-postcode').val('');
										alert('CEP não encontrado!');
										$('#input-postcode').focus();
									}
								});
							});
						});
						$(document).ready(function() {
							if ($('#input-postcode').val().length==8 || $('#input-postcode').val().length==9) {
								$('#input-postcode').trigger('blur');
							}
						});
					</script>
				
<?php echo $footer; ?>