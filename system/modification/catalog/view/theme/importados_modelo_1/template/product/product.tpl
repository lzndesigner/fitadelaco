<?php echo $header; ?>

<div class="breadcrumb clearfix">

  <div class="container">
  </div>

</div><!-- breadcrumb -->

<div class="container">



  <div class="row"><?php echo $column_left; ?>



    <?php if ($column_left && $column_right) { ?>



    <?php $class = 'col-sm-6'; ?>



    <?php } elseif ($column_left || $column_right) { ?>



    <?php $class = 'col-sm-9'; ?>



    <?php } else { ?>



    <?php $class = 'col-sm-12'; ?>



    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <section class="productDetail">

        <div class="basicDetails">

          <div class="row">



            <div class="col-xs-12 col-sm-6">
              <?php if ($thumb || $images) { ?>

              <div class="productGallery clearfix slider-for">

                <?php if ($thumb) { ?>                                      

                <figure>
                  <script type="text/javascript">
                  $(document).ready(function () {
                          $("#zoom_05").elevateZoom({
                              zoomType        : "inner",
                              debug : true,
                              cursor: "crosshair",
                              zoomWindowFadeIn: 500,
                              zoomWindowFadeOut: 500
                              }); 
                  }); 
                  $("#zoom_05").bind("click", function(e) {  
                    var ez =   $('#zoom_05').data('elevateZoom'); 
                    $.fancybox(ez.getGalleryList());
                    return false;
                  }); 
                  </script>
                  <img id="zoom_05" src="<?php echo $thumb; ?>" data-zoom-image="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>">
                  <figcaption>
                    <a data-rel="prettyPhoto" href="<?php echo $thumb; ?>">
                      <i class="xv-arrows_expand"></i></a>
                    </figcaption>
                  </figure>

                  <?php }// end if $thumb ?>

                  <?php if ($images) { ?>

                  <?php foreach ($images as $image) { ?>

                  <figure>
                    <img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>">

                    <figcaption>

                      <a data-rel="prettyPhoto" href="<?php echo $image['popup'] ?>">

                        <i class="xv-arrows_expand"></i></a>

                      </figcaption>

                    </figure>

                    <?php }// end foreach images as image ?>

                    <?php }// end if images ?>

                  </div><!-- productGallery -->
                  <?php if ($price) { ?>

                  <?php if ($special) { ?>

                  <span class="tag"><small><?php echo $percent; ?></small></span>

                  <?php }} ?>

                  <div class="slider-wraper medium">

                    <ul class="slider-nav" >

                      <?php if ($thumb) { ?>   

                      <li>

                        <figure><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>">

                         <figcaption></figcaption>

                       </figure>

                     </li>

                     <?php } ?>

                     <?php if ($images) { ?>

                     <?php foreach ($images as $image) { ?>

                     <li>

                      <figure><img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>">

                        <figcaption></figcaption>

                      </figure>

                    </li>

                    <?php } ?>

                    <?php } ?>

                  </ul>

                </div>
                <?php }// end if thumb || images ?> 


                <div class="share-button-group">

                  <!-- AddThis Button BEGIN -->
                  <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> 
                  <!-- AddThis Button END --> 


                </div><!-- End .share-button-group -->

                <?php if ($description) {?>
                <div class="description"><?php echo $description; ?></div>
                <?php }// end if description ?>

              </div><!-- col-sm-6 -->




              <div class="col-xs-12 col-sm-6">




                <span class="article">
                  <?php if($model){ ?><?php echo $text_model; ?> <?php echo $model; ?><?php } ?>
                  <?php if ($reward) { ?>| <?php echo $text_reward; ?> 
            <span class="<?php echo $live_options['live_options_reward_container']; ?>"><?php echo $reward; ?></span>
            <?php } ?>
                </span>
              <?php if ($minimum > 1) { ?>
              <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
              <?php } ?>

                <h2><?php echo $heading_title; ?></h2>                        
                <?php if ($manufacturer) { ?><h4><a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></h4><?php } ?>
                
                <hr class="invisible">

              <?php if(!$quantity == '0'){ ?>
              <div id="product">

               <?php if ($options) { ?>

               <?php foreach ($options as $option) { ?>

               <?php if ($option['type'] == 'select') { ?>

               <div class="itemDesc clearfix">

                <div class="name">

                  <label><?php echo $option['name']; ?> <?php if($option['required']){ echo "*";} ?></label>

                </div>

                <div class="cont">

                  <div class="">

                    <b class="fa fa-caret-down"></b>

                    <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>">

                      <option selected="" value="" disabled><?php echo $text_select; ?></option>

                      <?php foreach ($option['product_option_value'] as $option_value) { ?>

                      <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?> </option>

                      <?php } ?>

                    </select>

                  </div>

                </div>

              </div><!-- itemDesc -->

              <?php }// end type select ?>

              <?php if ($option['type'] == 'radio') { ?>

              <div class="itemDesc clearfix">

                <div class="name">

                  <label><?php echo $option['name']; ?> <?php if($option['required']){ echo "*";} ?></label>

                </div>

                <div class="cont">

                  <div class="">

                    <div id="input-option<?php echo $option['product_option_id']; ?>">

                      <ul class="ulRadiosCss">

                        <?php foreach ($option['product_option_value'] as $option_value) { ?>

                        <li>
                          <div>
                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" id="option<?php echo $option_value['product_option_value_id']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" />

                            <label for="option<?php echo $option_value['product_option_value_id']; ?>">

                              <?php echo $option_value['name']; ?>

                              <?php if ($option_value['image']) { ?><img src="<?php echo $option_value['image']; ?>" alt="" width="50" /><?php } ?>

                            </label>
                          </div>

                        </li>

                        <?php } ?>

                      </ul>

                    </div>

                  </div>

                </div>

              </div><!-- itemDesc -->

              <?php }//end type radio ?>

              <?php if ($option['type'] == 'checkbox') { ?>

              <div class="itemDesc clearfix">

                <div class="name">

                  <label><?php echo $option['name']; ?> <?php if($option['required']){ echo "*";} ?></label>

                </div>

                <div class="cont">

                  <div class="">

                    <div id="input-option<?php echo $option['product_option_id']; ?>">

                      <ul class="ulCheckCss">

                        <?php foreach ($option['product_option_value'] as $option_value) { ?>

                        <li>
                          <div>

                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" id="option<?php echo $option_value['product_option_value_id']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" />

                            <label for="option<?php echo $option_value['product_option_value_id']; ?>" title="Estoque: <?php echo $option_value['qta']; ?>" data-toggle="tooltip" data-placement="top">
                              <?php echo $option_value['name']; ?>

                            </label>
                          </div>

                        </li>

                        <?php } ?>

                      </ul>

                    </div>

                  </div>

                </div>

              </div><!-- itemDesc -->

              <?php }// end type checkbox ?>

              <?php if ($option['type'] == 'image') { ?>

              <div class="itemDesc clearfix">

                <div class="name">

                  <label><?php echo $option['name']; ?> <?php if($option['required']){ echo "*";} ?></label>

                </div>

                <div class="cont">

                  <div class="">

                    <div id="input-option<?php echo $option['product_option_id']; ?>">

                      <ul>

                        <?php foreach ($option['product_option_value'] as $option_value) { ?>



                        <li>



                          <label>

                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />

                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>

                            <?php if ($option_value['price']) { ?>

                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)

                            <?php } ?>

                          </label>

                        </li>

                        <?php } ?>

                      </ul>

                    </div>

                  </div>

                </div>

              </div><!-- itemDesc -->

              <?php }// end type image ?>

              <?php if ($option['type'] == 'text') { ?>

              <div class="itemDesc clearfix">

                <div class="name">

                  <label><?php echo $option['name']; ?> <?php if($option['required']){ echo "*";} ?></label>

                </div>

                <div class="cont">

                  <div class="">

                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />

                  </div>

                </div>

              </div><!-- itemDesc -->

              <?php }//end type text ?>

              <?php if ($option['type'] == 'textarea') { ?>

              <div class="itemDesc clearfix">

                <div class="name">

                  <label><?php echo $option['name']; ?> <?php if($option['required']){ echo "*";} ?></label>

                </div>

                <div class="cont">

                  <div class="">

                    <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>

                  </div>

                </div>

              </div><!-- itemDesc -->

              <?php }// end type textarea ?>



              <?php if ($option['type'] == 'file') { ?>

              <div class="itemDesc clearfix">

                <div class="name">

                  <label><?php echo $option['name']; ?> <?php if($option['required']){ echo "*";} ?></label>

                </div>

                <div class="cont">

                  <div class="">

                    <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>

                    <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />

                  </div>

                </div>

              </div><!-- itemDesc -->
              <?php }// end type file ?>



              <?php if ($option['type'] == 'date') { ?>
              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <div class="input-group date">
                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />

                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                  </span>
                </div>
              </div><!-- form-group -->
              <?php } ?>

              <?php if ($option['type'] == 'datetime') { ?>
              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <div class="input-group datetime">
                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
                </div>
              </div><!-- form-group -->
              <?php }// end type datetime ?>



              <?php if ($option['type'] == 'time') { ?>
              <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <div class="input-group time">
                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span>
                </div>
              </div><!-- form-group -->
              <?php }// end type time ?>

              <?php }//foreach options as option ?>

              <?php }//if option ?>


                <!-- preco -->
                <div class="col-xs-7 col-md-7">
                  
                <?php if ($price) { ?>

                <?php if (!$special) { ?>

                <span class="price">
            <span class="<?php echo $live_options['live_options_price_container']; ?>"><?php echo $price; ?></span>
            </span>

                <?php } else { ?>                

                <span class="price">
                 <span class="priceOld"><strike>
            <span class="<?php echo $live_options['live_options_price_container']; ?>"><?php echo $price; ?></span>
            </strike></span>
                 <br>
                 
            <span class="<?php echo $live_options['live_options_special_container']; ?>"><?php echo $special; ?></span>
            
                 <?php if($saving){ ?><p><i class="fa fa-tags"></i> <?php echo $saving_text; ?> <b><?php echo $saving; ?></b></p><?php } ?>
               </span>

               <?php } ?>

               <?php if ($points) { ?>

               <span class="price"><?php echo $text_points; ?> 
            <span class="<?php echo $live_options['live_options_points_container']; ?>"><?php echo $points; ?></span>
            </span>

               <?php } ?>

               <?php if ($discounts) { ?>                                      

               <ul>

                <?php foreach ($discounts as $discount) { ?>

                <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>

                <?php } ?>

              </ul>

              <?php } ?>

              <hr>

              <?php } ?>

              <!-- preco -->

              <?php if ($recurrings) { ?>

              <hr>

              <h3><?php echo $text_payment_recurring ?></h3>

              <div class="form-group required">

                <select name="recurring_id" class="form-control">

                  <option value=""><?php echo $text_select; ?></option>

                  <?php foreach ($recurrings as $recurring) { ?>

                  <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>

                  <?php } ?>

                </select>

                <div class="help-block" id="recurring-description"></div>

              </div>

              <?php } ?>
              </div><!-- col-md-6 -->
              <div class="col-xs-5 col-md-5">
               
                <script>
                $(document).ready(function() {

                   if($("#input-quantity").val() <= 1){
                    $('#menos').addClass('disabled');                        
                    }else{
                    $('#menos').removeClass('disabled');  
                    }

                  $("#mais").click(function(){
                    if($("#input-quantity").val() >= 1){
                    $('#menos').removeClass('disabled');  
                    $('#input-quantity').val(parseInt($('#input-quantity').val())+1);
                    }

                  });// mais

                  $("#menos").click(function(){
                      if($("#input-quantity").val()!=1){                                        
                        $('#input-quantity').val(parseInt($('#input-quantity').val())-1);         
                        if($("#input-quantity").val() <= 1){
                              $('#menos').addClass('disabled');  
                          }             
                      }
                  });// menos
                });
                </script>


                <div class="btn-group btn-quantity">
                  <b>Quantidade</b><br>
                  <button type="button" id="menos" class="btn btn-purple btn-sm"><i class="fa fa-minus"></i></button>
                  <input type="text" id="input-quantity" name="quantity" value="1" min="1">
                  <button type="button" id="mais" class="btn btn-purple  btn-sm"><i class="fa fa-plus"></i></button>
                </div>
              </div><!-- cols-5 -->

              <div class="itemDesc clearfix">
                <div class="cont" style="width:100%;">
                  <div class="second" style="width:40%; margin-right:0.5%; float:left;"> 
                    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                    <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-cart-product btn-fullwidth btn-round-5px"><i class="fa fa-shopping-cart"></i> <?php echo $button_cart; ?></button>
                  </div>
                  <div class="second" style="width:55%; margin:0; float:left;">
                          <a href="<?php echo $goCart; ?>" class="btn btn-purple btn-product btn-fullwidth btn-round-5px"><i class="fa fa-bag"></i> Ir para o Carrinho</a>
                  </div>
                </div><!-- cont -->
                  <hr>
                <div class="cont" style="width:100%;">


                  <ul class="ulWishCompare">
                      <li><a onclick="wishlist.add('<?php echo $product_id; ?>');"><i class="fa fa-heart"></i> <?php echo $text_whistlist; ?></a></li>
                      <li><a onclick="compare.add('<?php echo $product_id; ?>');"><i class="fa fa-exchange"></i> <?php echo $button_compare; ?></a></li>
                    </ul>

                </div>

              </div><!-- itemDesc -->

              <hr>   

              <div id="limite"></div>


            </div><!-- #product -->

            <?php }else{ ?>



            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

            <script type="text/javascript">

            jQuery(document).ready(function(){

              jQuery('#toggle').click(function(){

                jQuery('#boxOrcamento').toggleClass('hidden');               

              }); 

            });

            </script>



            <div class="alert alert-danger" id="alertestoque"><h4><?php echo $text_product_esgotado; ?></h4>

              <p><?php echo $text_product_esgotado_desc; ?></p>

              <a id="toggle" class="btn btn-default btn-sm" style="padding:4.5px 12px; font-size:12px;"><?php echo $text_product_esgotado_button; ?></a>

            </div>



            <div id="boxOrcamento" class="hidden">

              <div id="orcamento">

                <div class="col-md-12">



                  <hr>

                  <h4><?php echo $text_title_product_esgotado; ?></h4>

                  <form action="" method="post" enctype="multipart/form-data" name="email">

                    <div class="form-group required" style="margin:.5em 0;">

                      <label class="control-label"><?php echo $text_your_name; ?></label>

                      <input type="text" name="nome" id="nome" placeholder="<?php echo $text_your_name; ?>"  required class="form-control">

                    </div>

                    <div class="form-group required" style="margin:.5em 0;">

                      <label class="control-label"><?php echo $text_your_email; ?></label>

                      <input type="text" name="email_from" id="email_from"  required placeholder="<?php echo $text_your_email; ?>" class="form-control">

                    </div>

                    <div class="form-group required" style="margin:.5em 0;">

                      <label class="control-label"><?php echo $text_your_city; ?></label>

                      <input type="text" name="cidade_estado" id="cidade_estado" required placeholder="<?php echo $text_your_city; ?>" class="form-control">

                    </div>

                    <div class="form-group required" style="margin:.5em 0;">

                      <label class="control-label"><?php echo $text_your_phone; ?></label>

                      <input type="text" name="telefone" id="telefone" required placeholder="(99) 9.9999-9999" class="form-control">

                    </div>

                    <div class="form-group required" style="margin:.5em 0;">

                      <label class="control-label"><?php echo $text_your_product; ?></label>

                      <input type="text" name="produto" id="produto" required value="<?php echo $heading_title; ?> - <?php echo $model; ?>" class="form-control">

                    </div>

                    <div class="form-group" style="margin:.5em 0;">

                      <label class="control-label"><?php echo $text_your_message; ?></label>

                      <textarea name="mensagem" id="mensagem" class="form-control"></textarea>

                    </div>

                    <div class="form-group required" style="margin:.5em 0;">

                      <input type="submit" name="enviar" value="<?php echo $text_button_notification; ?>" class="btn btn-default">

                    </div>

                  </form>

                </div>

              </div><!-- /orcamento -->

            </div><!-- /boxOrcamento -->

            <div class="clearfix"></div>

            <?php

            if(isset($_POST['enviar'])){

//pego os dados enviados pelo formulario

              $nome = stripslashes(trim($_POST["nome"]));

              $email = $email_admin;

              $mensagem = stripslashes(trim($_POST["mensagem"]));

              $cidade_estado = stripslashes(trim($_POST["cidade_estado"]));

              $produto = stripslashes(trim($_POST["produto"]));

              $telefone = stripslashes(trim($_POST["telefone"]));

              $assunto = utf8_decode('Estoque Baixo - '.$heading_title.'');

              $email_from = $_POST["email_from"];

//formato o campo da mensagem

              $mensagem = wordwrap( $mensagem, 50, "

                ", 1);

              $arquivo = isset($_FILES["arquivo"]) ? $_FILES["arquivo"] : FALSE;

              if(file_exists($arquivo["tmp_name"]) and !empty($arquivo)){

                $fp = fopen($_FILES["arquivo"]["tmp_name"],"rb");

                $anexo = fread($fp,filesize($_FILES["arquivo"]["tmp_name"]));

                $anexo = base64_encode($anexo);

                fclose($fp);

                $anexo = chunk_split($anexo);

                $boundary = "XYZ-" . date("dmYis") . "-ZYX";

                $mens = "--$boundary\n";

                $mens .= "Content-Transfer-Encoding: 8bits\n";

      $mens .= "Content-Type: text/html; charset=\"utf-8\"\n\n"; //plain

      $mens .= "Nome: <b>$nome</b><br/><br/>E-mail: <b>$email_from</b><br/><br/>Cidade/Estado: <b>$cidade_estado</b><br/><br/>Telefone: <b>$telefone</b><br/><br/>Assunto: <b>$assunto</b><br/><br/>Produto/Modelo: <b>$produto</b><br/><br/>Notas Adicionais: <b>$mensagem</b>\n";

      $mens .= "--$boundary\n";

      $mens .= "Content-Type: ".$arquivo["type"]."\n";

      $mens .= "Content-Disposition: attachment; filename=\"".$arquivo["name"]."\"\n";

      $mens .= "Content-Transfer-Encoding: base64\n\n";

      $mens .= "$anexo\n";

      $mens .= "--$boundary--\r\n";

      $headers = "MIME-Version: 1.0\n";

      $headers .= "From: \"$nome\" <$email_from>\r\n";

      $headers .= "Return-Path: $email_from\r\n"; // return-path

      $headers .= "Content-type: multipart/mixed; boundary=\"$boundary\"\r\n";

      $headers .= "$boundary\n";

      //envio o email com o anexo

      mail($email,$assunto,$mens,$headers);

      echo"<div class='alert alert-success' style='margin:1em 0;'><h4>E-mail de Notificação foi Enviado com Sucesso!</h4><p>Você será notificado quando o produto estiver em estoque.</div>";

    }

      //se não tiver anexo

    else{

      $headers = "MIME-Version: 1.0\r\n";

      $headers .= "Content-type: text/html; charset=utf-8\r\n";

      $headers .= "From: \"$nome\" <$email_from>\r\n";

      $mensagem = "Nome: <b>$nome</b><br/><br/>E-mail: <b>$email_from</b><br/><br/>Cidade/Estado: <b>$cidade_estado</b><br/><br/>Telefone: <b>$telefone</b><br/><br/>Assunto: <b>$assunto</b><br/><br/>Produto/Modelo: <b>$produto</b><br/><br/>Notas Adicionais: <b>$mensagem</b>\n";

      //envia o email sem anexo

      mail($email,$assunto,$mensagem, $headers);

      echo"<div class='alert alert-success' style='margin:1em 0;'><h4>E-mail de Notificação foi Enviado com Sucesso!</h4><p>Você será notificado quando o produto estiver em estoque.</div>";

    }

  }

  ?>
  <?php }//end if quantity == 0 ?>






  <?php  if ($shipping_free == 0) {

    echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:150px; float:right;' />";

  } ?>

  <?php if ($review_status) { ?>

  <div class="rating">

   <?php if ($rating) { ?>

   <div class="star-rating star-<?php echo $rating;?>"></div>

   <?php } else { ?>

   <div class="star-rating star-0"></div>

   <?php } ?>                                      

   <span class=""><a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); $('body, html').animate({ scrollTop: 1000 }, 'slow'); return false;"><?php echo $reviews; ?></a> | 

    <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); $('body, html').animate({ scrollTop: 1000 }, 'slow'); return false;" class="rate-this"><?php echo $text_write; ?></a></span>

  </div><!-- rating -->

  <?php } ?>




</div><!-- col-md-6 -->


<?php if ($tags) { ?>        
<div class="col-md-12">           

  <div class="productTags"><i class="fa fa-tags"></i>

    <?php for ($i = 0; $i < count($tags); $i++) { ?>

    <?php if ($i < (count($tags) - 1)) { ?>

    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,

    <?php } else { ?>

    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>

    <?php } ?>

    <?php } ?>

  </div>


</div>
<?php } ?>

</div><!-- row -->

</div><!-- basicDetails -->

</section>


<section class="productDetail noPad">
  <?php if ($attribute_groups) { ?>

  <div class="xvAccordianWrap">

    <a class="xvaccorTriger" href="#"><i class="fa fa-bars"></i> <?php echo $tab_attribute; ?></a>

    <div class="accorPane">

      <div class="accorpaneWrap">

        <div class="prodDetail">

          <div class="row">

            <div class="col-sm-12">

              <table class="table table-bordered">

                <?php foreach ($attribute_groups as $attribute_group) { ?>

                <thead>

                  <tr>

                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>

                  </tr>

                </thead>

                <tbody>

                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>

                  <tr>

                    <td><?php echo $attribute['name']; ?></td>

                    <td><?php echo $attribute['text']; ?></td>

                  </tr>

                  <?php } ?>

                </tbody>

                <?php } ?>

              </table>

            </div>



          </div>

        </div>

      </div>

    </div>

  </div><!--//Accordion-->

  <?php } ?>



  <?php if ($review_status) { ?>

  <div class="xvAccordianWrap">

    <a class="xvaccorTriger" id="tab-review" href="#tab-review"><i class="fa fa-comments"></i> <?php echo $tab_review; ?></a>                                   

    <div class="accorPane">

      <div class="accorpaneWrap">

        <form class="form-horizontal">                                      

          <div class="prodReviews">

            <div id="review" style="margin-bottom:20px;"></div>

          </div>

          <div class="reviewSubmit">

            <div class="row">

              <div class="col-sm-8">

                <h6><?php echo $text_write; ?></h6>

                <?php if ($review_guest) { ?>

                <input class="light" type="text" name="name" id="input-name" required placeholder="<?php echo $entry_name; ?>">

                <textarea class="light" name="text" id="input-review" required placeholder="<?php echo $entry_review; ?>" style="margin-top:5px;"></textarea>

                <div class="help-block"><?php echo $text_note; ?></div>

                <div class=""><br>

                  <span><strong><?php echo $entry_rating; ?></strong></span>

                  <ul class="ratingsSelect">

                    <li>

                      <input type="radio" name="rating" value="1" />

                      <div class="star-rating star-1"></div>

                      <span class=""><?php echo $text_raing_star_1; ?></span>

                    </li>

                    <li>

                      <input type="radio" name="rating" value="2" />

                      <div class="star-rating star-2"></div>

                      <span class=""><?php echo $text_raing_star_2; ?></span>

                    </li>

                    <li>

                      <input type="radio" name="rating" value="3" />

                      <div class="star-rating star-3"></div>

                      <span class=""><?php echo $text_raing_star_3; ?></span>

                    </li>

                    <li>

                     <input type="radio" name="rating" value="4" />

                     <div class="star-rating star-4"></div>

                     <span class=""><?php echo $text_raing_star_4; ?></span>

                   </li>

                   <li>

                    <input type="radio" name="rating" value="5" />

                    <div class="star-rating star-5"></div>

                    <span class=""><?php echo $text_raing_star_5; ?></span>

                  </li>

                </ul>

              </div><br/>                                                    

              <img src="index.php?route=tool/captcha" alt="" id="captcha" />

              <input type="text" name="captcha" value="" id="input-captcha" required placeholder="<?php echo $entry_captcha; ?>" class="light" style="margin:5px 0;" />



              <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><?php echo $button_continue; ?></button>





              <?php } else { ?>



              <?php echo $text_login; ?>



              <?php } ?>



            </div>

          </div>

        </div>

      </form>

    </div>

  </div>

</div><!--//Accordion-->

<?php } ?>

</section>

<!-- tudo certo -->







<?php if ($products) { ?>
<section class="products">
  <header class="head familyOs">
  <h2><b><?php echo $text_related; ?></b></h2>
</header>

    <?php $i = 0; ?>

<ul class="products item-back-display style3 col-grid-4 space-30 clearfix">
    <?php foreach ($products as $product) { ?>    
     <li class="product text-center">
    <div class="productImages">
      <?php if ($product['quantity'] <= '0') { ?>
      <span class="dontStock"><?php echo $text_instock; ?></span>
      <?php } ?>
      <div class="image-default"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a></div>
    </div>
    <div class="productInfo">
      <h3><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h3>

        <?php if ($product['price']) { ?>
        <p><small><b>A partir de:</b></small></p>
        <ul class="pricestyle">
          <?php if (!$product['special']) { ?>
          <li><span class="priceReal"><?php echo $product['price']; ?></span></li>
          <?php } else { ?>
          <li>De: <strike><?php echo $product['price']; ?></strike></li>
          <li>Por: <span class="priceReal"><?php echo $product['special']; ?></span></li>
          <?php } ?> 
        </ul>
        <?php } ?>
        
        <?php if (!$product['quantity'] <= '0') { ?>
        <div class="btn-group btn-group-justified" role="group">

          <div class="btn-group" role="group">
            <button type="submit" class="add_to_cart_button btn btn-purple" data-toggle="tooltip" title="<?php echo $button_cart; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <?php echo $button_cart; ?></button>
          </div>

          <div class="btn-group" role="group">
            <button type="submit" class="add_to_cart_button btn btn-default" id="btnNotification" data-toggle="tooltip" title="<?php echo $button_details; ?>" onclick="location.href='<?php echo $product['href']; ?>#content';"><i class="fa fa-book"></i> <?php echo $button_details; ?></button>
          </div>

        </div>
        <?php }else{ ?>
        <ul class="linksBotoes">            
          <li><button type="submit" class="add_to_cart_button" id="btnNotification" data-toggle="tooltip" title="<?php echo $button_visit; ?>" onclick="location.href='<?php echo $product['href']; ?>#content';"><i class="fa fa-shopping-cart"></i> <?php echo $button_visit; ?></button></li>           
        </ul>
        <?php } ?>
      <?php  if ($product['shipping'] == 0) {echo "<img src='catalog/view/images/frete_gratis_gif.gif' alt='' style='width:110px; border-radius:5px; margin:.5em auto;' />";}?>
    </div>
  </li>
    <?php $i++; ?>
    <?php } ?>
  </ul>

  </div>
</section>

<?php } ?>







<?php echo $content_bottom; ?></div>

<?php echo $column_right; ?></div>

</div>

<script type="text/javascript"><!--

$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){

  $.ajax({

    url: 'index.php?route=product/product/getRecurringDescription',

    type: 'post',

    data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),

    dataType: 'json',

    beforeSend: function() {

      $('#recurring-description').html('');

    },

    success: function(json) {

      $('.alert, .text-danger').remove();



      if (json['success']) {

        $('#recurring-description').html(json['success']);

      }

    }

  });

});

//--></script> 

<script type="text/javascript"><!--

$('#button-cart').on('click', function() {

  $.ajax({

    url: 'index.php?route=checkout/cart/add',

    type: 'post',

    data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),

    dataType: 'json',

    beforeSend: function() {

      $('#button-cart').button('loading');

    },

    complete: function() {

      $('#button-cart').button('reset');

    },

    success: function(json) {

      $('.alert, .text-danger').remove();

      $('.form-group').removeClass('has-error');



      if (json['error']) {

        if (json['error']['option']) {

          for (i in json['error']['option']) {

            var element = $('#input-option' + i.replace('_', '-'));



            if (element.parent().hasClass('input-group')) {

              element.parent().after('<div class="alert alert-danger"><i class="fa fa-remove"></i> ' + json['error']['option'][i] + '</div>');

            } else {

              element.after('<div class="alert alert-danger"><i class="fa fa-remove"></i> ' + json['error']['option'][i] + '</div>');

            }

          }

        }



        if (json['error']['recurring']) {

          $('select[name=\'recurring_id\']').after('<div class="alert alert-danger"><i class="fa fa-remove"></i> ' + json['error']['recurring'] + '</div>');

        }



// Highlight any found errors

$('.alert alert-danger').parent().addClass('has-error');

}



if (json['success']) {

  $('.breadcrumb').after('<div class="container"><div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');



  $('#cart-total').html(json['total']);



  $('html, body').animate({ scrollTop: 0 }, 'slow');



  $('#cart > div').load('index.php?route=common/cart/info div div');

}

}

});

});

//--></script> 

<script type="text/javascript"><!--

$('.date').datetimepicker({

  pickTime: false

});



$('.datetime').datetimepicker({

  pickDate: true,

  pickTime: true

});



$('.time').datetimepicker({

  pickDate: false

});



$('button[id^=\'button-upload\']').on('click', function() {

  var node = this;



  $('#form-upload').remove();



  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');



  $('#form-upload input[name=\'file\']').trigger('click');



  timer = setInterval(function() {

    if ($('#form-upload input[name=\'file\']').val() != '') {

      clearInterval(timer);



      $.ajax({

        url: 'index.php?route=tool/upload',

        type: 'post',

        dataType: 'json',

        data: new FormData($('#form-upload')[0]),

        cache: false,

        contentType: false,

        processData: false,

        beforeSend: function() {

          $(node).button('loading');

        },

        complete: function() {

          $(node).button('reset');

        },

        success: function(json) {

          $('.text-danger').remove();



          if (json['error']) {

            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');

          }



          if (json['success']) {

            alert(json['success']);



            $(node).parent().find('input').attr('value', json['code']);

          }

        },

        error: function(xhr, ajaxOptions, thrownError) {

          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

        }

      });

    }

  }, 500);

});

//--></script> 

<script type="text/javascript"><!--

$('#review').delegate('.pagination a', 'click', function(e) {

  e.preventDefault();



  $('#review').fadeOut('slow');



  $('#review').load(this.href);



  $('#review').fadeIn('slow');

});



$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');



$('#button-review').on('click', function() {

  $.ajax({

    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',

    type: 'post',

    dataType: 'json',

    data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),

    beforeSend: function() {

      $('#button-review').button('loading');

    },

    complete: function() {

      $('#button-review').button('reset');

      $('#captcha').attr('src', 'index.php?route=tool/captcha#'+new Date().getTime());

      $('input[name=\'captcha\']').val('');

    },

    success: function(json) {

      $('.alert-success, .alert-danger').remove();



      if (json['error']) {

        $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');

      }



      if (json['success']) {

        $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');



        $('input[name=\'name\']').val('');

        $('textarea[name=\'text\']').val('');

        $('input[name=\'rating\']:checked').prop('checked', false);

        $('input[name=\'captcha\']').val('');

      }

    }

  });

});



$(document).ready(function() {

  $('.thumbnails').magnificPopup({

    type:'image',

    delegate: 'a',

    gallery: {

      enabled:true

    }

  });

});

//--></script> 

            <script type="text/javascript" src="index.php?route=product/live_options/js&product_id=<?php echo $product_id; ?>"></script>
            
<?php echo $footer; ?>