<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
  <meta charset="UTF-8" />
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content="<?php echo $keywords; ?>" />
  <?php } ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
  <script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
  <link href="view/javascript/bootstrap/less/bootstrap.less" rel="stylesheet/less" />
  <script src="view/javascript/bootstrap/less-1.7.4.min.js"></script>
  <link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
  <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
  <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
  <script src="view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <link type="text/css" href="view/stylesheet/stylesheet.css?5" rel="stylesheet" media="screen" />
  <script src="view/javascript/jquery.maskMoney.js" ></script>
  <script type="text/javascript">
  // Funcoes de Mascara para Valores Dentro do Administrador
  $(document).ready(function(){
    $(".formatValor").maskMoney({showSymbol:false, decimal:".", thousands:""});
  });
  </script>
  <script src="../catalog/view/javascript/jquery.maskedinput.js"></script>
  <script>
  $(document).ready(function(){
    $('#input-telephone').mask('(00) 0000-00009');
    $('#input-telephone').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#input-telephone').mask('(00) 0000-0000');
        } else {
          $('#input-telephone').mask('(00) 90000-0000');
        }
      });
    $('#input-fax').mask('(00) 0000-00009');
    $('#input-fax').blur(function(event) {
         if($(this).val().length == 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
          $('#input-fax').mask('(00) 0000-0000');
        } else {
          $('#input-fax').mask('(00) 90000-0000');
        }
      });
  });
  </script>
  <?php foreach ($styles as $style) { ?>
  <link type="text/css" href="<?php echo $style['href']; ?>" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
  <?php } ?>
  <?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>
  <script src="view/javascript/common.js" type="text/javascript"></script>
  <?php foreach ($scripts as $script) { ?>
  <script type="text/javascript" src="<?php echo $script; ?>"></script>
  <?php } ?>
<script type="text/javascript" src="view/javascript/stringToSlug/jquery.stringToSlug.min.js"></script>
</head>
<body>
  <div id="container">
    <header id="header" class="navbar navbar-static-top">
      <div class="navbar-header">
        <?php if ($logged) { ?>
        <a type="button" id="button-menu" class="pull-left"><i class="fa fa-indent fa-lg"></i></a>
        <?php } ?>
        <a href="<?php echo $home; ?>" class="navbar-brand hidden-xs hidden-sm hidden-md"><img src="https://www.innsystem.com.br/_galerias/logo_200.png" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" /></a></div>
        <?php if ($logged) { ?>
        <ul class="nav pull-right">

          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><span class="label label-danger pull-left"><?php echo $alerts; ?></span> <i class="fa fa-bell fa-lg"></i> <span class="hidden-xs hidden-sm hidden-md">Notificações</span></a>
            <ul class="dropdown-menu dropdown-menu-right alerts-dropdown">
              <li class="dropdown-header"><?php echo $text_order; ?></li>
              <li><a href="<?php echo $order_status; ?>" style="display: block; overflow: auto;"><span class="label label-warning pull-right"><?php echo $order_status_total; ?></span><?php echo $text_cofig_order_status; ?></a></li>
              <li><a href="<?php echo $complete_status; ?>"><span class="label label-success pull-right"><?php echo $complete_status_total; ?></span><?php echo $text_complete_status; ?></a></li>
              <li><a href="<?php echo $return; ?>"><span class="label label-danger pull-right"><?php echo $return_total; ?></span><?php echo $text_return; ?></a></li>
              <li class="divider"></li>
              <li class="dropdown-header"><?php echo $text_customer; ?></li>
              <li><a href="<?php echo $online; ?>"><span class="label label-success pull-right"><?php echo $online_total; ?></span><?php echo $text_online; ?></a></li>
              <li><a href="<?php echo $customer_approval; ?>"><span class="label label-danger pull-right"><?php echo $customer_total; ?></span><?php echo $text_approval; ?></a></li>
              <li class="divider"></li>
              <li class="dropdown-header"><?php echo $text_product; ?></li>
              <li><a href="<?php echo $product; ?>"><span class="label label-danger pull-right"><?php echo $product_total; ?></span><?php echo $text_stock; ?></a></li>
              <li><a href="<?php echo $review; ?>"><span class="label label-danger pull-right"><?php echo $review_total; ?></span><?php echo $text_review; ?></a></li>
              <li class="divider"></li>
              <li class="dropdown-header"><?php echo $text_affiliate; ?></li>
              <li><a href="<?php echo $affiliate_approval; ?>"><span class="label label-danger pull-right"><?php echo $affiliate_total; ?></span><?php echo $text_approval; ?></a></li>
            </ul>
          </li>

          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-desktop fa-lg"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_design; ?></span></a>
            <ul class="dropdown-menu dropdown-menu-right">
              <li><a href="<?php echo $banner; ?>"><?php echo $text_banner; ?></a></li>
              <li><a href="<?php echo $module; ?>"><?php echo $text_module; ?></a></li>
              <li><a href="<?php echo $layout; ?>"><?php echo $text_layout; ?></a></li>        
            </ul>
          </li>

          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users fa-lg"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_users; ?></span></a>
            <ul class="dropdown-menu dropdown-menu-right">
              <li><a href="<?php echo $user; ?>"><?php echo $text_user; ?></a></li>
              <li><a href="<?php echo $user_group; ?>"><?php echo $text_user_group; ?></a></li>
            </ul>
          </li>

          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog fa-lg"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_system; ?></span></a>
            <ul class="dropdown-menu dropdown-menu-right">
              <li class="dropdown-header"><?php echo $text_system; ?></li>
              <li><a href="<?php echo $setting; ?>"><?php echo $text_setting; ?></a></li>
              <li><a href="<?php echo $shipping; ?>"><?php echo $text_shipping; ?></a></li>
              <li><a href="<?php echo $payment; ?>"><?php echo $text_payment; ?></a></li>
              <li><a href="<?php echo $total; ?>"><?php echo $text_total; ?></a></li>
              <li class="divider"></li>
              <li class="dropdown-header"><?php echo $text_tools; ?></li>
              <li><a href="<?php echo $backup; ?>"><?php echo $text_backup; ?></a></li>
              <li style="display:none;"><a href="<?php echo $upload; ?>"><?php echo $text_upload; ?></a></li>      
              <li><a href="<?php echo $error_log; ?>"><?php echo $text_error_log; ?></a></li>
              <li class="divider"></li>
              <li class="dropdown-header"><?php echo $text_localisation; ?></li>
              <li style="display:none;"><a href="<?php echo $location; ?>"><?php echo $text_location; ?></a></li>
              <li style="display:none;"><a href="<?php echo $language; ?>"><?php echo $text_language; ?></a></li>
              <li style="display:none;"><a href="<?php echo $currency; ?>"><?php echo $text_currency; ?></a></li>
              <li style="display:none;"><a href="<?php echo $country; ?>"><?php echo $text_country; ?></a></li>
              <li style="display:none;"><a href="<?php echo $zone; ?>"><?php echo $text_zone; ?></a></li>
              <li style="display:none;"><a href="<?php echo $geo_zone; ?>"><?php echo $text_geo_zone; ?></a></li>
              <li><a href="<?php echo $stock_status; ?>"><?php echo $text_stock_status; ?></a></li>
              <li><a href="<?php echo $cofig_order_status; ?>"><?php echo $text_cofig_order_status; ?></a></li>
              <li><a href="<?php echo $length_class; ?>"><?php echo $text_length_class; ?></a></li>
              <li><a href="<?php echo $weight_class; ?>"><?php echo $text_weight_class; ?></a></li>
              <li class="divider"></li>
              <li class="dropdown-header"><?php echo $text_return; ?></li>            
              <li><a href="<?php echo $return_status; ?>"><?php echo $text_return_status; ?></a></li>
              <li><a href="<?php echo $return_action; ?>"><?php echo $text_return_action; ?></a></li>
              <li><a href="<?php echo $return_reason; ?>"><?php echo $text_return_reason; ?></a></li>                        
              <li class="divider"></li>
              <li class="dropdown-header"><?php echo $text_tax; ?></li>
              <li><a href="<?php echo $tax_class; ?>"><?php echo $text_tax_class; ?></a></li>
              <li><a href="<?php echo $tax_rate; ?>"><?php echo $text_tax_rate; ?></a></li>            
            </ul>
          </li>
<!--
    <li class="dropdown hide"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-briefcase fa-lg"></i> <span class="hidden-xs hidden-sm hidden-md">Suporte</span></a>
      <ul class="dropdown-menu dropdown-menu-right">
        <li class="dropdown-header"><i class="fa fa-shopping-cart"></i> <?php echo $text_store; ?></li>
        <?php foreach ($stores as $store) { ?>
        <li><a href="<?php echo $store['href']; ?>" target="_blank"><?php echo $store['name']; ?></a></li>
        <?php } ?>
        <li class="divider"></li>
        <li class="dropdown-header"><i class="fa fa-briefcase"></i> Central de Ajuda</li>
        <li><a href="https://innsystem.com.br/treinamento-loja-virtual" target="_blank">Treinamento</a></li>
        <li><a href="https://innsystem.com.br/dicas-loja-virtual" target="_blank">Dicas Úteis</a></li>
        <li><a href="https://innsystem.com.br/duvidas-frequentes" target="_blank">Dúvidas Frequentes</a></li>
        <li><a href="https://www.innsystem.com.br/submitticket.php?step=2&deptid=1" target="_blank">Ticket de Suporte</a></li>     
      </ul>
    </li> -->
    <li><a href="<?php echo $logout; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_logout; ?></span> <i class="fa fa-sign-out fa-lg"></i></a></li>
  </ul>
  <?php } ?>
</header>
