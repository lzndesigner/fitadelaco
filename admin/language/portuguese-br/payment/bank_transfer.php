<?php
// Heading
$_['heading_title']		 = 'Depósito Bancário';

// Text
$_['text_payment']		 = 'Pagamento';
$_['text_success']		 = 'Pagamento por Depósito bancário modificado com sucesso!';
$_['text_edit']          = 'Configurações do pagamento por Depósito bancário';
$_['text_bank_transfer'] = '<img src="../catalog/view/images/bank_transfer.png" alt="Depósito Bancário" title="Depósito Bancário" style="width:120px; border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_bank']		 = 'Instruções para depósito';
$_['entry_total']		 = 'Total mínimo';
$_['entry_order_status'] = 'Situação do pedido';
$_['entry_geo_zone']     = 'Região geográfica';
$_['entry_status']       = 'Situação';
$_['entry_sort_order']   = 'Posição';

// Help
$_['help_total']		 = 'O valor mínimo que o total do pedido deve alcançar para que o pagamento na entrega seja oferecido ao cliente.';

// Error
$_['error_permission']   = 'Atenção: Você não tem permissão para modificar o Pagamento na entrega!';
$_['error_bank']         = 'O campo Instruções para depósito é obrigatório!';