<?php
// Heading
$_['heading_title']    = 'Produtos em Destaques';

// Text
$_['text_module']      = 'Extensões';
$_['text_success']     = 'Extensão Produtos em destaque modificado com sucesso!';
$_['text_edit']        = 'Configurações do módulo Produtos em destaque';

// Entry
$_['entry_name']       = 'Título';
$_['entry_product']    = 'Produtos';
$_['entry_limit']      = 'Exibir';
$_['entry_width']      = 'Largura';
$_['entry_height']     = 'Altura';
$_['entry_status']     = 'Situação';

// Help
$_['help_product']     = '(Autocompletar)';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar o Extensão Produtos em destaque!';
$_['error_name']       = 'O Título deve ter entre 3 e 64 caracteres!';
$_['error_width']      = 'A largura é obrigatória!';
$_['error_height']     = 'A altura é obrigatória!';