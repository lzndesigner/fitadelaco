<?php
// Heading
$_['heading_title']    = 'Menu de departamentos';

// Text
$_['text_module']      = 'Extensões';
$_['text_success']     = 'Extensão Menu de departamentos modificado com sucesso!';
$_['text_edit']        = 'Configurações do Extensão Menu de departamentos';

// Entry
$_['entry_status']     = 'Situação';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar o Extensão Menu de departamentos!';