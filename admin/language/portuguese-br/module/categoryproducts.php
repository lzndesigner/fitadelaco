<?php
// Heading
$_['heading_title']    = 'Produtos por Categoria';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Sucesso: Você modificou o módulo Produtos por Categoria!';
$_['text_edit']        = 'Editar módulo Produtos por Categoria';

// Entry
$_['entry_name']       = 'Módulo';
$_['entry_product']    = 'Categoria';
$_['entry_limit']      = 'Limite';
$_['entry_width']      = 'Largura';
$_['entry_height']     = 'Altura';
$_['entry_status']     = 'Status';

// Help
$_['help_product']     = '(Autocomplete)';

// Error
$_['error_permission'] = 'Aviso: Você não tem permissão para modificar o módulo Produtos por Categoria!';
$_['error_name']       = 'O nome do módulo deve ter entre 3 e 64 caracteres!';
$_['error_width']      = 'Largura Requerida!';
$_['error_height']     = 'Altura Requerida';