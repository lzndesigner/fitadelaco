<?php
// Heading
$_['heading_title']    = 'Transportadora do Cliente';

// Text
$_['text_shipping']    = 'Frete';
$_['text_success']     = 'Frete Transportadora do Cliente modificado com sucesso!';
$_['text_edit']        = 'Configurações do frete Transportadora do Cliente';

// Entry
$_['entry_geo_zone']   = 'Região geográfica';
$_['entry_status']     = 'Situação';
$_['entry_sort_order'] = 'Posição';

// Error
$_['error_permission'] = 'Atenção: Você não tem permissão para modificar o frete Transportadora do Cliente!';