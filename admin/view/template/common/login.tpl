<!DOCTYPE html>
<html dir="ltr" lang="pt-br">
<head>
<meta charset="UTF-8" />
<title>Painel Administração - Loja Virtual</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/bootstrap/less/bootstrap.less" rel="stylesheet/less" />
<script src="view/javascript/bootstrap/less-1.7.4.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<script src="view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
<link type="text/css" href="view/stylesheet/stylesheet.css?2" rel="stylesheet" media="screen" />
<style type="text/css">
.centerfit{width:75%; height:75%; margin:2.25em auto; display:block;}
</style>
</head>
<body style="background:rgba(0,0,0,1) url(view/image/pattern/4.png) repeat;">
<div id="container">
<div id="content">
  <div class="container-fluid">
    <div class="row">
      <div class="box-login">
        <a href="index.php"><img src="https://www.innsystem.com.br/_galerias/logo_2.png" class="centerfit" alt="Painel Administração - Loja Virtual" title="Painel Administração - Loja Virtual" /></a>

        <div class="panel panel-default" style="border:0; border-radius:10px;">
          <div class="panel-heading">
            <h1 class="panel-title"><i class="fa fa-shield"></i> Área Restrita</h1>
          </div>
          <div class="panel-body">
            <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
              <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="input-username"><?php echo $entry_username; ?></label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input type="text" name="username" value="<?php echo $username; ?>" placeholder="<?php echo $entry_username; ?>" id="input-username" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label for="input-password"><?php echo $entry_password; ?></label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
                  <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                </div>
                        
              </div>
              <div class="text-left">
                <button type="submit" class="btn btn-primary"><i class="fa fa-key"></i> <?php echo $button_login; ?></button>
                <?php if ($forgotten) { ?>
                  <a href="<?php echo $forgotten; ?>" class="btn btn-danger"><i class="fa fa-key"></i> <?php echo $text_forgotten; ?></a></a>
                <?php } ?>
              </div>
              <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<footer id="footer" style="color:#FFF;">Adaptações: <a href="https://www.innsystem.com.br" target="_Blank" title="InnSystem - Inovação em Sistemas">InnSystem - Inovação em Sistemas</a>  Plataforma: <a href="http://www.opencart.com" target="_blank">OpenCart</a> <br> Todos os direitos reservados  &copy; 2010- <?php echo date('Y');?>.</footer>
</div>
</div>
</body></html>